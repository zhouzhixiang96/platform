#!/bin/bash

maven_home='/home/webDev/maven/apache-maven-3.9.6'
jar_path='./target/platform-0.0.1-SNAPSHOT.jar'

echo "starting"
nohup ${JAVA_HOME}/bin/java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5898 -jar ${jar_path} --spring.profiles.active=test > nohup.out 2>&1 &

tail -f ./nohup.out
package com.zixian.platform.template;

public abstract class OperateTemplate<T> {
	
	public T[] input_s = (T[])new Object[32];
	
	public T[] input_r = (T[])new Object[32];
	
	public T[] matInput_s = (T[])new Object[1024];
	
	public T[] matInput_r = (T[])new Object[1024];
	
	public T[] matConv = (T[])new Object[9];
	
	public long[] sameResult = new long[16];
	
	public long[] matOutput = new long[1024];
	
	public long[] matAOutput = new long[16*16];

	public long[] mat96Output = new long[96];

//	public T[] output = (T[])new Object[32];
	
	public long[] output = new long[32];
	public double[] outputDouble = new double[32];

	public String ip = "127.0.0.1";
	
	public int port = 9000;
	
	private boolean bMat = false;
	
	private boolean bresult = false;
	
	private boolean b16 = false;
	
	public int decimal = 0;
	
	//模板方法
	public void OperateCaculate()
	{
		//第一步：初始化测试数据
		initData();
		//第二步：开始算子计算
		caculate();
		//第三步：打印结果
		printResult();
	}
	
	public abstract void initData();
	
	public abstract void caculate();
	
	public void printResult()
	{
		if(b16)
		{
			System.out.println("16*16");
			for(int row = 0 ; row < 16 ; row++)
			{
				for(int col = 0 ; col < 16 ; col++)
				{
					System.out.print(matAOutput[row*16+col]);
					System.out.print("\t");
				}
				System.out.print("\n");
			}
		}else if(bMat)
		{
			for(int row = 0 ; row < 32 ; row++)
			{
				for(int col = 0 ; col < 32 ; col++)
				{
					System.out.print(matOutput[row*32+col]);
					System.out.print("\t");
				}
				System.out.print("\n");
			}

		}else if(bresult)
		{
			for(long l : sameResult)
			{
				System.out.print(l);
				System.out.print("\t");
			}
		}else {
			for(long l : output)
			{
				System.out.print(l);
				System.out.print("\t");
			}
		}
	}
	
	public long[] Long2long(Long[] values)
	{
		long[] tmp = new long[values.length];
		for(int i = 0 ; i < values.length ; i++)
		{
			tmp[i] = values[i].longValue();
		}
		return tmp;
	}
	
	public long[] getS()
	{
		long[] tmp = new long[32];
		for(int i = 0 ; i < input_s.length ; i++)
		{
			tmp[i] = (Long) input_s[i];
		}
		return tmp;
	}
	
	public long[] getR()
	{
		long[] tmp = new long[32];
		for(int i = 0 ; i < input_r.length ; i++)
		{
			tmp[i] = (Long) input_r[i];
		}
		return tmp;
	}
	
	public long[] getO()
	{
		return output;
	}

	public double[] getDoubleO()
	{
		return outputDouble;
	}
	
	public long[] getMatS()
	{
		long[] tmp = new long[1024];
		for(int i = 0 ; i < matInput_s.length ; i++)
		{
			tmp[i] = (Long) matInput_s[i];
		}
		return tmp;
	}
	
	public long[] getMatR()
	{
		long[] tmp = new long[1024];
		for(int i = 0 ; i < matInput_r.length ; i++)
		{
			tmp[i] = (Long) matInput_r[i];
		}
		return tmp;
	}
	
	public long[] getMatO()
	{
		return matOutput;
	}
	
	public long[] getResult()
	{
		bresult = true;
		return sameResult;
	}
	
	public long[] getMatC()
	{
		long[] tmp = new long[matConv.length];
		for(int i = 0 ; i < matConv.length ; i++)
		{
			tmp[i] = (Long) matConv[i];
		}
		return tmp;
	}

	public long[] getMatAS()
	{
		long[] tmp = new long[16*16];
		double seed = 0.05;
		for(int i = 0 ; i < 16 ; i++)
		{
			seed = seed * (i + 1);
			for(int j = 0 ; j < 16 ; j++)
			{
				tmp[i*16 + j] = (long) (seed*(Math.pow(2,decimal)));
				seed += 0.05;
			}
			seed = 0.05;
		}
		return tmp;
	}
	
	public long[] getMatAR()
	{
		long[] tmp = new long[16*16*4];
		double seed = 0.05;
		for(int i = 0 ; i < 16 ; i++)
		{
			seed = seed * (i + 2);
			for(int j = 0 ; j < 16 ; j++)
			{
				tmp[i*16 + j] = (long) (seed*(Math.pow(2,decimal)));
				seed += 0.05;
			}
			seed = 0.05;
		}
		for(int i = 0 ; i < 16 ; i++)
		{
			seed = seed * (i + 3);
			for(int j = 0 ; j < 16 ; j++)
			{
				tmp[16*16 + i*16 + j] = (long) (seed*(Math.pow(2,decimal)));
				seed += 0.05;
			}
			seed = 0.05;
		}
		for(int i = 0 ; i < 16 ; i++)
		{
			seed = seed * (i + 4);
			for(int j = 0 ; j < 16 ; j++)
			{
				tmp[16*16*2 + i*16 + j] = (long) (seed*(Math.pow(2,decimal)));
				seed += 0.05;
			}
			seed = 0.05;
		}
		return tmp;
	}
	
	public long[] getMatAO()
	{
		b16 = true;
		return matAOutput;
	}

	public long[] getMat96()
	{
		return mat96Output;
	}
	
	public void setLong(long[] s, long[] r, long[] o)
	{
		bMat = false;
		for(int i = 0 ; i < 32 ; i++)
		{
			input_s[i] = (T) Long.valueOf(s[i]);
			input_r[i] = (T) Long.valueOf(r[i]);
//			output[i] = (T) Long.valueOf(o[i]);
		}
	}
	
	public void setMat(long[] s, long[] r)
	{
		bMat = true;
		for(int i = 0 ; i < 1024 ; i++)
		{
			matInput_s[i] = (T) Long.valueOf(s[i]);
			matInput_r[i] = (T) Long.valueOf(r[i]);
		}
	}
	
	public void setConv(long[] c)
	{
		bMat = true;
		for(int i = 0 ; i < 9 ; i++)
		{
			matConv[i] = (T) Long.valueOf(c[i]);
		}
	}
}

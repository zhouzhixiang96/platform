package com.zixian.platform.template;

import cn.hutool.core.util.StrUtil;
import com.zixian.platform.entity.result.CalRResult;
import com.zixian.platform.entity.result.CalSResult;
import com.zixian.platform.entity.result.OperateResult;
import com.zixian.platform.enums.OperateEnum;
import com.zixian.platform.exception.PlatformExceptionEnum;
import com.zixian.platform.service.DynamicLibraryLoading;
import com.zixian.platform.utils.exception.PlatformException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * author: tangzw
 * date: 2024/2/2 10:56
 * description:
 **/
@Slf4j
public abstract class OperateTemplateV2<T> {
    public final String ip = "127.0.0.1";
    public final int port = 9000;
    DynamicLibraryLoading dynamicLibraryLoading = new DynamicLibraryLoading();

    public abstract OperateResult<Double, Double, Integer> calculate(String mType) throws InterruptedException;

    public abstract CalSResult<Double> calculateS(String mType, String ip, Integer port) throws InterruptedException;

    public abstract CalRResult calculateR(String mType, Integer port) throws InterruptedException;

    public static class Operate32Template extends OperateTemplateV2<Integer> {
        @Override
        public OperateResult<Double, Double, Integer> calculate(String mType) throws InterruptedException {
            if(mType.isEmpty()) {
                return null;
            }

            OperateEnum operateEnum = OperateEnum.toOperateEnum(mType);
            if (operateEnum == null) {
                throw new PlatformException(PlatformExceptionEnum.OPERATOR_NOT_EXISTS);
            }

            Result result = getResult(operateEnum);

            switch (operateEnum) {
                //已验证
                case LESS32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Less32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Less32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;
                //已验证
                case ADD32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Add32S(result.input_s, result.s_length , result.r_length, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Add32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;

                //已验证
                case SUB32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Sub32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Sub32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;

                //已验证
                case SUM32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Sum32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Sum32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;

                //已验证
                case EXP32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Exp32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port),
                            () ->dynamicLibraryLoading.getPPMLACSo().Exp32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;

//                case XOR32:
//                    sendThread = new Thread(() -> dynamicLibraryLoading.getPPMLACSo().Xor32S(input_r, s_length, s_length, 0, ip, port));
//                    sendThread.start();
//                    Thread.sleep(1000);
//                    dynamicLibraryLoading.getPPMLACSo().Xor32R(input_r, s_length, s_length, output, 0, port);

                //已验证
                case BATCHNORM32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().BatchNorm32S(result.input_s, result.s_row, result.s_col, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().BatchNorm32R(result.input_s, result.s_row, result.s_col, result.output, result.decimal_bit, port));
                    break;

                //已验证
                case SIGMOID32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Sigmoid32S(result.input_s, result.s_row, result.s_col, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Sigmoid32R(result.input_s, result.s_row, result.s_col, result.output, result.decimal_bit, port));
                    break;

                //已验证
                case RMSNORM32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().RmsNorm32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().RmsNorm32R(result.input_s, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;

                //已验证
                case CONV2D32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Conv2D32S(result.input_s, result.s_row, result.s_col, result.r_row, result.r_col, result.decimal_bit, ip , port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Conv2D32R(result.input_r, result.s_row, result.s_col, result.r_row, result.r_col, result.output, result.decimal_bit, port));
                    break;

                //已验证
                case TANH32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Tanh32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Tanh32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;

                //已验证
                case SOFTMAX32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Softmax32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Softmax32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;

                //已验证，准确度待考察
                case SILU32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Silu32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Silu32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;

                //已验证, 多次会失败
                case MATMUL32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().MatMul32S(result.input_s, result.s_row, result.s_col, result.r_row, result.r_col, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().MatMul32R(result.input_r, result.s_row, result.s_col, result.r_row, result.r_col, result.output, result.decimal_bit, port));
                    break;

                //已验证
                case SECMUL32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().Secmul32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().Secmul32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port));
                    break;

                //已验证, 多次会失败
                case MATTRANS32:
                    asyncRun(() -> dynamicLibraryLoading.getPPMLACSo().MatMulTransed32S(result.input_s, result.s_row, result.s_col, result.r_row, result.r_col, result.decimal_bit, ip, port),
                            () -> dynamicLibraryLoading.getPPMLACSo().MatMulTransed32R(result.input_r, result.s_row, result.s_col, result.r_row, result.r_col, result.output, result.decimal_bit, port));
                    break;

                default:
                    log.error("对应的32位运算即将实现{}", mType);
            }

            return OperateResult.<Double, Double, Integer>builder()
                    .originResult(Arrays.stream(result.getOutput()).boxed().collect(Collectors.toList()))
                    .inputS(getPrecise2Double(result.input_s, result.decimal_bit))
                    .inputR(getPrecise2Double(result.input_r, result.decimal_bit))
                    .result(getPrecise2Double(result.output, result.decimal_bit))
                    .build();
        }

        @Override
        public CalSResult<Double> calculateS(String mType, String ip, Integer port) throws InterruptedException {
            if(!StrUtil.isAllNotEmpty(mType, ip) || port == null) {
                return null;
            }

            OperateEnum operateEnum = OperateEnum.toOperateEnum(mType);
            if (operateEnum == null) {
                throw new PlatformException(PlatformExceptionEnum.OPERATOR_NOT_EXISTS);
            }

            Result result = getResult(operateEnum);

            switch (operateEnum) {
                //已验证
                case LESS32:
                    dynamicLibraryLoading.getPPMLACSo().Less32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port);

                //已验证
                case ADD32:
                    dynamicLibraryLoading.getPPMLACSo().Add32S(result.input_s, result.s_length , result.r_length, result.decimal_bit, ip, port);
                    break;

                //已验证
                case SUB32:
                    dynamicLibraryLoading.getPPMLACSo().Sub32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port);
                    break;

                //已验证
                case SUM32:
                    dynamicLibraryLoading.getPPMLACSo().Sum32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port);
                    break;

                //已验证
                case EXP32:
                    dynamicLibraryLoading.getPPMLACSo().Exp32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port);
                    break;

                //已验证
                case BATCHNORM32:
                    dynamicLibraryLoading.getPPMLACSo().BatchNorm32S(result.input_s, result.s_row, result.s_col, result.decimal_bit, ip, port);
                    break;

                //已验证
                case SIGMOID32:
                    dynamicLibraryLoading.getPPMLACSo().Sigmoid32S(result.input_s, result.s_row, result.s_col, result.decimal_bit, ip, port);
                    break;

                //已验证
                case RMSNORM32:
                    dynamicLibraryLoading.getPPMLACSo().RmsNorm32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port);
                    break;

                //已验证
                case CONV2D32:
                    dynamicLibraryLoading.getPPMLACSo().Conv2D32S(result.input_s, result.s_row, result.s_col, result.r_row, result.r_col, result.decimal_bit, ip , port);
                    break;

                //已验证
                case TANH32:
                    dynamicLibraryLoading.getPPMLACSo().Tanh32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port);
                    break;

                //已验证
                case SOFTMAX32:
                    dynamicLibraryLoading.getPPMLACSo().Softmax32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port);
                    break;

                //已验证，准确度待考察
                case SILU32:
                    dynamicLibraryLoading.getPPMLACSo().Silu32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port);
                    break;

                //已验证, 多次会失败
                case MATMUL32:
                    dynamicLibraryLoading.getPPMLACSo().MatMul32S(result.input_s, result.s_row, result.s_col, result.r_row, result.r_col, result.decimal_bit, ip, port);
                    break;

                //已验证
                case SECMUL32:
                    dynamicLibraryLoading.getPPMLACSo().Secmul32S(result.input_s, result.s_length, result.r_length, result.decimal_bit, ip, port);
                    break;

                //已验证, 多次会失败
                case MATTRANS32:
                    dynamicLibraryLoading.getPPMLACSo().MatMulTransed32S(result.input_s, result.s_row, result.s_col, result.r_row, result.r_col, result.decimal_bit, ip, port);
                    break;

                default:
                    log.error("对应的32位运算即将实现{}", mType);
            }

            return CalSResult.<Double>builder()
                    .inputS(getPrecise2Double(result.input_s, result.decimal_bit))
                    .build();
        }

        @Override
        public CalRResult<Double, Double, Long> calculateR(String mType, Integer port) throws InterruptedException {
            if(!StrUtil.isAllNotEmpty(mType) || port == null) {
                return null;
            }

            OperateEnum operateEnum = OperateEnum.toOperateEnum(mType);
            if (operateEnum == null) {
                throw new PlatformException(PlatformExceptionEnum.OPERATOR_NOT_EXISTS);
            }

            Result result = getResult(operateEnum);

            switch (operateEnum) {
                //已验证
                case LESS32:
                    dynamicLibraryLoading.getPPMLACSo().Less32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port);

                //已验证
                case ADD32:
                    dynamicLibraryLoading.getPPMLACSo().Add32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port);
                    break;

                //已验证
                case SUB32:
                    dynamicLibraryLoading.getPPMLACSo().Sub32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port);
                    break;

                //已验证
                case SUM32:
                    dynamicLibraryLoading.getPPMLACSo().Sum32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port);
                    break;

                //已验证
                case EXP32:
                    dynamicLibraryLoading.getPPMLACSo().Exp32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port);
                    break;

//                case XOR32:
//                    sendThread = new Thread(() -> dynamicLibraryLoading.getPPMLACSo().Xor32S(input_r, s_length, s_length, 0, ip, port));
//                    sendThread.start();
//                    Thread.sleep(1000);
//                    dynamicLibraryLoading.getPPMLACSo().Xor32R(input_r, s_length, s_length, output, 0, port);

                //已验证
                case BATCHNORM32:
                    dynamicLibraryLoading.getPPMLACSo().BatchNorm32R(result.input_s, result.s_row, result.s_col, result.output, result.decimal_bit, port);
                    break;

                //已验证
                case SIGMOID32:
                    dynamicLibraryLoading.getPPMLACSo().Sigmoid32R(result.input_s, result.s_row, result.s_col, result.output, result.decimal_bit, port);
                    break;

                //已验证
                case RMSNORM32:
                    dynamicLibraryLoading.getPPMLACSo().RmsNorm32R(result.input_s, result.s_length, result.r_length, result.output, result.decimal_bit, port);
                    break;

                //已验证
                case CONV2D32:
                    dynamicLibraryLoading.getPPMLACSo().Conv2D32R(result.input_r, result.s_row, result.s_col, result.r_row, result.r_col, result.output, result.decimal_bit, port);
                    break;

                //已验证
                case TANH32:
                    dynamicLibraryLoading.getPPMLACSo().Tanh32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port);
                    break;

                //已验证
                case SOFTMAX32:
                    dynamicLibraryLoading.getPPMLACSo().Softmax32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port);
                    break;

                //已验证，准确度待考察
                case SILU32:
                    dynamicLibraryLoading.getPPMLACSo().Silu32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port);
                    break;

                //已验证, 多次会失败
                case MATMUL32:
                    dynamicLibraryLoading.getPPMLACSo().MatMul32R(result.input_r, result.s_row, result.s_col, result.r_row, result.r_col, result.output, result.decimal_bit, port);
                    break;

                //已验证
                case SECMUL32:
                    dynamicLibraryLoading.getPPMLACSo().Secmul32R(result.input_r, result.s_length, result.r_length, result.output, result.decimal_bit, port);
                    break;

                //已验证, 多次会失败
                case MATTRANS32:
                    dynamicLibraryLoading.getPPMLACSo().MatMulTransed32R(result.input_r, result.s_row, result.s_col, result.r_row, result.r_col, result.output, result.decimal_bit, port);
                    break;

                default:
                    log.error("对应的32位运算即将实现{}", mType);
            }

            return CalRResult.<Double, Double, Long>builder()
                    .originResult(Arrays.stream(result.getOutput()).boxed().map(Long::valueOf).collect(Collectors.toList()))
                    .inputR(getPrecise2Double(result.input_r, result.decimal_bit))
                    .result(getPrecise2Double(result.output, result.decimal_bit))
                    .build();
        }

        private void printResult(Result result) {
            System.out.println(StrUtil.format("input_s: ", getPrecise2Double(result.input_s, result.getDecimal_bit())));
            System.out.println(StrUtil.format("input_r: ", getPrecise2Double(result.input_r, result.getDecimal_bit())));
        }

        private static List<Double> getPrecise2Double(int[] input, int decimalBit) {
            return Arrays.stream(input).mapToDouble(i -> {
                if (decimalBit > 0) {
                    return (double) i / (1 << decimalBit);
                } else {
                    return i;
                }
            }).boxed().collect(Collectors.toList());
        }

        public static List<Double> getPreciseLong2Double(long[] input, int decimalBit) {
            return Arrays.stream(input).mapToDouble(i -> {
                if (decimalBit > 0) {
                    return (double) i / (1 << decimalBit);
                } else {
                    return i;
                }
            }).boxed().collect(Collectors.toList());
        }

        private static Result getResult(OperateEnum operateEnum) {
            int s_length;
            int r_length;
            int o_length;
            int s_row = 32;
            int s_col = 32;
            int r_row = 32;
            int r_col = 32;
            int decimal_bit;
            if (operateEnum.getType() == 0) {
                s_length = 32;
                r_length = 32;
                o_length = s_length;
                decimal_bit = 0;
            } else if (operateEnum.getType() == 1) {
                s_length = s_row * s_col;
                r_length = r_row * r_col;
                o_length = s_length;
                decimal_bit = 0;
            } else if (operateEnum.getType() == 2) {
                s_length = 32;
                r_length = 32;
                o_length = 32;
                decimal_bit = 16;
            } else if (operateEnum.getType() == 3) {
                s_length = s_row * s_col;
                r_length = r_row * r_col;
                o_length = s_length;
                decimal_bit = 16;
            } else if (operateEnum.getType() == 4) {
                s_length = s_row * s_col;
                r_row = 3;
                r_col = 3;
                r_length = r_row * r_col;
                o_length = s_length;
                decimal_bit = 16;
            } else {
                s_length = 32;
                r_length = 32;
                o_length = 32;
                decimal_bit = 16;
            }
            int[] input_s = new int[s_length];
            int[] input_r = new int[r_length];
            int[] output = new int[o_length];

            for(int i = 0 ; i < s_length ; i++)
            {
                int factor = 1 << decimal_bit;
                double random = Math.random();
                if (decimal_bit > 0) {
                    input_s[i] = (int) (random * factor);
                } else {
                    input_s[i] = (int) (random * 100);
                }
            }

            for(int i = 0 ; i < r_length ; i++)
            {
                if (operateEnum.getType() == 5) {
                    input_r[i] = 0;
                    continue;
                }
                int factor = 1 << decimal_bit;
                double random = Math.random();
                if (decimal_bit > 0) {
                    input_r[i] = (int) (random * factor);
                } else {
                    input_r[i] = (int) (random * 100);
                }
            }

            return Result.builder()
                    .s_length(s_length)
                    .r_length(r_length)
                    .s_row(s_row)
                    .r_row(r_row)
                    .s_col(s_col)
                    .r_col(r_col)
                    .input_s(input_s)
                    .input_r(input_r)
                    .output(output)
                    .decimal_bit(decimal_bit)
                    .build();
        }

        @Data
        @Builder
        @AllArgsConstructor
        private static class Result {
            public final int s_length;
            public final int r_length;
            public final int s_row;
            public final int r_row;
            public final int s_col;
            public final int r_col;
            public final int[] input_s;
            public final int[] input_r;
            public final int[] output;
            public final int decimal_bit;
        }

        private Thread asyncRun(Runnable runnable1, Runnable runnable2) throws InterruptedException {
            Thread thread = new Thread(runnable1);
            thread.start();
//            Thread.sleep(1000);
            runnable2.run();
            return thread;
        }
    }

}

package com.zixian.platform.aop;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.alibaba.fastjson.JSON;
import com.zixian.platform.entity.HistoryEntity;
import com.zixian.platform.mapper.DatabaseMapper;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class HistoryAspect {
	
	//定义切点
    @Pointcut(value="@annotation(com.zixian.platform.aop.HistoryAop)")
    public void pointcut() { }
    //定义切点
    @Pointcut(value="execution(* com.zixian.platform.controller.*.*(..))")
    public void operErrorPointCut() { }
    
    private long startTimeStamp = 0;
    
    /**
     * 执行切点之前
     */
    @Before("pointcut()")
    public void doBefore(JoinPoint joinPoint) {
        log.info("=====================开始执行前置通知==================");
        startTimeStamp = System.currentTimeMillis();
        try {
            String targetName = joinPoint.getTarget().getClass().getName();
            String methodName = joinPoint.getSignature().getName();
            Object[] arguments = joinPoint.getArgs();
            Class<?> targetClass = Class.forName(targetName);
            Method[] methods = targetClass.getMethods();
            String operation = "";
            for (Method method : methods) {
                if (method.getName().equals(methodName)) {
                    Class<?>[] clazzs = method.getParameterTypes();
                    if (clazzs.length == arguments.length) {
                        operation = method.getAnnotation(HistoryAop.class).value();// 内容
                        break;
                    }
                }
            }
            StringBuilder paramsBuf = new StringBuilder();
            for (Object arg : arguments) {
                paramsBuf.append(arg);
                paramsBuf.append("&");
            }
            // *========控制台输出=========*//
            log.info("[X用户]执行了[" + operation + "],类:" + targetName + ",方法名：" + methodName + ",参数:"
                    + paramsBuf.toString());
            log.info("=====================执行前置通知结束==================");
        } catch (Throwable e) {
            log.error("around " + joinPoint + " with exception : " + e.getMessage());
        }
    }
    

    /**
     * 设置操作异常切入点，拦截用户的操作日志，连接点正常执行后执行，若连接点抛出异常则不会执行
     * @param joinPoint 切入点
     * @param keys 返回结果
     */
    @AfterReturning(value = "pointcut()", returning = "keys")
    public void saveOperationLog(JoinPoint joinPoint, Object keys) {
        log.info("=====================AfterReturning==================");
        //获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        //从获取到的RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        try {
            //在切面织入点通过反射机制获取织入点的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            //获取织入点的方法
            Method method = signature.getMethod();
            //获取操作
            HistoryAop annotation = method.getAnnotation(HistoryAop.class);
            String value = "";
            if (annotation != null) {
                value = annotation.value();
            }
            //获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();
            //获取请求的方法
            String methodName = method.getName();;
            methodName = className + "." + methodName;
            log.info("==>请求的类:{},方法:{}",className,methodName);
            Map<String, String> reqMap = converMap(request.getParameterMap());
            //将参数所在的数组转为json
            String params = JSON.toJSONString(reqMap);
            log.info("==>请求的参数:{}",params);
            log.info("==>请求的结果:{}",JSON.toJSONString(keys));
//            saveHistory(reqMap,value,true);
        }catch (Exception e){
        	log.error("历史记录传入参数异常", e);
        }
    }
 
    /**
     * 异常返回通知，用于拦截异常日志的信息，连接点抛出异常后执行
     * @param joinPoint 切入点
     * @param e 异常信息
     */
    @AfterThrowing(pointcut = "operErrorPointCut()", throwing = "e")
    public void saveErrorLog(JoinPoint joinPoint,Throwable e){
        log.info("=====================AfterThrowing==================");
        //获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        //从RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        try {
            //在切面织入点通过反射机制获取织入点的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            //获取织入点的方法
            Method method = signature.getMethod();
            //获取操作
            HistoryAop annotation = method.getAnnotation(HistoryAop.class);
            String value = "";
            if (annotation != null) {
                value = annotation.value();
                log.info(value);
            }
            //获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();
            //获取请求的方法
            String methodName = method.getName();
            methodName = className + "." + methodName;
            log.info("==>请求的类:{},方法:{}",className,methodName);
            //请求的参数
            Map<String, String> rtnMap = converMap(request.getParameterMap());
            String params = JSON.toJSONString(rtnMap);
            log.info("==>请求的参数:{}",params);
            log.info("==>请求的异常名称:{}",e.getClass().getName());
            log.info("==>请求的异常名称:{},异常内容:{}",e.getClass().getName(),e.getMessage());
            //请求URI
            log.info("==>请求URL:{}",request.getRequestURI());
            saveHistory(rtnMap,value,false);
        }catch (Exception exception){
            log.error("历史记录传入参数异常",e);
        }
    }
    
    /**
     * 转换request请求参数
     * @param paramMap request中获取的参数数组
     * @return 转换后的数组
     */
    public Map<String, String> converMap(Map<String,String[]> paramMap){
        Map<String, String> rtnMap = new HashMap<>();
        for (String key : paramMap.keySet()) {
        	String tmp = paramMap.get(key)[0];
        	if(key.equals("init"))
        	{
        		key = "initiator";
        	}else if(key.equals("parti"))
        	{
        		key = "participate";
        	}
            rtnMap.put(key,tmp);
        }
        return rtnMap;
    }
    
    @Autowired
    private DatabaseMapper databaseMapper;
    
    public void saveHistory(Map<String, String> paramMap,String type,boolean succ)
    {
    	String init = paramMap.get("initiator");
    	String part = paramMap.get("participate");
    	String count = paramMap.get("count");
    	if(count == null)count = "0";
    	String result = succ ? "成功" : "失败";
    	startTimeStamp = startTimeStamp == 0 ? System.currentTimeMillis() : startTimeStamp;
    	String time = String.valueOf((System.currentTimeMillis() - startTimeStamp)) + "毫秒";
    	init = init == null ? "中小企业" : init;
    	part = part == null ? "政务大数据中心" : part;
    	databaseMapper.insertHistory(new HistoryEntity(0, init, part, type, result, startTimeStamp, time, Integer.parseInt(count)));
    }
}

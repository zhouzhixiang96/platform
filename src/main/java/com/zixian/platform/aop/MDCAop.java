package com.zixian.platform.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.UUID;


/**
 * author: tangzw
 * date: 2024/1/16 15:35
 * description:
 **/
@Aspect
@Component
@Slf4j
public class MDCAop {
    public static final String TRACE_ID = "trace_id";

    @Pointcut(value = "execution( * com.zixian.platform.controller.*.*(..))")
    public void controllerPoint(){

    }

    @Around("controllerPoint()")
    public Object MDCAround(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            MDC.put(TRACE_ID, UUID.randomUUID().toString());
            return joinPoint.proceed();
        } finally {
            MDC.remove(TRACE_ID);
        }
    }
}

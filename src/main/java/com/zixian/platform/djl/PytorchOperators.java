package com.zixian.platform.djl;

import ai.djl.ndarray.NDArray;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PytorchOperators {

    public static NDArray selfAttention(NDArray x, NDArray w_q, NDArray w_k, NDArray w_v, NDArray mask, double sqrt_scale) {
        try {
            NDArray q = x.matMul(w_q);
            NDArray k = x.matMul(w_k);
            NDArray v = x.matMul(w_v);

            q = q.mul(1d / sqrt_scale).add(mask);
            NDArray a = q.matMul(k.transpose());
            NDArray a_hat = a.softmax(1);
            return a_hat.matMul(v);
        } catch (Exception e) {
            log.error("");
            return null;
        }
    }

    public static void errorLog(String func, Exception e) {
        log.error("PytorchOperators {} error: {}", func, e.getLocalizedMessage());
    }

    public static String toReadbaleString(NDArray array) {
        return array.toDebugString(1000, 100, 1000, 100, true);
    }
}

package com.zixian.platform.djl;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDList;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.index.NDIndex;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;
import ai.djl.nn.Activation;
import ai.djl.nn.Block;
import ai.djl.nn.Blocks;
import ai.djl.nn.norm.BatchNorm;
import ai.djl.training.ParameterStore;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@SuppressWarnings("LoggingPlaceholderCountMatchesArgumentCount")
@Slf4j
@Component
public class Operators {

    public static double[] conv2d(double[] inputArr, double[] kernelArr) {
        double inputShapeD = Math.sqrt(inputArr.length);
        double kernelShapeD = Math.sqrt(kernelArr.length);
        long inputShape = Double.valueOf(inputShapeD).longValue();
        long kernelShape = Double.valueOf(kernelShapeD).longValue();
        long resultShape = inputShape - kernelShape + 1;

        try (NDManager manager = NDManager.newBaseManager()) {
            NDArray input = manager.create(inputArr);
            NDArray kernel = manager.create(kernelArr);
            input = input.reshape(inputShape, -1);
            kernel = kernel.reshape(kernelShape, -1);

            NDArray result = manager.zeros(new Shape(resultShape, resultShape), DataType.FLOAT64);

            for (long row = 0; row < resultShape; row++) {
                for (long col = 0; col < resultShape; col++) {
                    NDIndex ndIndex = new NDIndex(StrUtil.format("{}:{}, {}:{}", row, row + kernelShape, col, col + kernelShape));
                    NDArray splitMatrix = input.get(ndIndex);
                    NDArray mul = splitMatrix.mul(kernel);
                    result.set(new NDIndex(row, col), mul.sum());
                }
            }

            return result.toDoubleArray();
        } catch (Exception e) {
            log.error("djl conv2d error: {}", e);
        }

        return new double[]{-1};
    }
    
    public static double[] batchNorm(double[] inputArr) {
    	double[] alphaArr = new double[inputArr.length];
    	for(int i = 0 ; i < inputArr.length ; i++)
    	{
    		alphaArr[i] = 1;
    	}
    	
    	double[] betaArr = new double[inputArr.length];
    	for(int i = 0 ; i < inputArr.length ; i++)
    	{
    		betaArr[i] = 0;
    	}
		return batchNorm(inputArr, alphaArr, betaArr, 1.0E-5);

    }
    
    public static double[] batchNorm(double[] inputSArr, double[] alphaArr, double[] betaArr, double eps)
    {
    	if (inputSArr == null || alphaArr == null || betaArr == null) {
            log.error("djl batchNorm input is null");
            return new double[]{-1};
        }

        if (inputSArr.length != alphaArr.length || inputSArr.length != betaArr.length) {
            log.error("djl batchNorm length not match");
            return new double[]{-1};
        }

        try (NDManager manager = NDManager.newBaseManager()) {
            NDArray alpha = manager.create(alphaArr);
            NDArray beta = manager.create(betaArr);
            NDArray input = manager.create(inputSArr);
            NDArray mean = input.mean();
            NDArray var = input.sub(mean).square().sum().div(input.getShape().get(0));

            NDArray result = alpha.mul(input.sub(mean)).div(var.add(eps).sqrt()).add(beta);
            return result.toDoubleArray();
        } catch (Exception e) {
            log.error("djl layerNorm error: {}", e);
        }

        return new double[]{-1};

    }
    
    public static double[] tanh(double[] inputArr) {
    	try (NDManager manager = NDManager.newBaseManager()) {
        	NDArray array = manager.create(inputArr);
            NDArray result = array.tanh();
            System.out.println(result);
    		return result.toDoubleArray();
    	}

    }
    
    public static double[] rmsNorm(double[] inputArr) {

    	 try (NDManager manager = NDManager.newBaseManager()) {
             NDArray input = manager.create(inputArr);
             double rms = 0.0;
             NDArray positive = input.abs();
             double sum = positive.sum().getDouble();
             rms = (sum/inputArr.length) + 1.0E-5;
             rms = 1.0 / rms;
             NDArray result = input.div(rms);
             return result.toDoubleArray();
         } catch (Exception e) {
             log.error("djl layerNorm error: {}", e);
         }

         return new double[]{-1};

    }
    

    public static double[] softmax(double[] inputSArr) {
        try (NDManager manager = NDManager.newBaseManager()) {
            NDArray input = manager.create(inputSArr);
            NDArray softmax = input.softmax(0);
            return softmax.toDoubleArray();
        } catch (Exception e) {
            log.error("djl softmax error: {}", e);
        }

        return new double[]{-1};
    }

    public static double[] elu(double[] inputSArr) {
        return elu(inputSArr, 1);
    }

    public static double[] elu(double[] inputSArr, double alpha) {
        try {
            if (inputSArr == null) {
                return null;
            }
            double[] result = new double[inputSArr.length];
            for (int i = 0; i < inputSArr.length; i++) {
                if (inputSArr[i] >= 0) {
                    result[i] = inputSArr[i];
                } else {
                    result[i] = (Math.exp(inputSArr[i]) - 1) * alpha;
                }
            }
            return result;
        } catch (Exception e) {
            log.error("djl elu error: {}", e);
        }

        return new double[]{-1};
    }

    public static double[] layerNorm(double[] inputSArr, double[] alphaArr, double[] betaArr) {
        return layerNorm(inputSArr, alphaArr, betaArr, 1.0E-5);
    }


    public static double[] layerNorm(double[] inputSArr, double[] alphaArr, double[] betaArr, double eps) {
        if (inputSArr == null || alphaArr == null || betaArr == null) {
            log.error("djl layerNorm input is null");
            return new double[]{-1};
        }

        if (inputSArr.length != alphaArr.length || inputSArr.length != betaArr.length) {
            log.error("djl layerNorm length not match");
            return new double[]{-1};
        }

        try (NDManager manager = NDManager.newBaseManager()) {
            NDArray alpha = manager.create(alphaArr);
            NDArray beta = manager.create(betaArr);
            NDArray input = manager.create(inputSArr);
            NDArray mean = input.mean();
            NDArray var = input.sub(mean).square().sum().div(input.getShape().get(0));

            NDArray result = alpha.mul(input.sub(mean)).div(var.add(eps).sqrt()).add(beta);
            return result.toDoubleArray();
        } catch (Exception e) {
            log.error("djl layerNorm error: {}", e);
        }

        return new double[]{-1};
    }

    public static double[] relu(double[] inputSArr) {
        if (inputSArr == null) {
            return new double[]{-1};
        }

        try {
            double[] result = new double[inputSArr.length];
            for (int i = 0; i < inputSArr.length; i++) {
                result[i] = Math.max(0, inputSArr[i]);
            }

            return result;
        } catch (Exception e) {
            log.error("djl relu error: {}", e);
        }

        return new double[]{-1};
    }


    public static double[] leakyRelu(double[] inputSArr, double alpha) {
        if (inputSArr == null) {
            return new double[]{-1};
        }

        try (NDManager manager = NDManager.newBaseManager()) {
            NDArray inputOrigin = manager.create(inputSArr);
            NDArray sign = inputOrigin.sign();
            sign = sign.add(1).div(2);
            NDArray neg = sign.neg().add(1); //正数位1， 负数为0

            NDArray input = manager.create(inputSArr);
            NDArray inputNeg = manager.create(inputSArr);
            input = inputOrigin.mul(sign);
            inputNeg = inputOrigin.mul(neg).mul(alpha);

            input = input.add(inputNeg);
            return input.toDoubleArray();
        } catch (Exception e) {
            log.error("djl leakyRelu error: {}", e);
        }

        return new double[]{-1};
    }

    public static double[] sigmoid(double[] inputSArr) {
        if (inputSArr == null) {
            return new double[]{-1};
        }

        try (NDManager manager = NDManager.newBaseManager()) {
            NDArray inputOrigin = manager.create(inputSArr);
            NDArray neg = inputOrigin.neg();
            NDArray numerator = manager.create(1);
            NDArray result = numerator.div(neg.exp().add(1));
            return result.toDoubleArray();
        } catch (Exception e) {
            log.error("djl sigmoid error: {}", e);
        }

        return new double[]{-1};
    }

    public static double[] silu(double[] inputSArr) {
        if (inputSArr == null) {
            return new double[]{-1};
        }

        try (NDManager manager = NDManager.newBaseManager()) {
            NDArray input = manager.create(inputSArr);
            NDArray sigmoid = manager.create(sigmoid(inputSArr));
            NDArray result = input.mul(sigmoid);
            return result.toDoubleArray();
        } catch (Exception e) {
            log.error("djl sigmoid error: {}", e);
        }

        return new double[]{-1};
    }

    public static double[] selfAttention(double[] inputSArr, double[] inputRArr, int s_row, int s_col, int r_row, int r_col) {
        if (inputSArr == null || inputRArr == null || inputSArr.length != s_row * s_col || inputRArr.length != 4 * r_row * r_col) {
            log.error("djl selfAttention size not match");
            return new double[]{-1};
        }

        try (NDManager manager = NDManager.newBaseManager()) {
            NDArray x = manager.create(inputSArr).reshape(s_row, s_col);
            int defaultSize = r_col * r_row;
            NDArray w_q = manager.create(ArrayUtil.sub(inputRArr, 0, defaultSize)).reshape(r_row, r_col);
            NDArray w_k = manager.create(ArrayUtil.sub(inputRArr, defaultSize, defaultSize * 2)).reshape(r_row, r_col);
            NDArray w_v = manager.create(ArrayUtil.sub(inputRArr, defaultSize * 2, defaultSize * 3)).reshape(r_row, r_col);
            NDArray mask = manager.create(ArrayUtil.sub(inputRArr, defaultSize * 3, defaultSize * 4)).reshape(r_row, r_col);

            NDArray result = PytorchOperators.selfAttention(x, w_q, w_k, w_v, mask, 4000);
            return result != null ? result.toDoubleArray() : new double[]{-1};
        } catch (Exception e) {
            log.error("djl selfAttention error: {}", e);
        }

        return new double[]{-1};
    }

}

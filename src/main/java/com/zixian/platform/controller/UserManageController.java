package com.zixian.platform.controller;

import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.zixian.platform.entity.AuthEntity;
import com.zixian.platform.entity.PartiesEntity;
import com.zixian.platform.entity.RespondEntity;
import com.zixian.platform.entity.UserEntity;
import com.zixian.platform.mapper.DatabaseMapper;
import com.zixian.platform.mapper.UserMapper;
import com.zixian.platform.utils.exception.CustomException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import static com.zixian.platform.exception.PlatformExceptionEnum.DATA_LIST_EMPTY;

@Api(tags = "用户管理")
@RestController
@RequestMapping("/user")
public class UserManageController {
	
	@Autowired
	private UserMapper userMapper;

	@RequestMapping(value = "/login")
	public String userLogin(@RequestBody UserEntity user) {
		return JSONObject.toJSONString(user);
	}
	
	@ApiOperation(value = "新增用户",notes = "新增用户，参数为用户实体类")
	@RequestMapping(value = "/regist")
	@Transactional(propagation=Propagation.REQUIRED,noRollbackFor={DuplicateKeyException.class})
	public RespondEntity userRegist(@Valid @RequestBody PartiesEntity part) {
		try{
			part.setCreateTimestamp(System.currentTimeMillis());
			userMapper.insertParties(part);
		}catch(DuplicateKeyException e){
			return RespondEntity.ERROR("已存在该用户");
		}
		return RespondEntity.SUCCESS("succ");
	}
	
	@ApiOperation(value = "更新用户",notes = "更新用户，参数为用户实体类")
	@RequestMapping(value = "/update")
	public RespondEntity userUpdate(@Valid @RequestBody PartiesEntity part) {
		int result = userMapper.updateParties(part);
		if(result == 0)RespondEntity.ERROR("更新失败，不存在该用户");
		return RespondEntity.SUCCESS("succ");
	}
	
	@ApiOperation(value = "随机新增用户",notes = "随机模拟新增用户信息")
	@GetMapping(value = "/random_reg")
	public RespondEntity randomUserRegist() {
		PartiesEntity parties = new PartiesEntity();
		Faker FAKER = new Faker(Locale.CHINA);
		parties.setAddress(FAKER.address().fullAddress());
		parties.setAuthId(0);
		parties.setDataList("id_card");
		parties.setLogInfo("模拟生成公司信息！");
		parties.setOrganizedId(FAKER.regexify("[a-zA-Z0-9_-]{12,16}"));
		parties.setOrganizedName(FAKER.company().industry());
		parties.setPhone(FAKER.phoneNumber().cellPhone());
		userMapper.insertParties(parties);
		return RespondEntity.SUCCESS("succ");
	}
	
	@ApiOperation(value = "获取所有合作单位",notes = "所有合作单位集合")
	@GetMapping(value = "/parties")
	public RespondEntity getAllParties(@RequestParam(value = "id", required = false) String[] ids) {
		List<PartiesEntity> result = userMapper.selectAllParties();
		ObjectMapper objectMapper = new ObjectMapper();
		String str = "";
		try {
			str = objectMapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return RespondEntity.ERROR();
		}
		return RespondEntity.SUCCESS(addCountColumn(str,"parties"));
	}
	
	@ApiOperation(value = "获取所有单位名称",notes = "名称字符串用,隔开")
	@GetMapping(value = "/accounts")
	public RespondEntity getAllAccount() {
		return RespondEntity.SUCCESS(JSONObject.toJSONString(userMapper.selectAllAccount()));
	}
	
	@ApiOperation(value = "获取指定单位拥有的数据目录",notes = "目录字符串用,隔开")
	@GetMapping(value = "/lists")
	public RespondEntity getCompanyList(@RequestParam(value = "account", required = true) String account) {
		String result = userMapper.selectDataTable(account);
		String[] listResult = result.split(",");
		if(listResult.length > 0)
			return RespondEntity.SUCCESS(JSONObject.toJSONString(listResult));
		return RespondEntity.ERROR(DATA_LIST_EMPTY);
	}
	
	@ApiOperation(value = "删除用户",notes = "参数为用户id字符串")
	@GetMapping(value = "/delete")
	public RespondEntity deleteParties(@RequestParam(value = "id", required = true) String id) {
		if(userMapper.deleteParties(id) > 0)
			return RespondEntity.SUCCESS("succ");
		return RespondEntity.ERROR("该用户不存在");
	}
	
	@Autowired
	private DatabaseMapper databaseMapper;
	
	//获取所有数据库表
	@ApiOperation(value = "获取数据库表名称",notes = "所有数据库表")
	@GetMapping(value = "/getAllTable")
	public RespondEntity getAllTalble()
	{
		return RespondEntity.SUCCESS(JSONObject.toJSONString(databaseMapper.getAllTable()));
	}
	
	//获取数据库单表属性
	@ApiOperation(value = "获取字段名称",notes = "传递参数为数据库表名称")
	@GetMapping(value = "/getTableColumn")
	public RespondEntity getTableColumn(@RequestParam(value = "tableName", required = true) String tableName)
	{
		try {
			return RespondEntity.SUCCESS(JSONObject.toJSONString(databaseMapper.getDescribeByDM(tableName)));
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return RespondEntity.ERROR(90001, "无可用字段");
		}
	}
	
	//获取权限详情
	@ApiOperation(value = "获取所有的权限信息",notes = "")
	@GetMapping(value = "/getAllAuth")
	public RespondEntity getAllAuthDetails()
	{
		List<AuthEntity> result = databaseMapper.getAllAuthDetail();
		ObjectMapper objectMapper = new ObjectMapper();
		String str = "";
		try {
			str = objectMapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return RespondEntity.ERROR();
		}
		return RespondEntity.SUCCESS(str);
	}
	
	public String addCountColumn(String jsonStr, String tableName)
	{
		JSONArray array = JSON.parseArray(jsonStr);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("data", array);
		jsonObject.put("totalTableCount", databaseMapper.getTableCount(tableName));
		return jsonObject.toJSONString();
	}
	
}

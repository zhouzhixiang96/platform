package com.zixian.platform.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.StopWatch;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.zixian.platform.entity.result.OperateResult;
import com.zixian.platform.service.ExitTestLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zixian.platform.aop.HistoryAop;
import com.zixian.platform.entity.OperatorEntity;
import com.zixian.platform.entity.RespondEntity;
import com.zixian.platform.entity.WebSocketRespondEntity;
import com.zixian.platform.mapper.DatabaseMapper;
import com.zixian.platform.service.OperateManagerService;
import com.zixian.platform.utils.socket.WebSocketServer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.ApiResponse;

import static com.zixian.platform.enums.PlatformConstant.OP32;
import static com.zixian.platform.enums.PlatformConstant.OP64;
import static com.zixian.platform.utils.socket.WebSocketServer.sendProgress;

@Api(tags = "算子管理")
@RestController
@RequestMapping("/operate")
@Slf4j
public class OperateController {
	
//	@ApiOperation(value = "线性回归算子",notes = "参数为数据量")

	@Value("${zixian.cpp.filePath:/home/webDev}")
	private String cppFilePath;
	
	@Autowired
    HttpServletRequest request;

	@Value("${enable.operator.check:false}")
	private Boolean enableCheck;

	@ApiOperation(value = "线性回归算子",notes = "参数为数据数量，后续可能调整")
	@PostMapping("/linearRegression")
	public String linearRegression(@RequestBody String content, HttpServletRequest request) {
		JSONObject obj = JSON.parseObject(content);
		int count = obj.getIntValue("count");
		System.out.println(count);
//		File xFile = new File("C:\\Users\\Administrator\\Desktop\\x_train.bin");  //文件对象
//		File yFile = new File("C:\\Users\\Administrator\\Desktop\\y_train.bin");  //文件对象
		File xFile = new File(cppFilePath + "/ppmlac-accel/x_train.bin");  //文件对象
		File yFile = new File(cppFilePath + "/ppmlac-accel/y_train.bin");  //文件对象
		List<Float> inputS = new ArrayList<>();
		List<Float> inputR = new ArrayList<>();
		try(DataOutputStream xStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(xFile)));
			DataOutputStream yStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(yFile)));) {
			for(int i = 0 ; i < count ; i++)
			{
				String temp = obj.getString("x"+i);
				temp = temp.replace("[","");
				temp = temp.replace("]","");
				String[] temps = temp.split(",");
				for(int j = 0 ; j < temps.length ; j++)
				{
					xStream.writeFloat(changeEdian(Float.parseFloat(temps[j])));
					inputS.add(Float.parseFloat(temps[j]));
				}
				yStream.writeFloat(changeEdian(Float.parseFloat(obj.getString("y"+i))));
				inputR.add(Float.parseFloat(obj.getString("y"+i)));
	 		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(CollUtil.join(inputS, ","));
		System.out.println(CollUtil.join(inputR, ","));

		Process process = null;
		try {
			//执行脚本文件
//			String cmd = "/home/zzx/ppmlac-accel/test-linearRegression-SRAVX2 "+count+" 1 /home/zzx/ppmlac-accel/x_train.bin /home/zzx/ppmlac-accel/y_train.bin /home/zzx/ppmlac-accel/weight.bin";
			String cmd = cppFilePath + "/ppmlac-accel/test-linearRegression-SRAVX2 "+count+" 1 /home/zxx/ppmlac-accel/x_train.bin /home/zxx/ppmlac-accel/y_train.bin /home/zxx/ppmlac-accel/weight.bin";
			System.out.printf("开始执行命令：" + cmd);
			process = Runtime.getRuntime().exec(cmd);
			
			BufferedReader reader = new BufferedReader(
		                new InputStreamReader(process.getInputStream()));

	        String line = "";
	        while ((line = reader.readLine()) !=null){
	            System.out.println(line);
	        }
	        
			process.waitFor();
		}catch (Exception e) {
			System.out.printf(e.getMessage(),e);
		}
		finally {
			try {
				if(null != process) {
					process.destroy();
				}
			} catch (Exception e2) {
				System.out.printf(e2.getMessage(),e2);
			}
		}
		
		String fileName = "./model_weights.txt";
		int c = 1;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = reader.readLine()) != null) {
            	WebSocketServer.SendMessage("x" + c + " weight:" + line , WebSocketServer.getIdByRequest(request));
                c++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		return "";
	}
	
	@Autowired
	private DatabaseMapper databaseMapper;
	
	@ApiOperation(value = "获取所有的算子类型",notes = "返回为统一json格式")
	@GetMapping("/getAllType")
	public RespondEntity getAllType()
	{
		return RespondEntity.SUCCESS(JSONObject.toJSONString(databaseMapper.getAllOperatorType()));
	}
	
	
	@GetMapping("/getOperator")
	@ApiOperation(value = "获取某种类型的所有算子",notes = "返回为统一json格式")
	@ApiResponses(value = {
		    @ApiResponse(code = 0, message = "算子信息", response = OperatorEntity.class)
		})
	public RespondEntity getOperator(@RequestParam(value = "type", required = true) String type)
	{
		List<OperatorEntity> result = databaseMapper.selectOperatorByType(type);
		ObjectMapper objectMapper = new ObjectMapper();
		String str = "";
		try {
			str = objectMapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return RespondEntity.ERROR();
		}
		return RespondEntity.SUCCESS(str);
	}
	
	@Autowired
	OperateManagerService operateManagerService;
	@Autowired
	ExitTestLibrary exitTestLibrary;

	@GetMapping("/operatorTest")
	public RespondEntity operatorTest(@RequestParam(name="type", required = true) String type)
	{
		operateManagerService.operateCal(type);
		System.out.println("inputs:");
		int count = 1;
		for(Double l : operateManagerService.getInputS())
		{
			System.out.print(l + "\t");
			if(count == 8)
			{
				System.out.print("\n");
				count = 0;
			}
			count++;
		}
		count = 1;
		System.out.println("inputr:");
		for(Double l : operateManagerService.getInputR())
		{
			System.out.print(l + "\t");
			if(count == 8)
			{
				System.out.print("\n");
				count = 0;
			}
			count++;
		}
		return RespondEntity.SUCCESS("测试成功");
	}

	@GetMapping("/operator32Test")
	public RespondEntity operator32Test(@RequestParam(name="type", required = true) String type)
	{
        try {
            operateManagerService.operate32Cal(type);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return RespondEntity.SUCCESS("测试成功");
	}
	
	@GetMapping("/operatorCal")
	@ApiOperation(value = "开启算子计算",notes = "")
	@HistoryAop("算子测试")
	@ApiResponses(value = {
		    @ApiResponse(code = 0, message = "显示信息", response = WebSocketRespondEntity.class)
		})
	public RespondEntity operatorCal(@ApiParam("算子类型") @RequestParam(value="type", required = true) String type, @ApiParam("检查位") @RequestParam(required = false, defaultValue = "0") Integer index) throws IOException, InterruptedException
	{
		StopWatch stopWatchTotal = new StopWatch();
		stopWatchTotal.start();

		int progress = 0;
		String ip = WebSocketServer.getIdByRequest(request);
		System.out.println(ip);
		sendProgress(progress, ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("配置发起方")), ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("配置协同方")), ip);
		progress = 20;
		sendProgress(progress, ip);
		List<String> result = new ArrayList<String>();
		DecimalFormat decimalFormat = new DecimalFormat("#0.000"); // 设置要显示两位小数

		StopWatch stopWatchCal = new StopWatch();
		stopWatchCal.start();

		Object[] inputSStr;
		Object[] inputRStr;
		List<Double> inputS;
		List<Double> inputR;
		if(type.contains(OP64))
		{
			operateManagerService.operateCal(type);
			inputSStr = operateManagerService.getInputS().stream().map(decimalFormat::format).toArray();
			WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("发送方数据", 3, false, changeTo2dimArray(inputSStr,8))), ip);
			inputRStr = operateManagerService.getInputR().stream().map(decimalFormat::format).toArray();
			WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("协同方数据", 4, false, changeTo2dimArray(inputRStr,8))), ip);
			result = operateManagerService.getOutput().stream().map(decimalFormat::format).collect(Collectors.toList());
			inputS = operateManagerService.getInputS();
			inputR = operateManagerService.getInputR();
		}else if(type.contains(OP32))
		{
			try {
				OperateResult<Double, Double, Integer> operateResult = operateManagerService.operate32Cal(type);
				inputSStr = operateResult.getInputS().stream().map(decimalFormat::format).toArray();
				WebSocketServer.SendMessageJson(WebSocketRespondEntity.BUILD("发送方数据", 3, false, changeTo2dimArray(inputSStr, 8)), ip);
				inputRStr = operateResult.getInputR().stream().map(decimalFormat::format).toArray();
				WebSocketServer.SendMessageJson(WebSocketRespondEntity.BUILD("协同方数据", 4, false, changeTo2dimArray(inputRStr, 8)), ip);
				result = operateResult.getResult().stream().map(decimalFormat::format).collect(Collectors.toList());
				inputS = operateResult.getInputS();
				inputR = operateResult.getInputR();
			} catch (InterruptedException e) {
	            throw new RuntimeException(e);
	        }
		}else {
			return RespondEntity.ERROR("算子不存在");
		}
		stopWatchCal.stop();

		progress = 40;
		sendProgress(progress, ip);
		List<List<String>> confoundData = dealConfoundData(ip);
		progress = 60;
		sendProgress(progress, ip);
        sendProgress(100, ip);
		stopWatchTotal.stop();
		long totalTime = stopWatchTotal.getTotal(TimeUnit.MILLISECONDS);
		long calTime = stopWatchCal.getTotal(TimeUnit.MILLISECONDS);

		if (enableCheck) {
			double[] check = operateManagerService.check(type, inputS, inputR);
			WebSocketServer.SendMessage(JSONObject.toJSONString(new WebSocketRespondEntity("校验结果：", -1, false, CollUtil.join(Lists.newArrayList(check), StrUtil.COMMA))), ip);
			if (check.length > index) {
				log.info("ppmlac结果：{}, 对比结果：{}", result.get(index), check[index]);
				List<String> checkList = CollUtil.sub(Arrays.stream(check).boxed().map(decimalFormat::format).collect(Collectors.toList()), 0, 8);
				log.info("djl结果：[{}]", CollUtil.join(checkList, StrUtil.COMMA + StrUtil.SPACE));
				List<String> resultList = result.subList(0, checkList.size());
				Double totalMean = 0d;
				for (int i = 0; i < resultList.size(); i++) {
					Double checkValue = Double.valueOf(checkList.get(i));
					Double resultValue = Double.valueOf(resultList.get(i));
					totalMean += Math.abs((checkValue - resultValue) / checkValue);
				}
				log.info("误差为：{}", totalMean / resultList.size());
			}
		}
		log.info("输入S：[{}]", CollUtil.join(CollUtil.newArrayList(inputSStr).subList(0, 8), StrUtil.COMMA + StrUtil.SPACE));
		log.info("输入R：[{}]", CollUtil.join(CollUtil.newArrayList(inputRStr).subList(0, 8), StrUtil.COMMA + StrUtil.SPACE));
		log.info("S混淆数据：[{}]", CollUtil.join(confoundData.get(0).subList(0, 8).stream().map(s -> s.replaceAll("[S,R]:", "")).collect(Collectors.toList()), StrUtil.COMMA + StrUtil.SPACE));
		log.info("R混淆数据：[{}]", CollUtil.join(confoundData.get(1).subList(0, 8).stream().map(s -> s.replaceAll("[S,R]:", "")).collect(Collectors.toList()), StrUtil.COMMA + StrUtil.SPACE));
		log.info("计算结果为：[{}]", CollUtil.join(result.subList(0, 8), StrUtil.COMMA + StrUtil.SPACE));
		log.info("内部通信以及计算耗时：{}", calTime + "ms");
		log.info("接口总耗时：{}", totalTime + "ms");
		return RespondEntity.SUCCESS("计算完成结果为" + result);
	}

	@GetMapping("/exampleS")
	@ApiOperation(value = "发送方数据举例",notes = "")
	public RespondEntity getExampleSData(@ApiParam("数据库表名") @RequestParam(value="tableName", required = true) String tableName)
	{
		long count = databaseMapper.getTableCount(tableName);
		double random = Math.random();
        long index = (long)(random * count) ;
		List<Map<String ,Object>> result = databaseMapper.selectExampleListMap(index, tableName);
		return RespondEntity.SUCCESS(JSON.toJSONString(result));
	}
	
	@GetMapping("/exampleR")
	@ApiOperation(value = "协同方数据举例",notes = "")
	public RespondEntity getExampleRData(@ApiParam("数据库表名") @RequestParam(name="tableName", required = true) String tableName)
	{
		long count = databaseMapper.getTableCount(tableName);
		double random = Math.random();
        long index = (long)(random * count) ;
		List<Map<String ,Object>> result = databaseMapper.selectExampleListMap(index, tableName);
		return RespondEntity.SUCCESS(JSON.toJSONString(result));
	}

	@GetMapping("/testExit")
	public RespondEntity testExit() {
		Thread testThread = new Thread(() -> exitTestLibrary.myexit());
		testThread.start();
		return RespondEntity.SUCCESS(JSON.toJSONString("ok"));
	}

	public float changeEdian(float f) {
		ByteBuffer buffer = ByteBuffer.allocate(4);
    	buffer.putFloat(f);
    	buffer.order(ByteOrder.LITTLE_ENDIAN);
    	return buffer.getFloat(0);
	}
	
	public <T> List<List<T>>  changeTo2dimArray(T[] data,int num)
	{
		int totalCount = data.length;
//		T[][] result = new long[(totalCount + num -1)/num][num];
		List<List<T>> slicedArrays = new ArrayList<>(); // 存放切片后的结果列表
        for (int i = 0; i < totalCount; i += num) {
            T[] slice = Arrays.copyOfRange(data, i, i + num); // 对每段长度为num的子数组进行切片
            slicedArrays.add(Lists.newArrayList(slice));
        }
        
//        int row = 0;
//        for (long[] arr : slicedArrays) {
//        	int col = 0;
//            for (long v : arr) {
//            	result[row][col] = v;
//            	col++;
//            }
//            row++;
//        }
        return slicedArrays;
	}

	public List<List<String>> dealConfoundData(String ip)
	{
		String baseDir = System.getProperty("user.dir");
		List<String> strings = FileUtil.readLines(baseDir + "/SR.log", Charset.defaultCharset());
		strings = CollUtil.sub(strings, 0, 32);
		List<String> send0 = strings.stream().map(s -> StrUtil.split(s, StrUtil.SPACE).get(0)).collect(Collectors.toList());
		List<String> send1 = strings.stream().map(s -> StrUtil.split(s, StrUtil.SPACE).get(1)).collect(Collectors.toList());
		for(int i = 0 ; i < send0.size() ; i++)
		{
			WebSocketServer.SendMessageJson(WebSocketRespondEntity.BUILD("S混淆数据", 0, false, "发送方混淆数据：" + send0.get(i)), ip);
			WebSocketServer.SendMessageJson(WebSocketRespondEntity.BUILD("R混淆数据", 1, false, "协同方混淆数据：" + send1.get(i)), ip);
		}

		return CollUtil.newArrayList(send0, send1);
	}
}

package com.zixian.platform.controller;

import java.util.List;

import com.zixian.platform.entity.CommonArrayRespEntity;
import com.zixian.platform.entity.persistence.LoggingEventException;
import com.zixian.platform.service.LoggingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zixian.platform.entity.HistoryEntity;
import com.zixian.platform.entity.LogInfoEntity;
import com.zixian.platform.entity.RespondEntity;
import com.zixian.platform.mapper.DatabaseMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "主页数据管理")
@RestController
@RequestMapping("/database")
@Slf4j
public class DataBaseController {
	
	@Autowired
	private DatabaseMapper databaseMapper;

	private final LoggingService loggingService;

    public DataBaseController(LoggingService loggingService) {
        this.loggingService = loggingService;
    }

    //获取所有的历史记录信息
	@ApiOperation(value = "获取所有历史记录信息",notes = "")
	@GetMapping(value = "/getAllHistory")
	public RespondEntity getAllHistoryInfo()
	{
//	    Cursor<HistoryEntity> cursor =databaseMapper.selectHistoryInfo();
//	    cursor.forEach(his -> {
//	    	System.out.println(JSONObject.toJSONString(his));
//	    });
//	    if(cursor.isConsumed()){
//	    	System.out.println("final");
//	    }else {
//	    	System.out.println("err");
//	    }
		List<HistoryEntity> result = databaseMapper.selectHistoryInfo();
		ObjectMapper objectMapper = new ObjectMapper();
		String str = "";
		try {
			str = objectMapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return RespondEntity.ERROR();
		}
		return RespondEntity.SUCCESS(addCountColumn(str,"history"));
	}
	
	//获取指定数量的历史记录信息
	@ApiOperation(value = "获取指定数量历史记录信息",notes = "")
	@GetMapping(value = "/getHistory")
	public RespondEntity getHistoryInfo(@RequestParam(value = "index", required = true) long index,
										@RequestParam(value = "count", required = true) int count)
	{
		List<HistoryEntity> result = databaseMapper.selectHistoryInfoByCount(index, count);
		ObjectMapper objectMapper = new ObjectMapper();
		String str = "";
		try {
			str = objectMapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return RespondEntity.ERROR();
		}
		return RespondEntity.SUCCESS(addCountColumn(str,"history"));
	}
	
	//获取所有的日志记录信息
	@ApiOperation(value = "获取所有日志记录信息",notes = "")
	@GetMapping(value = "/getAllLog")
	public RespondEntity<CommonArrayRespEntity<LogInfoEntity>> getAllLogInfo(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "100") Integer pageSize)
	{
		try {
			return RespondEntity.SUCCESS(loggingService.selectLogByPage(page, pageSize));
		} catch (Exception e) {
			log.error("getAllLog error: {}", e);
			return RespondEntity.ERROR();
		}
	}
	
	//获取指定数量的日志记录信息
	@ApiOperation(value = "获取指定数量日志记录信息",notes = "")
	@GetMapping(value = "/getLog")
	public RespondEntity getLogInfo(@RequestParam(value = "index", required = true) int index,
									@RequestParam(value = "count", required = true) int count)
	{
		List<LogInfoEntity> result = databaseMapper.selectLogInfoByCount(index, count);
		ObjectMapper objectMapper = new ObjectMapper();
		String str = "";
		try {
			str = objectMapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			log.error("getLog error: {}", e);
			return RespondEntity.ERROR();
		}
		return RespondEntity.SUCCESS(addCountColumn(str,"log_info"));
	}

	@ApiOperation(value = "获取日志错误的堆栈信息",notes = "")
	@GetMapping(value = "/getLogStack")
	public RespondEntity<List<LoggingEventException>> getLogStack(@RequestParam Long eventId)
	{
		try {
			return RespondEntity.SUCCESS(loggingService.getLogStack(eventId));
		} catch (Exception e) {
			log.error("getLogStack error: {}", e);
			return RespondEntity.ERROR();
		}
	}
	
	//获取数据量
	@ApiOperation(value = "获取指定数据库表的数据量",notes = "")
	@GetMapping(value = "/getCount")
	public RespondEntity getTableDataCount(@RequestParam(value = "tableName", required = true) String tableName)
	{
		return RespondEntity.SUCCESS(Long.toString(databaseMapper.getTableCount(tableName)));
	}
	
	public String addCountColumn(String jsonStr, String tableName)
	{
		JSONArray array = JSON.parseArray(jsonStr);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("data", array);
		jsonObject.put("totalTableCount", databaseMapper.getTableCount(tableName));
		return jsonObject.toJSONString();
	}
}

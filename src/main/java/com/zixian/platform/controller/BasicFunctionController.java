package com.zixian.platform.controller;

import java.security.KeyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zixian.platform.service.BasicFunctionService;

@RestController
@RequestMapping("/basic")
public class BasicFunctionController {
	
	@Autowired
	private BasicFunctionService basicFunctionService;
	
	@GetMapping(value = "goodKey")
	public String createGoodKeyPair(@RequestParam(value = "account", required = true) String account) {
		//判断账号是否有效
		if(account.isEmpty())return "err";
		//生成2048位RSA密钥对
		KeyPair keyPair = basicFunctionService.generateGoodKeyPair();
		//保存密钥对到数据库
		basicFunctionService.savePublicKey(account, keyPair);
		basicFunctionService.savePrivateKey(account, keyPair);
		//返回结果
		String result = "";
		result += basicFunctionService.getPublicKeyByAccount(account).toString();
		result += "\n";
		result += basicFunctionService.getPrivateKeyByAccount(account).toString();
		return result;
		
	}
	
	@GetMapping(value = "strongKey")
	public String createStrongKeyPair(@RequestParam(value = "account", required = true) String account) {
		//判断账号是否有效
		if(account.isEmpty())return "err";
		//生成4096位RSA密钥对
		KeyPair keyPair = basicFunctionService.generateStrongKeyPair();
		//保存密钥对到数据库
		basicFunctionService.savePublicKey(account, keyPair);
		basicFunctionService.savePrivateKey(account, keyPair);
		//返回结果
		String result = "";
		result += basicFunctionService.getPublicKeyByAccount(account).toString();
		result += "\n";
		result += basicFunctionService.getPrivateKeyByAccount(account).toString();
		return result;
	}
}

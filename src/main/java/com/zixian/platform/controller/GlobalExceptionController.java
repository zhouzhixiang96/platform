package com.zixian.platform.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.exceptions.PersistenceException;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.zixian.platform.entity.RespondEntity;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionController {
	
    
        /**
     * 处理方法参数异常，如 实体类上的@NotBlank注解异常，@NotNull注解等异常信息返回。
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public RespondEntity handleMethodArgumentException(MethodArgumentNotValidException  ex){
        log.error("参数格式错误：{}", ex.getBindingResult().getFieldError().getDefaultMessage());
        return RespondEntity.ERROR(ex.getBindingResult().getFieldError().getDefaultMessage()) ;
    }

    @ExceptionHandler(PersistenceException.class)
    public RespondEntity handlePersistenceException(PersistenceException  ex){
        log.error("数据保存失败：{}，错误详情：{}", ex.getClass(), getHappenPlace(ex));
        return RespondEntity.ERROR("数据保存错误：" + ex.getLocalizedMessage()) ;
    }
    
    
        /**
     * 断言、自定义异常等处理
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public RespondEntity handleException(Exception  ex) {
        String happenPlace = getHappenPlace(ex);
        log.error("错误类型：{}，错误详情：{}", ex.getClass().getSimpleName(), happenPlace);
        return RespondEntity.ERROR(ex.getMessage());
    }

    private static String getHappenPlace(Exception ex) {
        String happenPlace = CollUtil.isNotEmpty(CollUtil.newArrayList(ex.getStackTrace()))
                ? StrUtil.subAfter(ex.getStackTrace()[0].getClassName(), StrUtil.DOT, true) + StrUtil.C_COLON + ex.getStackTrace()[0].getLineNumber()
                : "UNKNOWN";
        return happenPlace;
    }

}

package com.zixian.platform.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.KeyPair;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import com.zixian.platform.entity.result.RateLevelInfoResult;
import com.zixian.platform.enums.RateLevel;
import com.zixian.platform.service.CreditRatingModelService;
import com.zixian.platform.service.DynamicLibraryLoading;
import com.zixian.platform.service.OperateManagerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zixian.platform.aop.HistoryAop;
import com.zixian.platform.entity.MutiDimDataEntity;
import com.zixian.platform.entity.PersonEntity;
import com.zixian.platform.entity.RespondEntity;
import com.zixian.platform.entity.WebSocketRespondEntity;
import com.zixian.platform.enums.BusinessEnum;
import com.zixian.platform.enums.PlatformEnum;
import com.zixian.platform.mapper.CompanyMapper;
import com.zixian.platform.mapper.UserMapper;
import com.zixian.platform.service.PrivacyIntersectionService;
import com.zixian.platform.service.PrivateRetrievalService;
import com.zixian.platform.utils.math.MathUtils;
import com.zixian.platform.utils.math.MathUtils.LagrangeInterpolationFormula;
import com.zixian.platform.utils.paillier.CommonUtils;
import com.zixian.platform.utils.paillier.PaillierCipher;
import com.zixian.platform.utils.paillier.PaillierKeyPair;
import com.zixian.platform.utils.socket.WebSocketServer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "业务管理")
@RestController
@RequestMapping("/privacay")
public class PrivacyComputeController {
	
	@Autowired
	private PrivacyIntersectionService privacyIntersectionService;
	
	@Autowired
	private PrivateRetrievalService pir;
	
	@Autowired
	private UserMapper userMapper;

	@Value("${zixian.cpp.filePath:/home/webDev}")
	private String cppFilePath;

	@Value("${enable.operator.check:false}")
	private Boolean enableCheck;
	
	@Autowired
    HttpServletRequest request;

	private final CreditRatingModelService creditRatingModelService;
	
	private String ip = null;

    public PrivacyComputeController(@Qualifier("percentageRatingModelService") CreditRatingModelService creditRatingModelService) {
        this.creditRatingModelService = creditRatingModelService;
    }

    //隐私求交
	@ApiOperation(value = "开启隐私求交",notes = "开始隐私求交业务")
	@GetMapping(value = "/psi")
	@HistoryAop("隐私求交")
	public RespondEntity pisCalc(@ApiParam("发起方的账号") @RequestParam(value = "init", required = true) String init,
						  @ApiParam("协同方的账号") @RequestParam(value = "parti", required = true) String parti,
						  @ApiParam("发起方的数据库表名") @RequestParam(value = "iniTab", required = true) String iniTab,
						  @ApiParam("协同方的数据库表名") @RequestParam(value = "partTab", required = true) String partTab,
						  @ApiParam("发起方的数据表字段名") @RequestParam(value = "iniCol", required = true) String iniCol,
						  @ApiParam("协同方的数据表字段名") @RequestParam(value = "parCol", required = true) String parCol,
						  @ApiParam("参与计算的数量") @RequestParam(value = "count", required = true) int count) {
		if(init.isEmpty() || parti.isEmpty() || iniCol.isEmpty() || parCol.isEmpty())
		{
			return RespondEntity.ERROR(90000,"传入参数为空");
		}
		//判断权限逻辑
		int sendAuth = userMapper.selectDataAuth(init);
		//ip赋值
		String ip = WebSocketServer.getIdByRequest(request);
		sendProgress(0,ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("步骤一：判断发起方权限:" + sendAuth)), ip);
		int recAuth = userMapper.selectDataAuth(parti);
		sendProgress(8,ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("步骤二：判断协同方权限:" + recAuth)), ip);
		sendProgress(12,ip);
		if(sendAuth != PlatformEnum.AUTH_LEVEL_4.getAuthId() && (sendAuth != PlatformEnum.AUTH_LEVEL_1.getAuthId() && recAuth != PlatformEnum.AUTH_LEVEL_1.getAuthId()))
		{
			WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("权限不符合要求" + recAuth)), ip);
			return RespondEntity.ERROR(90000,"权限错误，请确定参与方与协同方权限");
		}
		//判断是否满足需求逻辑
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("步骤三：判断数据是否满足要求")), ip);
		if(!iniCol.equals(parCol))
		{
			WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("数据字段不符合要求")), ip);
			return RespondEntity.ERROR(90000,"选择双方字段不匹配，不满足业务需求");
		}
		sendProgress(25,ip);
		//初始化
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤四：初始化布谷鸟哈希表，buck = 3 ，bucksize = " + count)), ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.RECIEVESUCC("步骤四：初始化布谷鸟哈希表，buck = 3 ，bucksize = " + count)), ip);
		sendProgress(31,ip);
		privacyIntersectionService.createSelectionHash(count);
		
		privacyIntersectionService.createSourceHash(count);	
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("步骤五：配置数据在数据库路径-发起方：" + init + "字段名：" + iniCol + "协同方：" + parti + "字段名" + parCol)), ip);
		sendProgress(33,ip);
		privacyIntersectionService.setDataPath(iniTab, iniCol, partTab, parCol);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤六：开始多线程计算布谷鸟哈希")), ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.RECIEVESUCC("步骤六：开始多线程计算布谷鸟哈希")), ip);
		sendProgress(36,ip);
		privacyIntersectionService.databaseStart(count);
		sendProgress(54,ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤七-1：原始布谷鸟表结构-sourceTable:" + privacyIntersectionService.getSourceCuckooHash().getTableStruct())), ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.RECIEVESUCC("步骤七-2：原始布谷鸟表结构-selectTable:" + privacyIntersectionService.getSelectionCuckooHash().getTableStruct())), ip);
		sendProgress(60,ip);
//		privacyIntersectionService.getSourceCuckooHash().printBitArray("C:\\Users\\Administrator\\Desktop\\source_pis_hash.txt");
		privacyIntersectionService.getSourceCuckooHash().printBitArray(cppFilePath + "/ppmlacdev/src/example/source_pis_hash.txt");
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤七-3：生成加密文本-sourceTable:" + privacyIntersectionService.getSourceCuckooHash().tempS)), ip);
		sendProgress(66,ip);
//		privacyIntersectionService.getSelectionCuckooHash().printBitArray("C:\\Users\\Administrator\\Desktop\\selection_pis_hash.txt");
		privacyIntersectionService.getSelectionCuckooHash().printBitArray(cppFilePath + "/ppmlacdev/src/example/selection_pis_hash.txt");
		sendProgress(69,ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.RECIEVESUCC("步骤七-4：生成加密文本-selectTable:" + privacyIntersectionService.getSelectionCuckooHash().tempS)), ip);
		File file = new File(cppFilePath + "/ppmlacdev/src/example/result.txt");
		if(file.exists())
		{
			System.out.println("上次结果文件存在，删除" + file.delete());
		}
		boolean b = privacyIntersectionService.ot_new(privacyIntersectionService.getSourceCuckooHash().getIndexByteInfo(),privacyIntersectionService.getSelectionCuckooHash().getIndexByteInfo());
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("步骤八：调用硬件ot计算" + b)), ip);
		if(!b)return RespondEntity.ERROR(90000,"ppmlac硬件错误");
		sendProgress(85,ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("步骤九：分析计算结果")), ip);
//		int countCatch = privacyIntersectionService.catchResult(cppFilePath + "/ppmlacdev/src/example/result.txt", cppFilePath + "/ppmlacdev/src/example/resultCatch.txt");
		long countCatch = privacyIntersectionService.catchResult();
//		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("步骤十：解密得到交集结果,数量为：" + String.valueOf(countCatch))), ip);
		sendProgress(100,ip);
		return RespondEntity.SUCCESS("解密得到交集结果,数量为：" + String.valueOf(countCatch));
		
	}
	
	//匿踪查询-拉格朗日
	@ApiOperation(value = "开始拉格朗日匿踪查询",notes = "开始拉格朗日匿踪查询业务")
	@GetMapping(value = "/pir-lagrange")
	@HistoryAop("匿踪查询-拉格朗日插值法")
	public RespondEntity pirCalcLagrange(@RequestParam(value = "id", required = true) String[] ids) {
	    String result = "";
	    String sourcePath = "";
        if(System.getProperty("os.name").toLowerCase().contains("linux"))
        {
        	sourcePath = cppFilePath + "/testLagrange.txt";
        }else {
        	sourcePath = "C:\\Users\\Administrator\\Desktop\\testLagrange.txt";
        }
		KeyPair keyPair = PaillierKeyPair.generateGoodKeyPair();
		int c = 0;
		//ip赋值
		String ip = WebSocketServer.getIdByRequest(request);
		sendProgress(c,ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤一：获取密钥")), ip);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("步骤二：发送公钥" + keyPair.getPublic().toString())), ip);
		sendProgress(c += 4,ip);
		int N = 100;
	    BigInteger bXi[] = new BigInteger[N];
	    BigInteger bYi[] = new BigInteger[N];
	    int index = 0;
		BufferedReader reader;
		ByteBuffer byteBuffer;
		Charset charset = Charset.forName("UTF-8");
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.RECIEVESUCC("步骤三：对原始数据进行拉格朗日插值多项式计算")), ip);
		sendProgress(c += 2,ip);
		try {
           reader = new BufferedReader(new FileReader(sourcePath));
           String line = reader.readLine();
           while (line != null && index < N) {
	    	   	PersonEntity person = JSONObject.parseObject(line, PersonEntity.class);
	    	   	String key = person.getId();    	   	
	    	   	byteBuffer = charset.encode(key);
	            byte[] nBytesKey = byteBuffer.array();
	            int keyLen = 0;
	    	   	
	    	   	BigInteger bKey = CommonUtils.fromUnsignedByteArray(nBytesKey);
	    	   	String encryptKey = PaillierCipher.encrypt(bKey, keyPair.getPublic(),false);
	    	   	keyLen = encryptKey.length() / 2;
	    	   	nBytesKey = new byte[keyLen];
	    	   	System.arraycopy(CommonUtils.hexStringToBytes(encryptKey), 0, nBytesKey, 0, keyLen);
	    	   	bXi[index] = CommonUtils.fromUnsignedByteArray(nBytesKey);	    	   	
	        	byteBuffer = charset.encode(line);
	            byte[] nBytesValue = byteBuffer.array();
	            int valueLen = 0;
	    	   	
	    	   	BigInteger bValue = CommonUtils.fromUnsignedByteArray(nBytesValue);
	    	   	String encryptValue = PaillierCipher.encrypt(bValue, keyPair.getPublic(),false);
	    	   	valueLen = encryptValue.length() / 2;
	    	   	nBytesValue = new byte[valueLen];
	    	   	System.arraycopy(CommonUtils.hexStringToBytes(encryptValue), 0, nBytesValue, 0, valueLen);
	    	   	bYi[index] = CommonUtils.fromUnsignedByteArray(nBytesValue);
            	// read next line
            	line = reader.readLine();
            	index++;
            	if(index % 2 == 0)sendProgress(c += 1,ip);
       		}
       		reader.close();
       	} catch (IOException e) {
       		e.printStackTrace();
       	}
		LagrangeInterpolationFormula M = new MathUtils().new LagrangeInterpolationFormula();
		M.bInterpolateData(bXi, bYi);
		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤四：对请求数据进行加密处理")), ip);
		sendProgress(c += 12,ip);
		for(String id : ids) {	
			byteBuffer = charset.encode(id);
            byte[] nBytesKey = byteBuffer.array();
            int keyLen = 0;
			
    	   	BigInteger bKey = CommonUtils.fromUnsignedByteArray(nBytesKey);
    		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤四-1：原数据转化为数值" + bKey)), ip);
    		sendProgress(c += 5/ids.length,ip);
    	   	String encryptKey = PaillierCipher.encrypt(bKey, keyPair.getPublic(),false);
    	   	WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤四-2：原数据使用公钥加密" + encryptKey)), ip);
    	   	sendProgress(c += 5/ids.length,ip);
    	   	keyLen = encryptKey.length() / 2;
    	   	nBytesKey = new byte[keyLen];
    	   	System.arraycopy(CommonUtils.hexStringToBytes(encryptKey), 0, nBytesKey, 0, keyLen);
    	   	BigInteger bx = CommonUtils.fromUnsignedByteArray(nBytesKey);
    	   	WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤四-3：密文转化为数值" + bx)), ip);
    	   	sendProgress(c += 5/ids.length,ip);
    	   	if(M.fValue(bx).compareTo(BigInteger.valueOf(0)) == 0 ? true : false)
    	   	{
    	   		sendProgress(c += 10/ids.length,ip);
    	   		String s = CommonUtils.byteToHexString(CommonUtils.asUnsignedByteArray(M.hValue(bx)));
    	   		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤五：通过多项式计算初密文" + s)), ip);
    	   		BigInteger b = PaillierCipher.decrypt(s, keyPair.getPrivate());
    	   		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤六：通过持有私钥对密文结果解密" + b)), ip);
    	   		sendProgress(c += 5/ids.length,ip);
    	   		String temp = new String(CommonUtils.asUnsignedByteArray(b));
//    	   		WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("步骤七：解密后数值信息转化为明文信息" + temp)), ip);
    	   		result += temp.trim() + "\n";
    	   	}else {
//    	   		WebSocketServer.SendMessage("失败：未找到对应信息", ip);
    	   		RespondEntity.ERROR(90000,"失败：未找到对应信息");
    	   	}
		}
		sendProgress(100,ip);
		return RespondEntity.SUCCESS("解密后数值信息转化为明文信息:" + result);
	}
	
	//匿踪查询
	@ApiOperation(value = "开始基于OT的匿踪查询",notes = "参数为身份证号")
	@GetMapping(value = "/pir")
	@HistoryAop("匿踪查询-不经意传输")
	public RespondEntity pirCalc(@RequestParam(value = "id", required = false) String[] ids) {
		//初始化两方hash表
    	String ip = WebSocketServer.getIdByRequest(request);
        int size = 3000000;
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("开始基于OT的匿踪查询流程")), ip);
        pir.createHashTable(size);
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("初始化哈希表-select:" + size)), ip);
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.RECIEVESUCC("初始化哈希表-source:" + size)), ip);
    	String sourcePath = null;
    	int c = 0;
    	sendProgress(c,ip);
		//ip赋值
        if(System.getProperty("os.name").toLowerCase().contains("linux"))
        {
        	sourcePath = cppFilePath + "/testPIR.txt";
        }else {
        	sourcePath = "C:\\Users\\Administrator\\Desktop\\testPIR.txt";
        }
        sendProgress(c += 12,ip);
	    //对源数据进行布谷鸟hash 
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.RECIEVESUCC("开始源数据哈希:")), ip);
        pir.setFilePath(sourcePath);
        pir.startInsertHash();
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.RECIEVESUCC("源数据哈希结果举例:" + pir.getSourceCuckooHash().getTableStruct())), ip);
        sendProgress(c += 32,ip);
        //对选择数据进行布谷鸟hash
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("开始查询数据哈希:")), ip);
        pir.insertSelData(ids);
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.SENDERSUCC("查询数据哈希结果举例:" + pir.getSelectionCuckooHash().getTableStruct())), ip);
        sendProgress(c += 3,ip);
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("保存索引位置信息")), ip);
//        if(!pir.saveHashFile(sourceHashPath, selHashPath))return RespondEntity.ERROR(90000,"save hash err");
        sendProgress(c += 5,ip);
        //执行脚本文件
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("执行不经意传输协议")), ip);
	    if(!pir.ot(null,null))return RespondEntity.ERROR(90000,"shell ot err");
	    WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("完成不经意传输")), ip);
	    sendProgress(c += 25,ip);
	    WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC("生成加密结果文件")), ip);
//	    String result = pir.selectResult(resultFile);
	    String result = pir.selectResult();
	    sendProgress(100,ip);
	    if(result.isEmpty()) return RespondEntity.SUCCESS("未查询到当前用户");
		return RespondEntity.SUCCESS(result);
	}
	
	@ApiOperation(value = "获取所有的业务类型",notes = "返回为统一json格式")
	@GetMapping("/getAllBusiness")
	public RespondEntity getAllType()
	{
		BusinessEnum[] values = BusinessEnum.values();
		JSONArray jsonArray = new JSONArray();
        for (BusinessEnum value : values) {
        	JSONObject obj = new JSONObject();
        	obj.put("type", value.getBusinessId());
        	obj.put("name", value.getName());
        	jsonArray.add(obj);
        }
		return RespondEntity.SUCCESS(JSONObject.toJSONString(jsonArray));
	}
	
	@ApiOperation(value = "开启企业资信评分计算",notes = "返回为统一json格式")
	@GetMapping("/startCreditRating")
	@HistoryAop("企业资信评分")
	public RespondEntity<List<RateLevelInfoResult>> startEnterpriseCreditRating(@RequestParam(value = "uid", required = true) List<String> ids, HttpServletRequest request)
	{
		Map<String, RateLevelInfoResult> result = creditRatingModelService.getCompanyRating(ids);
		return RespondEntity.SUCCESS(Lists.newArrayList());
	}

	@ApiOperation(value = "开启个人资信评分计算",notes = "返回为统一json格式")
	@GetMapping("/startPersonCreditRating")
	@HistoryAop("个人资信评分")
	public RespondEntity<Map<String, RateLevelInfoResult>> startCreditRating(@RequestParam(value = "uid", required = true) List<String> ids)
	{
		Map<String, RateLevelInfoResult> result = creditRatingModelService.getRating(ids);
		return RespondEntity.SUCCESS(result);
	}
	
	@Autowired
	private CompanyMapper companyMapper;
	
	@GetMapping("/jointStatistics")
	@HistoryAop("平均薪资计算")
	public RespondEntity jointStatistics() throws InterruptedException
	{
		Thread sendThread = null;
		DynamicLibraryLoading dynamicLibraryLoading = new DynamicLibraryLoading();
		int totalCount = 100000;
		//A公司提供所有的薪资数据
		int[] companyA = companyMapper.selectSalaryByCount("company_a", totalCount);
		log.info("获取到A公司所有薪资");
		//B公司提供所有的薪资数据
		int[] companyB = companyMapper.selectSalaryByCount("company_b", totalCount);
		log.info("获取到B公司所有薪资");
		//A公司薪资求和
		long[] addA = new long[16];
		sendThread = new Thread(() -> dynamicLibraryLoading.getPPMLACSo().Sum64S(Arrays.stream(companyA).asLongStream().toArray(), totalCount, 16, 0, "127.0.0.1", 12000));
		sendThread.start();
		dynamicLibraryLoading.getPPMLACSo().Sum64R(Arrays.stream(companyB).asLongStream().toArray(), totalCount, 16, addA, 0, 12000);
		sendThread.interrupt();
		log.info("获取到A公司所有薪资总和");
		//B公司薪资求和
		long[] addB = new long[16];
		sendThread = new Thread(() -> dynamicLibraryLoading.getPPMLACSo().Sum64S(Arrays.stream(companyB).asLongStream().toArray(), totalCount, 16, 0, "127.0.0.1", 12000));
		sendThread.start();
		dynamicLibraryLoading.getPPMLACSo().Sum64R(Arrays.stream(companyA).asLongStream().toArray(), totalCount, 16, addB, 0, 12000);
		sendThread.interrupt();
		log.info("获取到B公司所有薪资总和");
		//A和B公司薪资求和
		long[] addAB = new long[totalCount];
		sendThread = new Thread(() -> dynamicLibraryLoading.getPPMLACSo().Add64S(addA, 16, 16, 0, "127.0.0.1", 12000));
		sendThread.start();
		dynamicLibraryLoading.getPPMLACSo().Add64R(addB, 16, 16, addAB, 0, 12000);
		sendThread.interrupt();
		long trueValue = 0;
		for(int i = 0 ; i < totalCount ; i++)
		{
			trueValue = trueValue + companyA[i] + companyB[i];
		}
		return RespondEntity.SUCCESS("A公司和B公司的联合平均工资为：" + (addAB[0] * 1.0)/(totalCount*2) + "真实平均工资为：" + (trueValue * 1.0)/(totalCount*2));
	}
	
	@GetMapping("/linearRegression")
	@HistoryAop("线性回归预测")
	public RespondEntity linearRegression(@RequestParam(value = "iters", required = true) int iters,
										  @RequestParam(value = "learning", required = true) float learning)
	{
		Thread sendThread = null;
		DynamicLibraryLoading dynamicLibraryLoading = new DynamicLibraryLoading();
		List<MutiDimDataEntity> listMul = companyMapper.getAllData();
		int decimal = 32;
		int row = 50;
		int col = 6;
		long[] inputS = new long[row*col + col];
		long[] inputR = new long[row];
		long[] output = new long[col];

		if (enableCheck) {
			printParameters(listMul);
		}

		//准备S方训练数据
		for(int i = 0 ; i < listMul.size() ; i++)
		{
			MutiDimDataEntity mul = listMul.get(i);
			inputS[i*col + 0] = (long)(mul.getPopulation() * (1L << decimal));
			inputS[i*col + 1] = (long)(mul.getArea() * (1L << decimal));
			inputS[i*col + 2] = (long)(mul.getSalary() * (1L << decimal));
			inputS[i*col + 3] = (long)(mul.getIlliteracy() * (1L << decimal));
			inputS[i*col + 4] = (long)(mul.getGraduate() * (1L << decimal));
			inputS[i*col + 5] = (long)(mul.getFrost() * (1L << decimal));
			inputR[i] = (long)(mul.getLinearData() * (1L << decimal));
		}
		//准备R方真实数据
		System.out.println("iters = " + iters + " learning_rate = " + learning);
		sendThread = new Thread(() -> dynamicLibraryLoading.getPPMLACSo().LinearRegressionS(inputS, row, col, iters, learning, decimal, "127.0.0.1", 12000));
		sendThread.start();
		dynamicLibraryLoading.getPPMLACSo().LinearRegressionR(inputR, row, col, decimal, output, 12000);
		sendThread.interrupt();
		double[] result = new double[col];
		for(int i = 0 ; i < col ; i++)
		{
			result[i] = (output[i] * 1.0) / (1L << decimal);
		}
		//验证
		for(MutiDimDataEntity m : listMul)
		{
			double predict = 0.0;
			predict = m.getPopulation() * result[0]
					+ m.getArea() * result[1]
					+ m.getSalary() * result[2]
					+ m.getIlliteracy() * result[3]
					+ m.getGraduate() * result[4]
					+ m.getFrost() * result[5];
			System.out.println(m.getLinearData() + "  vs  " + predict);
		}

		if (enableCheck) {
			checkMean(listMul, result);
		}

		return RespondEntity.SUCCESS(Arrays.toString(result));
	}

	private static void checkMean(List<MutiDimDataEntity> listMul, double[] result) {
		DecimalFormat decimalFormat = new DecimalFormat("#0.000"); // 设置要显示两位小数

		List<Double> inputROrigin = CollUtil.sub(listMul.stream().map(MutiDimDataEntity::getLinearData).collect(Collectors.toList()), 0, 16);
		List<Double> predictList = new ArrayList<>();
		double totalMean = 0;
		for (MutiDimDataEntity m : listMul) {
			double predict = m.getPopulation() * result[0]
					+ m.getArea() * result[1]
					+ m.getSalary() * result[2]
					+ m.getIlliteracy() * result[3]
					+ m.getGraduate() * result[4]
					+ m.getFrost() * result[5];

			predictList.add(predict);
			totalMean += Math.abs((predict - m.getLinearData()) / m.getLinearData());
		}
		log.info("误差为: {}", totalMean / listMul.size());
		log.info("期望结果为: {}", inputROrigin.stream().map(decimalFormat::format).collect(Collectors.toList()));
		log.info("实际计算结果为: {}", CollUtil.sub(predictList, 0, 16).stream().map(decimalFormat::format).collect(Collectors.toList()));
	}

	private static void printParameters(List<MutiDimDataEntity> listMul) {
		List<List<Double>> inputSOrigin;
		List<Double> inputROrigin;
		inputSOrigin = listMul.stream().map(mutiDimDataEntity -> {
			List<Double> list = new ArrayList<>();
			list.add((double) mutiDimDataEntity.getPopulation());
			list.add(mutiDimDataEntity.getArea());
			list.add(mutiDimDataEntity.getSalary());
			list.add(mutiDimDataEntity.getIlliteracy());
			list.add(mutiDimDataEntity.getGraduate());
			list.add(mutiDimDataEntity.getFrost());
			return list;
		}).collect(Collectors.toList());
		DecimalFormat decimalFormat = new DecimalFormat("#0.000"); // 设置要显示两位小数
		inputROrigin = listMul.stream().map(MutiDimDataEntity::getLinearData).collect(Collectors.toList());
		log.info("inputS: {}", inputSOrigin.stream().map(list -> list.stream().map(decimalFormat::format).collect(Collectors.toList())).collect(Collectors.toList()));
		log.info("inputR: {}", inputROrigin.stream().map(decimalFormat::format).collect(Collectors.toList()));
	}

	@GetMapping("/logisticRegression")
	@HistoryAop("逻辑回归预测")
	public RespondEntity logisticRegression(@RequestParam(value = "iters", required = true) int iters,
										  @RequestParam(value = "learning", required = true) float learning)
	{
		Thread sendThread = null;
		DynamicLibraryLoading dynamicLibraryLoading = new DynamicLibraryLoading();
		List<MutiDimDataEntity> listMul = companyMapper.getAllData();
		int decimal = 32;
		int row = 50;
		int col = 6;
		long[] inputS = new long[row*col + col + 1];
		long[] inputR = new long[row];
		long[] output = new long[col];

		if (enableCheck) {
			printParameters(listMul);
		}

		//准备S方训练数据
		for(int i = 0 ; i < listMul.size() ; i++)
		{
			MutiDimDataEntity mul = listMul.get(i);
			inputS[i*col + 0] = (long)(mul.getPopulation() * (1L << decimal));
			inputS[i*col + 1] = (long)(mul.getArea() * (1L << decimal));
			inputS[i*col + 2] = (long)(mul.getSalary() * (1L << decimal));
			inputS[i*col + 3] = (long)(mul.getIlliteracy() * (1L << decimal));
			inputS[i*col + 4] = (long)(mul.getGraduate() * (1L << decimal));
			inputS[i*col + 5] = (long)(mul.getFrost() * (1L << decimal));
			inputR[i] = (long)(mul.getLinearData() * (1L << decimal));
		}
		//准备R方真实数据
		System.out.println("iters = " + iters + " learning_rate = " + learning);
		sendThread = new Thread(() -> dynamicLibraryLoading.getPPMLACSo().LogisticRegressionS(inputS, row, col, iters, learning, decimal, "127.0.0.1", 12000));
		sendThread.start();
		dynamicLibraryLoading.getPPMLACSo().LogisticRegressionR(inputR, row, col, decimal, output, 12000);
		sendThread.interrupt();
		double[] result = new double[col + 1];
		for(int i = 0 ; i < col ; i++)
		{
			result[i] = (output[i] * 1.0) / (1L << decimal);
		}
		//验证
		for(MutiDimDataEntity m : listMul)
		{
			double predict = 0.0;
			predict = m.getPopulation() * result[0]
					+ m.getArea() * result[1]
					+ m.getSalary() * result[2]
					+ m.getIlliteracy() * result[3]
					+ m.getGraduate() * result[4]
					+ m.getFrost() * result[5]
					+ result[6];
			System.out.println(m.getLinearData() + "  vs  " + predict);
		}

		if (enableCheck) {
			checkMean(listMul, result);
		}

		return RespondEntity.SUCCESS(Arrays.toString(result));
		
	}
	
	public void sendProgress(int value,String ip) {
		JSONObject json = new JSONObject();
		json.put("progress", value);
		json.put("err", false);
		json.put("message", "进度条信息");
		WebSocketServer.SendMessage(json.toJSONString(), ip);
	}
	
}

package com.zixian.platform.controller;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.KeyPair;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import cn.hutool.core.util.StrUtil;
import com.zixian.platform.utils.socket.WebSocketServer;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zixian.platform.utils.paillier.PaillierKeyPair;
import com.zixian.platform.utils.redis.RedisUtils;
import com.alibaba.fastjson.JSONObject;
import com.zixian.platform.entity.PersonEntity;
import com.zixian.platform.entity.RespondEntity;
import com.zixian.platform.mapper.DatabaseMapper;
import com.zixian.platform.mapper.PersonMapper;
import com.zixian.platform.service.CreateHashFileService;
import com.zixian.platform.service.PrivacyIntersectionService;
import com.zixian.platform.service.PrivateRetrievalService;
import com.zixian.platform.utils.math.MathUtils;
import com.zixian.platform.utils.math.MathUtils.LagrangeInterpolationFormula;
import com.zixian.platform.utils.paillier.CommonUtils;
import com.zixian.platform.utils.paillier.PaillierCipher;

@RestController
@RequestMapping("/test")
@Slf4j
public class DemoHelloWorld {

	@Autowired
	private WebSocketServer webSocketServer;


	@Value("${zixian.cpp.filePath:/home/webDev}")
	private String cppFilePath;


    public String getCurrentTime()
	{
		// 创建一个日期对象
        Date date = new Date();
        // 创建一个格式化对象
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        // 使用格式化对象将日期格式化为字符串
        String formattedDate = sdf.format(date);
        
        return formattedDate;
	}
	
	@Autowired
	DatabaseMapper databaseMapper;
	
	//获取所有数据库表
	@GetMapping(value = "/getAllTable")
	public RespondEntity getAllTalble() throws Exception
	{
		return RespondEntity.SUCCESS(databaseMapper.getAllTable().toString());
	}
	
	//隐私求交服务类
	@Autowired
	private PrivacyIntersectionService psi;
	
	@Autowired
	private PrivateRetrievalService pir;
	
	@GetMapping(value = "/hello", name = "返回 hello world")
	public String hello(@RequestParam(value = "name", required = false) String name) {
		return String.format("hello %s!!!! ", name == null ? "World" : name);
	}
	
	@GetMapping(value = "/calc")
	public String dealShellFile()
	{
		Process process = null;
		String result = "";
		try {
			//执行脚本文件
			String cmd = cppFilePath + "/ppmlacdev/src/example/simple.sh /mnt/raid5/llama/ppmlac-xdma2-2022.2-secmul-hid-cm15-278.2MHz.xclbin";
			System.out.printf("开始执行命令：" + cmd);
			process = Runtime.getRuntime().exec(cmd);
			
			BufferedReader reader = new BufferedReader(
		                new InputStreamReader(process.getInputStream()));

	        String line = "";
	        boolean checked = false;
	        while ((line = reader.readLine()) !=null){
	            System.out.println(line);
	            if(line.indexOf("Revealed output") >= 0)checked = true;
	            if(checked)
	            {
	            	result += line + "\n";
	            }
	        }
			process.waitFor();
		}catch (Exception e) {
			System.out.printf(e.getMessage(),e);
		}
		finally {
			try {
				if(null != process) {
					process.destroy();
				}
			} catch (Exception e2) {
				System.out.printf(e2.getMessage(),e2);
			}
		}
		return result;
	}
	
	
	@GetMapping(value = "/ls")
	public void dealls()
	{
		Process process = null;
		try {
			//执行脚本文件
			String cmd = "ls";
			System.out.printf("开始执行命令：" + cmd);
			process = Runtime.getRuntime().exec(cmd);
			
			BufferedReader reader = new BufferedReader(
		                new InputStreamReader(process.getInputStream()));

	        String line = "";
	        while ((line = reader.readLine()) !=null){
	            System.out.println(line);
	        }
	        
			process.waitFor();
		}catch (Exception e) {
			System.out.printf(e.getMessage(),e);
		}
		finally {
			try {
				if(null != process) {
					process.destroy();
				}
			} catch (Exception e2) {
				System.out.printf(e2.getMessage(),e2);
			}
		}
	}
	boolean busy = false;
	//同态加密
	@GetMapping(value = "/getKeyPair")
	public String getKeyPair()
	{
		Object o;
		synchronized(redisUtil) {
			o = redisUtil.get("busy");
		}
		if(o == null)
		{
			busy = false;
		}else {
			busy = o.toString().equals("1") ? true : false;
		}
		System.out.println(busy);
		
		while(busy)
		{
			try {
				Thread.sleep(500);
				synchronized(redisUtil) {
					busy = redisUtil.get("busy").toString().equals("1") ? true : false;
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(redisUtil.set("busy", "1",60));
//		KeyPair keyPair = PaillierKeyPair.generateGoodKeyPair();//2048位密钥
		KeyPair keyPair = PaillierKeyPair.generateStrongKeyPair();//4096位密钥
		BigInteger bigInt = new BigInteger("307195495766114389629358171885231841304996376446423746299610723591464237462996107235914642374629961072359164715033668037481364452337001037424528465078226451432840616664146259067477655155066440764351451370706074546963424463146280109219640429315326437075352654483115004953463095296407643514513707060745469634244631462801092196404293");
		System.out.println(bigInt);
		String encrypt0 = PaillierCipher.encrypt(bigInt, keyPair.getPublic(),true);
		String encrypt1 = PaillierCipher.encrypt(new BigInteger("1"), keyPair.getPublic(),false);
		System.out.println("encrypt 0:" + encrypt0);
		System.out.println("encrypt 1:" + encrypt1);
		System.out.println("decrypt 0:" + PaillierCipher.decrypt(encrypt0,keyPair.getPrivate()));
		System.out.println("decrypt 1:" + PaillierCipher.decrypt(PaillierCipher.ciphertextAdd(encrypt0, encrypt1),keyPair.getPrivate()));
//		BigInteger big0 = new BigInteger(encrypt0,16);
//		BigInteger big1 = new BigInteger(encrypt1,16);
//		BigInteger result = new BigInteger("0");
//		result = result.add(big0.multiply(new BigInteger("10")));
//		result = result.add(big1.multiply(new BigInteger("20")));
//		
////		System.err.println("decrypt 0+1+1+0:" + PaillierCipher.decrypt(PaillierCipher.ciphertextAdd(PaillierCipher.ciphertextAdd(encrypt0, encrypt1), PaillierCipher.ciphertextAdd(encrypt0, encrypt1)),keyPair.getPrivate()));
//		System.out.println("          " + result.toString(16).toUpperCase());
//		System.out.println("result:" + PaillierCipher.decrypt(result.toString(16).toUpperCase(),keyPair.getPrivate()));
		redisUtil.set("busy", "0",60);
		return "public key:\n" + keyPair.getPublic() +"\nprivate key:\n" + keyPair.getPrivate();
	}
	
	//隐私求交
	@GetMapping(value = "/psi")
	public String psiCalc(@RequestParam(value = "id", required = false) String[] names)
	{
	    //初始化两方hash表
        int size = 12500003;
        psi.createSourceHash(size);
        psi.createSelectionHash(size);
        String selPath = null;
    	String selHashPath = null;
    	String sourcePath = null;
    	String sourceHashPath = null;
    	String resultFile = null;
    	String catchFile = null;
        if(System.getProperty("os.name").toLowerCase().contains("linux"))
        {
        	selPath = "./selection_data.txt";
        	selHashPath = "./selection_data_hash.txt";
        	sourcePath = "./source_data.txt";
        	sourceHashPath = "./source_data_hash.txt";
        	resultFile = "/home/zzx/ppmlacdev/src/example/result.txt";
        	catchFile = "/home/zzx/ppmlacdev/src/example/resultCatch.txt";
        }else {
        	selPath = "C:\\Users\\Administrator\\Desktop\\selection_data.txt";
        	selHashPath = "C:\\Users\\Administrator\\Desktop\\selection_data_hash.txt";
        	sourcePath = "C:\\Users\\Administrator\\Desktop\\source_data.txt";
        	sourceHashPath = "C:\\Users\\Administrator\\Desktop\\source_data_hash.txt";
        	resultFile = "C:\\Users\\Administrator\\Desktop\\result.txt";
        	catchFile = "C:\\Users\\Administrator\\Desktop\\resultCatch.txt";
        }
        //对选择数据进行布谷鸟hash
        if(names == null)
        {
        	psi.setSelectionHash(selPath);
        }else {
        	if(!psi.insertSelectionHash(names))return "insert select hash err";
        }

	    //对源数据进行布谷鸟hash 
	    psi.setSourceCuckooHash(sourcePath);

	    if(!psi.start())return "insert err";
	    
	    if(!psi.saveSelectionHashFile(selHashPath))return "save select hash err";
	    if(!psi.saveSourceHashFile(sourceHashPath))return "save source hash err";
	    
        //执行脚本文件
	    if(!psi.ot())return "shell ot err";
	    
		int catchCount = psi.catchResult(resultFile, catchFile);
		return String.valueOf(catchCount);
	}
	
	//匿踪查询
	@GetMapping(value = "/pir")
	public String pirCalc(@RequestParam(value = "id", required = false) String[] ids) {
		//初始化两方hash表
        int size = 12500000;
        pir.createHashTable(size);
    	String sourcePath = null;
    	String sourceHashPath = null;
    	String resultFile = null;
    	String selHashPath = null;
        if(System.getProperty("os.name").toLowerCase().contains("linux"))
        {
        	sourcePath = "./testPIR.txt";
        	sourceHashPath = "./source_data_hash.txt";
        	selHashPath = "./selection_data_hash.txt";
        	resultFile = "/home/zzx/ppmlacdev/src/example/result.txt";
        }else {
        	sourcePath = "C:\\Users\\Administrator\\Desktop\\testPIR.txt";
        	sourceHashPath = "C:\\Users\\Administrator\\Desktop\\source_data_hash.txt";
        	resultFile = "C:\\Users\\Administrator\\Desktop\\server data\\result.txt";
        	selHashPath = "C:\\Users\\Administrator\\Desktop\\selection_data_hash.txt";
        }

	    //对源数据进行布谷鸟hash 
        pir.setFilePath(sourcePath);
        pir.startInsertHash();
        
        //对选择数据进行布谷鸟hash
        pir.insertSelData(ids);
        
        if(!pir.saveHashFile(sourceHashPath, selHashPath))return "save hash err";
	    
        //执行脚本文件
	    if(!pir.ot())return "shell ot err";
			    
		return pir.selectResult(resultFile);
	}
	
	public String string2HexString(String str)
	{
		char[] arrResult;
    	arrResult = str.toCharArray();
    	String result = "";
    	for(int i = 0; i < arrResult.length; i++)
        {
            int asc1 = (int)(arrResult[i]);
            result += String.format("%02x", asc1);
        }
    	return result;
	}
	
	public static String hexToString(String hex) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < hex.length(); i += 2) {
            String hexSubstring = hex.substring(i, i + 2);
            int decimal = Integer.parseInt(hexSubstring, 16);
            result.append(Character.toChars(decimal));
        }
        return result.toString();
    }
	
	//匿踪查询-拉格朗日
	@GetMapping(value = "/pir-lagrange")
	public String pirCalcLagrange(@RequestParam(value = "id", required = false) String[] ids) {
	    String result = "";
//		String sourcePath = "C:\\Users\\Administrator\\Desktop\\create_data_lagrange.txt";
	    String sourcePath = "C:\\Users\\Administrator\\Desktop\\testLagrange.txt";
		KeyPair keyPair = PaillierKeyPair.generateGoodKeyPair();
		int N = 10;
	    BigInteger bXi[] = new BigInteger[N];
	    BigInteger bYi[] = new BigInteger[N];
	    int index = 0;
		BufferedReader reader;
		ByteBuffer byteBuffer;
		Charset charset = Charset.forName("UTF-8");
		try {
           reader = new BufferedReader(new FileReader(sourcePath));
           String line = reader.readLine();
           while (line != null && index < N) {
	    	   	PersonEntity person = JSONObject.parseObject(line, PersonEntity.class);
	    	   	String key = person.getId();
	    	   	
//	    	   	key = string2HexString(key);
//	    	   	int keyLen = key.length() / 2;
//	    	   	byte[] nBytesKey = new byte[keyLen];
//	    	   	System.arraycopy(CommonUtils.hexStringToBytes(key), 0, nBytesKey, 0, keyLen);
	    	   	
	    	   	byteBuffer = charset.encode(key);
	            byte[] nBytesKey = byteBuffer.array();
	            int keyLen = 0;
	    	   	
	    	   	BigInteger bKey = CommonUtils.fromUnsignedByteArray(nBytesKey);
//	    	   	System.out.println(CommonUtils.byteToHexString(CommonUtils.asUnsignedByteArray(bKey)));
	    	   	String encryptKey = PaillierCipher.encrypt(bKey, keyPair.getPublic(),false);
//	    	   	System.out.println(PaillierCipher.decrypt(encryptKey, keyPair.getPrivate()));
	    	   	keyLen = encryptKey.length() / 2;
	    	   	nBytesKey = new byte[keyLen];
	    	   	System.arraycopy(CommonUtils.hexStringToBytes(encryptKey), 0, nBytesKey, 0, keyLen);
	    	   	bXi[index] = CommonUtils.fromUnsignedByteArray(nBytesKey);
	    	   	
//	    	   	String value = string2HexString(line);
//	    	   	if(index == 0)System.out.println(value);
//	    	   	int valueLen = value.length() / 2;
//	    	   	byte[] nBytesValue = new byte[valueLen];
//	    	   	System.arraycopy(CommonUtils.hexStringToBytes(value), 0, nBytesValue, 0, valueLen);
	    	   	
	        	byteBuffer = charset.encode(line);
	            byte[] nBytesValue = byteBuffer.array();
	            int valueLen = 0;
	    	   	
	    	   	BigInteger bValue = CommonUtils.fromUnsignedByteArray(nBytesValue);
//	    	   	if(index == 0)System.out.println(bValue);
	    	   	String encryptValue = PaillierCipher.encrypt(bValue, keyPair.getPublic(),false);
//	    	   	if(index == 0)System.out.println(PaillierCipher.decrypt(encryptValue, keyPair.getPrivate()));
//	    	   	if(index == 0)System.out.println(encryptValue);
	    	   	valueLen = encryptValue.length() / 2;
	    	   	nBytesValue = new byte[valueLen];
	    	   	System.arraycopy(CommonUtils.hexStringToBytes(encryptValue), 0, nBytesValue, 0, valueLen);
//	    	   	if(index == 0)System.out.println(Arrays.toString(nBytesValue));
	    	   	bYi[index] = CommonUtils.fromUnsignedByteArray(nBytesValue);
//	    	   	if(index == 0)System.out.println(bYi[0]);
	    	   	
//	            Consumer<byte[]> consumer = (s) -> {
//	            	System.out.println(Arrays.toString(s));
//	            };
//	            consumer.accept(CommonUtils.hexStringToBytes(encrypt0));
//	            consumer.accept(nBytes1);
            	// read next line
            	line = reader.readLine();
            	index++;
       		}
       		reader.close();
       	} catch (IOException e) {
       		e.printStackTrace();
       	}
		LagrangeInterpolationFormula M = new MathUtils().new LagrangeInterpolationFormula();
		M.bInterpolateData(bXi, bYi);
		for(String id : ids) {
//			id = string2HexString(id);
//			int keyLen = id.length() / 2;
//    	   	byte[] nBytesKey = new byte[keyLen];
//    	   	System.arraycopy(CommonUtils.hexStringToBytes(id), 0, nBytesKey, 0, keyLen);
			
			byteBuffer = charset.encode(id);
            byte[] nBytesKey = byteBuffer.array();
            int keyLen = 0;
			
    	   	BigInteger bKey = CommonUtils.fromUnsignedByteArray(nBytesKey);
    	   	String encryptKey = PaillierCipher.encrypt(bKey, keyPair.getPublic(),false);
//    	   	System.out.println(PaillierCipher.decrypt(encryptKey, keyPair.getPrivate()));
    	   	keyLen = encryptKey.length() / 2;
    	   	nBytesKey = new byte[keyLen];
    	   	System.arraycopy(CommonUtils.hexStringToBytes(encryptKey), 0, nBytesKey, 0, keyLen);
    	   	BigInteger bx = CommonUtils.fromUnsignedByteArray(nBytesKey);
//    	   	System.out.println("f("+bx.intValue()+")=" + M.hValue(bx));
//            System.out.println(M.fValue(bx).compareTo(BigInteger.valueOf(0)) == 0 ? true : false);
    	   	if(M.fValue(bx).compareTo(BigInteger.valueOf(0)) == 0 ? true : false)
    	   	{
    	   		String s = CommonUtils.byteToHexString(CommonUtils.asUnsignedByteArray(M.hValue(bx)));
//    	   		System.out.println(Arrays.toString(CommonUtils.asUnsignedByteArray(M.hValue(bx))));
//    	   		System.out.println(s);
    	   		BigInteger b = PaillierCipher.decrypt(s, keyPair.getPrivate());
//    	   		System.out.println(b);
//    	   		s = CommonUtils.byteToHexString(CommonUtils.asUnsignedByteArray(b));
//    	   		System.out.println(s);
//    	   		System.out.println(hexToString(s));
//    	   		System.out.println(new String(CommonUtils.asUnsignedByteArray(b)));
    	   		String temp = new String(CommonUtils.asUnsignedByteArray(b));
    	   		result += temp.trim() + "\n";
    	   	}
		}
		return result;
	}
	
	@Resource
    private RedisUtils redisUtil;
 
    @RequestMapping("/redis/set")
    public boolean redisset(String key, String value){
        System.out.println(key+"--"+value);
        return redisUtil.set(key,value,60);
    }
 
    @RequestMapping("/redis/get")
    public Object redisget(String key){
        System.out.println(redisUtil.get(key));
        return redisUtil.get(key);
    }
 
    @RequestMapping("/redis/expire")
    public boolean expire(String key,long ExpireTime){
        return redisUtil.expire(key,ExpireTime);
    }
    
    @RequestMapping("/testThread")
    public void test()
    {
		CreateHashFileService myService1 = new CreateHashFileService("C:\\Users\\Administrator\\Desktop\\source_data.txt", "C:\\Users\\Administrator\\Desktop\\source_data_hash.txt",100000);
		CreateHashFileService myService2 = new CreateHashFileService("C:\\Users\\Administrator\\Desktop\\selection_data.txt", "C:\\Users\\Administrator\\Desktop\\selection_data_hash.txt",100000);
		myService1.setName("source thread");
		myService2.setName("selection thread");
		myService1.start();
		myService2.start();
    }
    
    @RequestMapping("/testThreadPool")
    public void testPool()
    {
    	long l = System.currentTimeMillis();
        System.out.println("service 执行----->");
        List<Future<String>> result = new ArrayList<>();
        try {
            for (int i = 0; i < 300; i++) {
                Future<String> integerFuture = psi.doAsynFunc(String.valueOf(i));
                result.add(integerFuture);
            }
            for (Future<String> future : result) {
                System.out.println(future.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("service执行出错");
        }
        System.out.println("service 结束----->" + (System.currentTimeMillis() - l));
    }
    
	@Autowired
	private PersonMapper personMapper;
    
    @GetMapping(value = "/sql")
	public String testSql(@RequestParam(value = "id", required = true) String[] ids) {
    	String result = "";
    	List<Future<String>> resultFuture = new ArrayList<>();
    	System.out.println("receive request!!!!!");
    	try {
    		for(String id : ids)
    		{
    			Future<String> integerFuture = psi.doAsynFunc(id);
    			resultFuture.add(integerFuture);
    		}
    		for (Future<String> future : resultFuture) {
    			result += JSONObject.toJSONString(personMapper.selectPersonById(future.get())) + "\n";
            }
			
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
//    	for(String id : ids)
//    	{
//			result += JSONObject.toJSONString(personMapper.selectPersonById(id)) + "\n";
//    	}
		return result;
    }

	@GetMapping("/testWS")
	public RespondEntity testWS(@RequestParam String msg, HttpServletRequest request) {
		WebSocketServer.SendMessage(StrUtil.format("收到: {}", msg), WebSocketServer.getIdByRequest(request));
		return RespondEntity.SUCCESS("success");
	}

	@GetMapping("/getAllSession")
	public RespondEntity<Map<String, String>> getAllSession() {
		return RespondEntity.SUCCESS(WebSocketServer.getSessions());
	}

	@GetMapping("/testLog")
	public RespondEntity<String> testLog() {
		log.error("這是一個測試日誌", new RuntimeException("測試錯誤"));
		return RespondEntity.SUCCESS("success");
	}
	
	@GetMapping(value = "/testScoreCard")
	public void scoreCard()
	{
		Process process = null;
		try {
			//执行脚本文件
			String cmd = "python /home/zxx/Anaconda/score_card.py";
			System.out.printf("开始执行命令：" + cmd);
			process = Runtime.getRuntime().exec(cmd);
			
			BufferedReader reader = new BufferedReader(
		                new InputStreamReader(process.getInputStream()));

	        String line = "";
	        while ((line = reader.readLine()) !=null){
	            System.out.println(line);
	        }
	        
			process.waitFor();
		}catch (Exception e) {
			System.out.printf(e.getMessage(),e);
		}
		finally {
			try {
				if(null != process) {
					process.destroy();
				}
			} catch (Exception e2) {
				System.out.printf(e2.getMessage(),e2);
			}
		}
		System.out.println("python finish");
	}
}

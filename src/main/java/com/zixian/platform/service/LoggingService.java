package com.zixian.platform.service;

import com.zixian.platform.entity.CommonArrayRespEntity;
import com.zixian.platform.entity.LogInfoEntity;
import com.zixian.platform.entity.persistence.LoggingEventException;

import java.util.List;

/**
 * author: tangzw
 * date: 2024/1/17 11:19
 * description:
 **/
public interface LoggingService {
    CommonArrayRespEntity<LogInfoEntity> selectLogByPage(Integer page, Integer pageSize);

    List<LoggingEventException> getLogStack(Long eventId);
}

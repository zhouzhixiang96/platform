package com.zixian.platform.service;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.zixian.platform.service.PpmlacSLibrary.PPLMAC_S_Middleware;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PpmlacRLibrary {

    private static String cppFilePath;
    @Value("${zixian.cpp.filePath:/home/webDev}")
    public void setCppFilePath(String value) {
        cppFilePath = value;
    }

    public interface PPLMAC_R_Middleware extends Library {

        // 加载DLL
        PpmlacRLibrary.PPLMAC_R_Middleware INSTANCE = Native.loadLibrary(cppFilePath + "/java/jdk1.8.0_381/lib/amd64/libppmlac-accelapi-R-AVX2.so", PpmlacRLibrary.PPLMAC_R_Middleware.class);

        void Add64R(double[] input, int input_size, int output_size, String ip, int port);
        void Less64R(double[] input, int input_size, int output_size, String ip, int port);

        void LinearRegressionR(double[] input, int rows, int cols, int iters, double learning_rate, String ip, int port);
        void RmsNorm64R(double[] input, int input_size, int output_size, String ip, int port);
        void Secmul64R(double[] input, int input_size, int output_size, String ip, int port);
        void Sigmoid64R(double[] input, int input_size, int output_size, String ip, int port);
        void Silu64R(double[] input, int input_size, int output_size, String ip, int port);
        void Softmax64R(double[] input, int input_size, int output_size, String ip, int port);
        void Sub64R(double[] input, int input_size, int output_size, String ip, int port);
        void Sum64R(double[] input, int input_size, int output_size, String ip, int port);
        void Tanh64R(double[] input, int input_size, int output_size, String ip, int port);
        void Xor64R(double[] input, int input_size, int output_size, String ip, int port);
        void Exp64R(double[] input, int input_size, int output_size, String ip, int port);
        void LayerNorm64R(double[] input, int input_size, int output_size, String ip, int port);
        void Elu64R(double[] input, int input_size, int output_size, String ip, int port);
        void Relu64R(double[] input, int input_size, int output_size, String ip, int port);
        void LeakyRelu64R(double[] input, int input_size, int output_size, String ip, int port);
        void SelfAtt64R(double[] input, int shape_1, int shape_2, int shape_3, int shape_4, double sqrted_scale, String ip, int port);
        void Conv2D64R(double[] input, int s_rows, int s_cols, int r_rows, int r_cols, String ip, int port);
        void BatchNorm1d64R(double[] input, int s_rows, int s_cols, int r_rows, int r_cols, String ip, int port);
        
        void MatMul64R(double[] input, int s_rows, int s_cols, int r_rows, int r_cols, String ip, int port);
        void MatMulTransed64R(double[] input, int s_rows, int s_cols, int r_rows, int r_cols, String ip, int port);

        void OTRSS(byte[] input, byte[] output, int num_messages, String ip, int port);
    }

    public PPLMAC_R_Middleware getInstance() {
        return PPLMAC_R_Middleware.INSTANCE;
    }

    public void add64R(double[] input, int input_size, int output_size, String ip, int port) {
        PPLMAC_R_Middleware.INSTANCE.Add64R(input, input_size, output_size, ip, port);
    }
    
    public void otS(byte[] input, byte[] output, int num_messages, String ip, int port) {
        PPLMAC_R_Middleware.INSTANCE.OTRSS(input, output, num_messages, ip, port);
    }
}

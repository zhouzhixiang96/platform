package com.zixian.platform.service;

import com.zixian.platform.entity.result.RateLevelInfoResult;

import java.util.List;
import java.util.Map;

/**
 * author: tangzw
 * date: 2024/2/1 15:20
 * description:
 **/
public interface CreditRatingModelService {
    Map<String, RateLevelInfoResult> getRating(List<String> ids);

    Map<String, RateLevelInfoResult> getCompanyRating(List<String> ids);
}

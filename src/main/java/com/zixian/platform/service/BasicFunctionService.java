package com.zixian.platform.service;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

public interface BasicFunctionService {
	
	//创建2048密钥对
	public KeyPair generateGoodKeyPair();
	//创建4096密钥对
	public KeyPair generateStrongKeyPair();
	//保存公钥
	public boolean savePublicKey(String account,KeyPair pKey);
	//保存私钥
	public boolean savePrivateKey(String account,KeyPair pKey);
	//数据库获取公钥
	public PublicKey getPublicKeyByAccount(String account);
	//数据库获取私钥
	public PrivateKey getPrivateKeyByAccount(String account);
	//文本转化为数值
	public BigInteger String2Big(String str);
	//数值转化为文本
	public String Big2String(BigInteger big);
	//同态密文加
	public String ciphertextAdd(String ciphertext1, String ciphertext2);
	//数值加密
	public String encrypt(BigInteger m, PublicKey publicKey, boolean bRandom);
	//密文解密
	public BigInteger decrypt(String ciphertext, PrivateKey privateKey);
	//根据数据路径进行布谷鸟hash
	public boolean startCuckooHash(String source, String dest, int count);
}

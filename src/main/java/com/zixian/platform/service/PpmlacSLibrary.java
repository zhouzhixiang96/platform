package com.zixian.platform.service;

import com.sun.jna.Library;
import com.sun.jna.Native;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PpmlacSLibrary {

    private static String cppFilePath;
    @Value("${zixian.cpp.filePath:/home/webDev}")
    public void setCppFilePath(String value) {
        cppFilePath = value;
    }

//    static {
//        Native.setProtected(true);
//        log.error("protected: {}", Native.isProtected());
//    }

    public interface PPLMAC_S_Middleware extends Library {

        // 加载DLL
        PpmlacSLibrary.PPLMAC_S_Middleware INSTANCE = Native.loadLibrary(cppFilePath + "/java/jdk1.8.0_381/lib/amd64/libppmlac-accelapi-S-AVX2.so", PpmlacSLibrary.PPLMAC_S_Middleware.class);

        void Add64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Less64S(double[] input, int input_size, double[] output, int output_size, String ip,int port);

        void LinearRegressionS(double[] input, int rows, int cols, double[] output, int iters, double learning_rate, String ip, int port);
        void RmsNorm64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Secmul64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Sigmoid64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Silu64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Softmax64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Sub64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Sum64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Tanh64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Xor64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Exp64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void LayerNorm64S(double[] input, int input_size, double[] output, int output_size, String ip,int port);
        void Elu64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void Relu64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void LeakyRelu64S(double[] input, int input_size, double[] output, int output_size, String ip, int port);
        void SelfAtt64S(double[] input, int shape_1, int shape_2, int shape_3, int shape_4, double[] output, double sqrted_scale, String ip, int port);
        void Conv2D64S(double[] input, int s_rows, int s_cols, int r_rows, int r_cols, double[] output, String ip, int port);
        void BatchNorm1d64S(double[] input, int s_rows, int s_cols, int r_rows, int r_cols, double[] output, String ip, int port);
        
        void MatMul64S(double[] input, int s_rows, int s_cols, int r_rows, int r_cols, double[] output, String ip, int port);
        void MatMulTransed64S(double[] input, int s_rows, int s_cols, int r_rows, int r_cols, double[] output, String ip, int port);

        void OTRSR(byte[] input, int num_messages, String ip, int port);
    }

    public void add64S(double[] input, int input_size, double[] output, int output_size, String ip, int port) {
        PPLMAC_S_Middleware.INSTANCE.Add64S(input, input_size, output, output_size, ip, port);
    }

    public PPLMAC_S_Middleware getInstance() {
        return PPLMAC_S_Middleware.INSTANCE;
    }
    
    public void otR(byte[] input, int num_messages, String ip, int port) {
        PPLMAC_S_Middleware.INSTANCE.OTRSR(input, num_messages, ip, port);
    }
}

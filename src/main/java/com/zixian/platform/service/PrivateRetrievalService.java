package com.zixian.platform.service;

import com.zixian.platform.utils.cuckoo.CuckooHashTable;

public interface PrivateRetrievalService {
	//创建hash表
	public void createHashTable(int size);
	
	//设置文件路径
	public void setFilePath(String path);
	
	//开始插入
	public boolean startInsertHash();
	
	//选择数据向量
	public boolean insertSelData(String[] ids);
	
	//保存hash文件
	public boolean saveHashFile(String source,String select);
	
	//脚本执行方法
	public boolean ot();
	
	//调用动态库执行ot
	public boolean ot(byte[] msg1, byte[] sel);
	
	//返回查询结果
	public String selectResult(String resultPath);
	
	//调用动态库查询
	public String selectResult();
	
	//获取数据持有方的hash缓存
	public CuckooHashTable<String> getSourceCuckooHash();
	
	//获取数据请求方的hash缓存
	public CuckooHashTable<String> getSelectionCuckooHash();
}

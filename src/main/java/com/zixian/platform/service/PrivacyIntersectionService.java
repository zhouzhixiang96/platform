package com.zixian.platform.service;

import java.util.concurrent.Future;

import com.zixian.platform.utils.cuckoo.CuckooHashTable;


public interface PrivacyIntersectionService {
	
	//创建sourceHash
	public void createSourceHash(int size);
	
	//创建selectionHash
	public void createSelectionHash(int size);

	//获取数据持有方的hash缓存
	public CuckooHashTable<String> getSourceCuckooHash();
	
	//获取数据请求方的hash缓存
	public CuckooHashTable<String> getSelectionCuckooHash();
	
	//加载原数据
	public void setSourceCuckooHash(String sourcePath);
	
	//加载选择数据
	public void setSelectionHash(String selectionPath);
	
	//加载选择数据重载
	public boolean insertSelectionHash(String[] selections);
	
	public boolean start();
	
	//保存源hash文件
	public boolean saveSourceHashFile(String sourcePath);
	
	//保存选择hash文件
	public boolean saveSelectionHashFile(String selectionPath);
	
	//删除源hash文件
	public boolean deleteSourceHashFile();
	
	//删除选择hash文件
	public boolean deleteSelectionHashFile();
	
	//执行OT
	public boolean ot();
	
	//执行模拟器OT
	public boolean ot(byte[] msg1, byte[] sel);
	
	//执行计算卡OT
	public boolean ot_new(byte[] msg1, byte[] sel);
	
	//模拟器读取结果
	public long catchResult();
	
	//读取计算结果文件
	public int catchResult(String resultFile,String saveFile);
	
	//查询指定数据
	public String selectResult(String resultFile);
	
	//多线程异步执行
	public Future<String> doAsynFunc(String i) throws InterruptedException;

	public boolean databaseStart(int count);
	
	//数据库配置赋值
	public void setDataPath(String sTable,String sColumn,String iTable,String iColumn);
}

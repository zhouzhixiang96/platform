package com.zixian.platform.service;

import java.util.List;

import com.zixian.platform.entity.result.CalRResult;
import com.zixian.platform.entity.result.CalSResult;
import com.zixian.platform.entity.result.OperateResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

public interface OperateManagerService {
	
	//封装的算子展示类
	public void operateCal(String type);
	
	CalSResult<Double> operate64CalS(String type, String ip, Integer port);

	CalRResult operate64CalR(String type, Integer port);
	
	//处理动态库打印日志
	public void dealSOLog(int lines);

	OperateResult<Double, Double, Integer> operate32Cal(String type) throws InterruptedException;
	
	public List<Double> getInputS();
	
	public List<Double> getInputR();

	public List<Double> getOutput();

	int getShowDecimal();

	CalSResult<Double> operate32CalS(String type, String ip, Integer port);

	CalRResult operate32CalR(String type, Integer port);

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	final class LessResult {
		private long bigCount;
		private long smallCount;
		private int[] inputS;
		private int[] inputR;
	}

	double[] check(String type, List<Double> inputS, List<Double> inputR);
}

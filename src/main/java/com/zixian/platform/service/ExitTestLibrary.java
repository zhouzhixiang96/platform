package com.zixian.platform.service;

import com.sun.jna.Library;
import com.sun.jna.Native;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ExitTestLibrary {

    private static String cppFilePath;
    @Value("${zixian.cpp.filePath:/home/webDev}")
    public void setCppFilePath(String value) {
        cppFilePath = value;
    }

    public interface Exit_Middleware extends Library {

        // 加载DLL
        ExitTestLibrary.Exit_Middleware INSTANCE = Native.loadLibrary(cppFilePath + "/java/jdk1.8.0_381/lib/amd64/exit.so", ExitTestLibrary.Exit_Middleware.class);

        void myexit();
    }

    public void myexit() {
        Exit_Middleware.INSTANCE.myexit();
    }
}

package com.zixian.platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zixian.platform.entity.CompanyEntity;

public interface CompanyServicePlus extends IService<CompanyEntity> {

}

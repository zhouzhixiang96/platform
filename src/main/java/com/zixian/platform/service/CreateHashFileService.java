package com.zixian.platform.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.zixian.platform.utils.cuckoo.CuckooHashTable;
import com.zixian.platform.utils.cuckoo.CuckooHashTable.HashFamily;

public class CreateHashFileService extends Thread{

	//定义散列函数集合
    HashFamily<String> hashFamily = new HashFamily<String>() {
        //根据which选取不同的散列函数
        @Override
        public int hash(String x, int which) {
            int hashVal = 0;
            int n = x.length();
            switch (which){
                case 0:{
            		int b = 378551;
            		int a = 63689;
            		for (int i = 0; i < n; i++) {
            			hashVal = hashVal * a + x.charAt(i);
            			a = a * b;
            		}
                    break;
                }
                case 1:
                    for (int i = 0; i < n; i ++){
                        hashVal = 37 * hashVal + x.charAt(i);
                    }
                    break;
                case 2:
                    for (int i = 0; i < n; i ++){
                        hashVal = 204 * hashVal + x.charAt(i);
                    }
                    break;
                case 3:
            		for (int i = 0; i < n; i++) {
            			if ((i & 1) == 0) {
            				hashVal ^= ((hashVal << 7) ^ x.charAt(i) ^ (hashVal >> 3));
            			} else {
            				hashVal ^= (~((hashVal << 11) ^ x.charAt(i) ^ (hashVal >> 5)));
            			}
            		}
                	break;
            }
            return hashVal;
        }
        //返回散列函数集合的个数
        @Override
        public int getNumberOfFunctions() {
            return 3;
        }

        @Override
        public void generateNewFunctions() {

        }
    };
    
	public String getCurrentTime()
	{
		// 创建一个日期对象
        Date date = new Date();
        // 创建一个格式化对象
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        // 使用格式化对象将日期格式化为字符串
        String formattedDate = sdf.format(date);
        
        return formattedDate;
	}
	
	private String sourPath;
	
	private String desPath;
	
	private int count;
	
	
	public CreateHashFileService(String openPath,String savePath,int total)
	{
		sourPath = openPath;
		desPath = savePath;
		count = total;
	}

	@Override
	public void run() {
		//定义布谷鸟散列
        CuckooHashTable<String> cuckooHashTable = new CuckooHashTable<String>(hashFamily, count);    
        System.out.println(getName() + " start insert hash:" + getCurrentTime());
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(sourPath),4*1024*1024);
            String line = reader.readLine();
            
            while (line != null) {
                cuckooHashTable.insert(line);
                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }  
        
        System.out.println(getName() + " finish insert hash:" + getCurrentTime());

        //打印表
        cuckooHashTable.printArray();
        System.out.println(getName() + " start save hash:" + getCurrentTime());
        System.out.println(getName() + cuckooHashTable.printBitArray(desPath));
	}
}

package com.zixian.platform.service;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import com.zixian.platform.exception.PlatformExceptionEnum;
import com.zixian.platform.utils.exception.PlatformException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sun.jna.Library;
import com.sun.jna.Native;

import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Component
@Slf4j
public class DynamicLibraryLoading {

	private static String cppFilePath;
	@Value("${zixian.cpp.filePath:/home/webDev}")
	public void setCppFilePath(String value) {
		cppFilePath = value;
	}

	public <I, R> List<R> batchRunFlattened(Supplier<List<I>> sSupplier, BiFunction<Integer, Integer, List<I>> rSupplier, int count, int step, BiFunction<List<I>, List<I>, List<R>> consumer) {
		List<List<R>> rList = batchRun(sSupplier, rSupplier, count, step, consumer);
        return rList.stream().flatMap(Collection::stream).collect(Collectors.toList());
	}

	public <I, R> List<R> batchRun(Supplier<List<I>> sSupplier, BiFunction<Integer, Integer, List<I>> rSupplier, int count, int step, BiFunction<List<I>, List<I>, R> consumer) {
		int start = 0;
		int limit = step;
		int getCount = 0;
		List<R> finalResult = Lists.newArrayList();
		List<I> inputS = sSupplier.get();
		do {
			List<I> inputR = rSupplier.apply(start, limit);
			if (CollUtil.isEmpty(inputR)) {
				break;
			}
			getCount = inputR.size();
			if (inputS.size() != inputR.size()) {
				log.error("DynamicLibraryLoading.batchRun error: size not match");
				throw new PlatformException(PlatformExceptionEnum.CALS_ERROR);
			}
			R result = consumer.apply(inputS, inputR);
			finalResult.add(result);
			start += step;
			limit += step;
		} while (getCount == step);

		return finalResult;
	}

    // 接口名字 我直接写的 DLL文件名
	public interface PPLMAC_Middleware extends Library {

		// 加载DLL
//		PPLMAC_Middleware INSTANCE = (PPLMAC_Middleware)Native.loadLibrary(DynamicLibraryLoading.cppFilePath + "/java/jdk1.8.0_381/lib/amd64/libppmlac-accelapi.so", PPLMAC_Middleware.class);
		
//		基础算子
//		void Secmul64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//      void Secmul64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
//		void Secmul32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//		void Secmul32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);
//	    void Add64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Add64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
//	    void Add32S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//      void Add32R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
//      void Sum64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Sum64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
//		void Sum32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Sum32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);
//	    void Sub64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Sub64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
//	    void Sub32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Sub32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);
//	    void Xor64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Xor64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
//	    void Xor32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Xor32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);
//		void Exp64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Exp64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
//		void Exp32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Exp32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);

		//机器学习算子
//		void BatchNorm64S(long[] input,  int s_rows,  int s_cols,  int decimal_bits, char* ip,  int port);
//	    void BatchNorm64R(long[] input,  int s_rows,  int s_cols, long[] output, int decimal_bits,  int port);
//	    void BatchNorm32S(ppmlac::accel::int32t* input,  int s_rows,  int s_cols,  int decimal_bits, char* ip,  int port);
//	    void BatchNorm32R(ppmlac::accel::int32t* input,  int s_rows,  int s_cols, ppmlac::accel::int32t* output, int decimal_bits,  int port);
//	    void Sigmoid64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Sigmoid64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
//	    void Sigmoid32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void Sigmoid32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);
//	    void RmsNorm64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void RmsNorm64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
//	    void RmsNorm32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
//	    void RmsNorm32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);
//	    void Conv2D64S(long[] input,  int s_rows,  int s_cols,  int r_rows,  int r_cols, int decimal_bits,  char* ip,  int port);
// 		void Conv2D64R(long[] input,  int s_rows,  int s_cols,  int r_rows,  int r_cols,long[] output,  int decimal_bits,  int port);
// 		void Conv2D32S(ppmlac::accel::int32t* input,  int s_rows,  int s_cols,  int r_rows,  int r_cols, int decimal_bits,  char* ip,  int port);
// 		void Conv2D32R(ppmlac::accel::int32t* input,  int s_rows,  int s_cols,  int r_rows,  int r_cols,ppmlac::accel::int32t* output,  int decimal_bits,  int port); 
// 		void SelfAtt64S(long[] input,  int s_rows,  int s_cols,  int r_rows, int r_cols,  double sqrted_scale,  int decimal_bits,  char* ip, int port);
// 		void SelfAtt64R(long[] input,  int s_rows,  int s_cols,  int r_rows, int r_cols,  double sqrted_scale, long[] output,  int decimal_bits, int port);
// 		void Tanh64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
// 		void Tanh64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
// 		void Tanh32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
// 		void Tanh32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);
// 		void Softmax64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
// 		void Softmax64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
// 		void Softmax32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
// 		void Softmax32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);
// 		void Silu64S(long[] input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
// 		void Silu64R(long[] input,  int s_size,  int r_size, long[] output,  int decimal_bits,  int port);
// 		void Silu32S(ppmlac::accel::int32t* input,  int s_size,  int r_size,  int decimal_bits,  char* ip,  int port);
// 		void Silu32R(ppmlac::accel::int32t* input,  int s_size,  int r_size, ppmlac::accel::int32t* output,  int decimal_bits,  int port);
//		void LinearRegressionS(long[] input,  int rows,  int cols,  int iters, float learning_rate,  int decimal_bits,  char* ip,  int port);
//		void LinearRegressionR(long[] input,  int rows,  int cols,  int decimal_bits,long[] output,  int port);
//		void LogisticRegressionS(long[] input,  int rows,  int cols,  int iters, float learning_rate,  int decimal_bits,  char* ip,  int port);
//		void LogisticRegressionR(long[] input,  int rows,  int cols,  int decimal_bits,long[] output,  int port);
		//矩阵算子
//	    void MatMul64S(long[] input,  int s_rows,  int s_cols,  int r_rows,  int r_cols, int decimal_bits,  char* ip,  int port);
//	    void MatMul64R(long[] input,  int s_rows,  int s_cols,  int r_rows,  int r_cols,long[] output,  int decimal_bits,  int port);
//	    void MatMul32S(ppmlac::accel::int32t* input,  int s_rows,  int s_cols,  int r_rows,  int r_cols, int decimal_bits,  char* ip,  int port);
//	    void MatMul32R(ppmlac::accel::int32t* input,  int s_rows,  int s_cols,  int r_rows,  int r_cols,ppmlac::accel::int32t* output,  int decimal_bits,  int port);
//	    void MatMulTransed64S(long[] input,  int s_rows,  int s_cols,  int r_rows, int r_cols,  int decimal_bits,  char* ip,  int port);
//	    void MatMulTransed64R(long[] input,  int s_rows,  int s_cols,  int r_rows, int r_cols, long[] output,  int decimal_bits,  int port);
//	    void MatMulTransed32S(ppmlac::accel::int32t* input,  int s_rows,  int s_cols,  int r_rows, int r_cols,  int decimal_bits,  char* ip,  int port);
//	    void MatMulTransed32R(ppmlac::accel::int32t* input,  int s_rows,  int s_cols,  int r_rows, int r_cols, ppmlac::accel::int32t* output,  int decimal_bits,  int port);

		//OT协议
//	    void OTRSS(ppmlac::accel::int8t* input,  int num_messages,  int port);
//	    void OTRSR(ppmlac::accel::int8t* input_r,  int num_messages, ppmlac::accel::int8t* output,  char* ip, int port);



		//基础算子
		void Less64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Less64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void Less32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Less32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void Add64S(long[] input,int s_size,int r_size,int decimal_bits,String ip,int port);
		void Add64R(long[] input,int s_size,int r_size,long[] output,int decimal_bits,int port);
		void Secmul64S(long[] input,int s_size,int r_size,int decimal_bits,String ip,int port);
		void Secmul64R(long[] input,int s_size,int r_size,long[] output,int decimal_bits,int port);
		void Secmul32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Secmul32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void Sum64S(long[] input,int s_size,int r_size,int decimal_bits,String ip,int port);
		void Sum64R(long[] input,int s_size,int r_size,long[] output,int decimal_bits,int port);
		void Sum32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Sum32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void Add32S(int[] input,int s_size,int r_size,int decimal_bits,String ip,int port);
		void Add32R(int[] input,int s_size,int r_size,int[] output,int decimal_bits,int port);
		void Sub64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Sub64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void Sub32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Sub32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void Xor64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Xor64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void Xor32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Xor32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void Exp64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Exp64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void Exp32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Exp32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		//机器学习算子
		void BatchNorm64S(long[] input, int s_rows, int s_cols, int decimal_bits,String ip, int port);
		void BatchNorm64R(long[] input, int s_rows, int s_cols, long[] output,int decimal_bits, int port);
		void BatchNorm32S(int[] input, int s_rows, int s_cols, int decimal_bits,String ip, int port);
		void BatchNorm32R(int[] input, int s_rows, int s_cols, int[] output,int decimal_bits, int port);
		void Sigmoid64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Sigmoid64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void Sigmoid32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Sigmoid32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void RmsNorm64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void RmsNorm64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void RmsNorm32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void RmsNorm32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void Conv2D64S(long[] input, int s_rows, int s_cols, int r_rows, int r_cols,int decimal_bits, String ip, int port);
		void Conv2D64R(long[] input, int s_rows, int s_cols, int r_rows, int r_cols,long[] output, int decimal_bits, int port);
		void Conv2D32S(int[] input, int s_rows, int s_cols, int r_rows, int r_cols,int decimal_bits, String ip, int port);
		void Conv2D32R(int[] input, int s_rows, int s_cols, int r_rows, int r_cols,int[] output, int decimal_bits, int port);
		void SelfAtt64S(long[] input, int s_rows, int s_cols, int r_rows,int r_cols, double sqrted_scale, int decimal_bits, String ip,int port);
		void SelfAtt64R(long[] input, int s_rows, int s_cols, int r_rows,int r_cols, double sqrted_scale, long[] output, int decimal_bits,int port);
		void Tanh64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Tanh64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void Tanh32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Tanh32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void Softmax64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Softmax64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void Softmax32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Softmax32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void Silu64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Silu64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void Silu32S(int[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Silu32R(int[] input, int s_size, int r_size, int[] output, int decimal_bits, int port);
		void LinearRegressionS(long[] input, int rows, int cols, int iters,float learning_rate, int decimal_bits, String ip, int port);
		void LinearRegressionR(long[] input, int rows, int cols, int decimal_bits,long[] output, int port);
		void LogisticRegressionS(long[] input, int rows, int cols, int iters,float learning_rate, int decimal_bits, String ip, int port);
		void LogisticRegressionR(long[] input, int rows, int cols, int decimal_bits,long[] output, int port);
		void Relu64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Relu64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void LeakyRelu64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void LeakyRelu64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void Elu64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void Elu64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		void LayerNorm64S(long[] input, int s_size, int r_size, int decimal_bits, String ip, int port);
		void LayerNorm64R(long[] input, int s_size, int r_size, long[] output, int decimal_bits, int port);
		//矩阵算子
		void MatMul64S(long[] input, int s_rows, int s_cols, int r_rows, int r_cols,int decimal_bits, String ip, int port);
		void MatMul64R(long[] input, int s_rows, int s_cols, int r_rows, int r_cols,long[] output, int decimal_bits, int port);
		void MatMul32S(int[] input, int s_rows, int s_cols, int r_rows, int r_cols,int decimal_bits, String ip, int port);
		void MatMul32R(int[] input, int s_rows, int s_cols, int r_rows, int r_cols,int[] output, int decimal_bits, int port);
		void MatMulTransed64S(long[] input, int s_rows, int s_cols, int r_rows,int r_cols, int decimal_bits, String ip, int port);
		void MatMulTransed64R(long[] input, int s_rows, int s_cols, int r_rows,int r_cols, long[] output, int decimal_bits, int port);
		void MatMulTransed32S(int[] input, int s_rows, int s_cols, int r_rows,int r_cols, int decimal_bits, String ip, int port);
		void MatMulTransed32R(int[] input, int s_rows, int s_cols, int r_rows,int r_cols, int[] output, int decimal_bits, int port);

		

		//OT协议
	   void OTRSS(byte[] input, int num_messages, int port);
	   void OTRSR(byte[] input_r, int num_messages, byte[] output, String ip,int port);
	}

	public PPLMAC_Middleware getPPMLACSo()
	{
//		return PPLMAC_Middleware.INSTANCE;
		return null;
	}
}

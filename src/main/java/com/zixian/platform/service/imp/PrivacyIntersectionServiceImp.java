package com.zixian.platform.service.imp;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.Future;

import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.github.javafaker.Faker;
import com.zixian.platform.entity.PersonEntity;
import com.zixian.platform.mapper.DatabaseMapper;
import com.zixian.platform.mapper.PersonMapper;
import com.zixian.platform.service.DynamicLibraryLoading;
import com.zixian.platform.service.PpmlacRLibrary;
import com.zixian.platform.service.PpmlacSLibrary;
import com.zixian.platform.service.PrivacyIntersectionService;
import com.zixian.platform.utils.cuckoo.CuckooHashTable;
import com.zixian.platform.utils.cuckoo.CuckooHashTable.HashFamily;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PrivacyIntersectionServiceImp implements PrivacyIntersectionService {

	@Value("${zixian.cpp.filePath:/home/webDev}")
	private String cppFilePath;
	
	private final int otFixCount = 64;
	
	//定义散列函数集合
    HashFamily<String> hashFamily = new HashFamily<String>() {
        //根据which选取不同的散列函数
        @Override
        public int hash(String x, int which) {
            int hashVal = 0;
            switch (which){
                case 0:{
        		int b = 378551;
        		int a = 63689;
        		int n = x.length();
        		for (int i = 0; i < n; i++) {
        			hashVal = hashVal * a + x.charAt(i);
        			a = a * b;
        		}
                break;
                }
                case 1:
                    for (int i = 0; i < x.length(); i ++){
                        hashVal = 37 * hashVal + x.charAt(i);
                    }
                    break;
                case 2:
                    for (int i = 0; i < x.length(); i ++){
                        hashVal = 204 * hashVal + x.charAt(i);
                    }
                    break;
                default:
                	break;
            }
            return hashVal;
        }
        //返回散列函数集合的个数
        @Override
        public int getNumberOfFunctions() {
            return 3;
        }

        @Override
        public void generateNewFunctions() {

        }
    };
    
    @Autowired
    private DatabaseMapper databaseMapper;
    
    class InsertThread extends Thread{
    	
    	private String filePath = null;
    	
    	private boolean isSourceFile = false;
    	
    	public InsertThread(String path,boolean sourceFile) {
    		filePath = path;
    		isSourceFile = sourceFile;
		}
    	
    	@Override
    	public void run() {
    		if((selectionTable == null && sourceTable == null) || filePath == null)return;
 	       //对选择数据进行布谷鸟hash
 	        System.out.println("start cuckooHash for " + filePath + ":" + getCurrentTime());
 	        BufferedReader reader;
 	        try {
 	            reader = new BufferedReader(new FileReader(filePath));
 	            String line = reader.readLine();
 	            while (line != null) {
 	            	if(isSourceFile)
 	            	{
 	            		sourceTable.insert(line);
 	            	}else {
 	            		selectionTable.insert(line);
 	            	}
 	                // read next line
 	                line = reader.readLine();
 	            }
 	            reader.close();
 	        } catch (IOException e) {
 	            e.printStackTrace();
 	        }
    	}
    }
    
    @Autowired
    private SqlSessionFactory sqlSessionFactory;
    
    class DatabaseInsertThread extends Thread{
    	
    	private String tableName = null;
    	
    	private String columnName = null;

		private int count;
    	
    	private boolean isSourceFile = false;
    	
    	public DatabaseInsertThread(String table,String column,boolean sourceFile, int countNum) {
    		tableName = table;
    		columnName = column;
    		isSourceFile = sourceFile;
			count = countNum;
		}
    	
    	@Override
    	public void run() {
    		if((selectionTable == null && sourceTable == null) || columnName == null)return;
 	       //对选择数据进行布谷鸟hash
    		createHashTable(isSourceFile ? sourceTable : selectionTable,tableName,columnName, count);
    	}
    	
//      @Transactional//类中调用此处不生效
    	private void createHashTable(CuckooHashTable<String> hashTable,String table,String column, int count)
    	{
//    		long count = databaseMapper.getTableRowCount(table);
//    	    Cursor<String> cursor =databaseMapper.selectColumn(table,column,count);
//    	    //Cursor 实现了迭代器接口，因此在实际使用当中，从 Cursor 取数据非常简单
//    	    cursor.forEach(s -> {
//    	    	hashTable.insert(s);
//    	    });
    		Cursor<String> cursor = null;
    		//采用此方式保持数据库链接
			SqlSession sqlSession = null;
    		try {
				sqlSession = sqlSessionFactory.openSession();
                cursor = sqlSession.getMapper(DatabaseMapper.class).selectColumn(table, column, count);
    		}
    		finally {
    	    	cursor.forEach(s -> {
    	    		System.out.println( this.getName() + "  " +s);
    		    	hashTable.insert(s);
    	    	});
				if (sqlSession != null) {
					sqlSession.close();
				}
    	    }
    	}
    }
    
	private String getCurrentTime()
	{
		// 创建一个日期对象
        Date date = new Date();
        // 创建一个格式化对象
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        // 使用格式化对象将日期格式化为字符串
        String formattedDate = sdf.format(date);
        
        return formattedDate;
	}
	
	
	private CuckooHashTable<String> sourceTable = null;
	
	private CuckooHashTable<String> selectionTable = null;
	
	private String source = null;
	
	private String selection = null;
	
	private String sourceHashPath = null;
	
	private String selectionHashPath = null;
	
	private String sourceTableName = null;
	
	private String selectionTableName = null;
	
	private String sourceColumn = null;
	
	private String selectionColumn = null;
	
	@Override
	public void createSourceHash(int size) {
		sourceTable = new CuckooHashTable<String>(hashFamily, size);
	}

	@Override
	public void createSelectionHash(int size) {
		selectionTable = new CuckooHashTable<String>(hashFamily, size);
	}

	@Override
	public CuckooHashTable<String> getSourceCuckooHash() {
		return sourceTable;
	}

	@Override
	public CuckooHashTable<String> getSelectionCuckooHash() {
		return selectionTable;
	}

	@Override
	public void setSourceCuckooHash(String sourcePath) {
//		if(sourceTable == null || sourcePath == null)return false;
//       //对源数据进行布谷鸟hash
//        System.out.println("start cuckooHash for source_data:" + getCurrentTime());
//        BufferedReader reader;
//        try {
//            reader = new BufferedReader(new FileReader(sourcePath));
//            String line = reader.readLine();
//            while (line != null) {
//            	sourceTable.insert(line);
//                // read next line
//                line = reader.readLine();
//            }
//            reader.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//            return false;
//        }
		source = sourcePath;
//		return true;
	}

	@Override
	public void setSelectionHash(String selectionPath) {
//		if(selectionTable == null || selectionPath == null)return false;
//	       //对选择数据进行布谷鸟hash
//	        System.out.println("start cuckooHash for selection_data:" + getCurrentTime());
//	        BufferedReader reader;
//	        try {
//	            reader = new BufferedReader(new FileReader(selectionPath));
//	            String line = reader.readLine();
//	            while (line != null) {
//	            	selectionTable.insert(line);
//	                // read next line
//	                line = reader.readLine();
//	            }
//	            reader.close();
//	        } catch (IOException e) {
//	            e.printStackTrace();
//	            return false;
//	        }
		selection = selectionPath;
//		return true;
	}
	
	@Override
	public boolean insertSelectionHash(String[] selections) {
		if(selectionTable == null || selections == null)return false;
		for(String s : selections)
		{
			selectionTable.insert(s);
		}
		return true;
	}

	@Override
	public boolean saveSourceHashFile(String sourcePath) {
		if(sourceTable == null || sourcePath == null)return false;
        if(!sourceTable.printBitArray(sourcePath))
        {
        	return false;
        }
        sourceHashPath = sourcePath;
		return true;
	}

	@Override
	public boolean saveSelectionHashFile(String selectionPath) {
		if(selectionTable == null || selectionPath == null)return false;
        if(!selectionTable.printBitArray(selectionPath))
        {
        	return false;
        }
        selectionHashPath = selectionPath;
		return true;
	}

	@Override
	public boolean deleteSourceHashFile() {
		File file =new File(sourceHashPath);
		return file.delete();
	}

	@Override
	public boolean deleteSelectionHashFile() {
		File file =new File(selectionHashPath);
		return file.delete();
	}

	@Override
	public boolean ot() {
		//执行脚本文件
        System.out.println("start linux shell:" + getCurrentTime());
        Process process = null;
        boolean b = true;
		try {
			//执行脚本文件
			String cmd = cppFilePath + "/ppmlacdev/src/example/ot-rs.sh /mnt/raid5/llama/ppmlac-xdma2-2022.2-secmul-hid-cm15-278.2MHz.xclbin";   //changsha 10.8
//		String cmd = "/home/zzx/ppmlacdev/src/example/ot-rs.sh /mnt/raid5/zixian/xclbins/ppmlac-xdma2-2022.2-secmul-hid-cm15-278.2MHz.xclbin";		//shanghai 151.99
			System.out.println("开始执行命令：" + cmd + getCurrentTime());
			process = Runtime.getRuntime().exec(cmd);   
			
			BufferedReader reader = new BufferedReader(
	                new InputStreamReader(process.getInputStream()));

	        String line = "";
	        while ((line = reader.readLine()) !=null){
	        	System.out.println(line);
	            if(line.indexOf("ERROR") >= 0 )
	            {
	            	b = false;
	            }
	        }
			
			process.waitFor();
		}catch (Exception e) {
			System.out.printf(e.getMessage(),e);
			return false;
		}
		finally {
			try {
				if(null != process) {
					process.destroy();
				}
			} catch (Exception e2) {
				System.out.printf(e2.getMessage(),e2);
				return false;
			}
		}
		System.out.println("结束执行命令：" + getCurrentTime());
		File file = new File(cppFilePath + "/ppmlacdev/src/example/result.txt");
		if(!file.exists())
		{
			System.out.println("ot执行失败");
			b = false;
		}
		return b;
	}
	
	private byte[] output = null;
	
	private long catchCount = 0;
	
	@Override
	public boolean ot_new(byte[] msg1, byte[] sel) {
		int fixCount = sel.length;
		while(fixCount % otFixCount != 0)
		{
			fixCount++;
		}
		int msg1Count = msg1.length;
		int selCount= sel.length;
		if(msg1Count != selCount) return false;
		Thread sendThread = null;
		byte[] inputS = new byte[fixCount*2];
		byte[] inputR = new byte[fixCount];
		output = new byte[fixCount];
		
		for(int i = 0 ; i < fixCount ; i++)
		{
			inputS[i] = 0;
		}
		
		for(int i = fixCount ; i < fixCount * 2 ; i++)
		{
			if(i - fixCount < msg1Count)
			{
				inputS[i] = msg1[i-fixCount];
			}else {
				inputS[i] = 0;
			}
			
		}
		
		for(int i = 0 ; i < selCount ; i++)
		{
			inputR[i] = sel[i];
		}
		
		for(int i = selCount ; i < fixCount ; i++)
		{
			inputR[i] = 0;
		}
		
//		int inc = 1;
//		System.out.println("inputS:");
//		for(int v : inputS)
//		{
//			System.out.print(v + "\t");
//			if(inc == 8)
//			{
//				System.out.print("\n");
//				inc = 0;
//			}
//			inc++;
//		}
		
		PpmlacSLibrary ppmlacSLibrary = new PpmlacSLibrary();
		
		sendThread = new Thread(() ->
			ppmlacSLibrary.getInstance().OTRSR(inputR, inputR.length*8, "127.0.0.1", 12000)
		);

		sendThread.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			log.error(e.getMessage());
		}
		
//		System.out.println("inputR:");
//		inc = 1;
//		for(int v : inputR)
//		{
//			System.out.print(v + "\t");
//			if(inc == 8)
//			{
//				System.out.print("\n");
//				inc = 0;
//			}
//			inc++;
//		}
		PpmlacRLibrary ppmlacRLibrary = new PpmlacRLibrary();
		
		ppmlacRLibrary.getInstance().OTRSS(inputS, output, fixCount*8, "127.0.0.1", 12000);
		
		int count = 0;
		catchCount = 0;
		int row = 0;
		int col = 8;
		System.out.println("finish ot,output size = " + output.length);
		for(byte b : output)
		{
			col--;
			if(count == 8)
			{
				count = 0;
				row++;
				col = 7;
			}
			count++;
			int pos = row*8 + 7 - col;
			if(b == 1)
			{
				String catchStr = selectionTable.value(pos);
//				System.out.println("catch:" + catchStr);
				if(catchStr == null)continue;
				if(sourceTable.contains(catchStr))
				{
					catchCount++;
				}else {
					catchStr = sourceTable.value(pos);
					if(catchStr == null)continue;
					if(selectionTable.contains(catchStr))
					{
						catchCount++;
					}
				}
			}
		}
		return true;
		
	}
	
	@Override
	public boolean ot(byte[] msg1, byte[] sel) {
		int fixCount = sel.length;
		while(fixCount % 64 != 0)
		{
			fixCount++;
		}
		int msg1Count = msg1.length;
		int selCount= sel.length;
		if(msg1Count != selCount) return false;
		DynamicLibraryLoading dynamicLibraryLoading = new DynamicLibraryLoading();
		Thread sendThread = null;
		byte[] inputS = new byte[fixCount*2];
		byte[] inputR = new byte[fixCount];
		output = new byte[fixCount];
		
		for(int i = 0 ; i < fixCount ; i++)
		{
			inputS[i] = 0;
		}
		
		for(int i = fixCount ; i < fixCount * 2 ; i++)
		{
			if(i - fixCount < msg1Count)
			{
				inputS[i] = msg1[i-fixCount];
			}else {
				inputS[i] = 0;
			}
			
		}
		
		for(int i = 0 ; i < selCount ; i++)
		{
			inputR[i] = sel[i];
		}
		
		for(int i = selCount ; i < fixCount ; i++)
		{
			inputR[i] = 0;
		}
		
//		int inc = 1;
//		System.out.println("inputS:");
//		for(int v : inputS)
//		{
//			System.out.print(v + "\t");
//			if(inc == 8)
//			{
//				System.out.print("\n");
//				inc = 0;
//			}
//			inc++;
//		}
		
		sendThread = new Thread(() ->
			dynamicLibraryLoading.getPPMLACSo().OTRSS(inputS, inputR.length*8, 12000)
		);
		sendThread.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			log.error(e.getMessage());
		}
		
//		System.out.println("inputR:");
//		inc = 1;
//		for(int v : inputR)
//		{
//			System.out.print(v + "\t");
//			if(inc == 8)
//			{
//				System.out.print("\n");
//				inc = 0;
//			}
//			inc++;
//		}
		
		dynamicLibraryLoading.getPPMLACSo().OTRSR(inputR, fixCount*8, output, "127.0.0.1", 12000);
		int count = 0;
		catchCount = 0;
		int row = 0;
		int col = 8;
		for(byte b : output)
		{
			col--;
			if(count == 8)
			{
				count = 0;
				row++;
				col = 7;
			}
			count++;
			int pos = row*8 + 7 - col;
			if(b == 1)
			{
				String catchStr = selectionTable.value(pos);
//				System.out.println("catch:" + catchStr);
				if(catchStr == null)continue;
				if(sourceTable.contains(catchStr))
				{
					catchCount++;
				}else {
					catchStr = sourceTable.value(pos);
					if(catchStr == null)continue;
					if(selectionTable.contains(catchStr))
					{
						catchCount++;
					}
				}
			}
		}
		return true;
		
	}
	
	@Override
	public long catchResult() {
		return catchCount;
	}

	@Override
	public int catchResult(String resultFile, String saveFile) {
		System.out.println("start catch：" + getCurrentTime());
    	FileOutputStream fos;
    	BufferedReader reader;
		int count = 0;
		int catchCount = 0;
		//读结果文件
        try {
            reader = new BufferedReader(new FileReader(resultFile));
	        String line = "";
	        boolean checked = false;
	        
            fos = new FileOutputStream(saveFile);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            String s = "";
           
            while ((line = reader.readLine()) !=null) {
		            if(line.indexOf("Revealed output") >= 0)checked = true;
		            if(line.indexOf("bits oblivious transfer execution") >= 0)checked = false;
		            //处理结果数据
		            if(checked)
		            {
		            	int index = line.indexOf("0x");
		           	  	if(index >= 0)
		           	  	{
		           		  String tempStr = line.substring(index);
		           		  String[] temp = tempStr.split(" ");
		           		  for(int i = 0 ; i < temp.length ; i++)
		           		  {
		           			  String tmp_inner = temp[i];
		           			  tmp_inner = tmp_inner.substring(tmp_inner.indexOf("0x") + 2);
		           			  int strLength = tmp_inner.length();
		           			  //前补0
		           			  if(strLength < 16)
		           			  {
		           				  String zero = "";
		           				  for(int j = 0 ; j < 16 - strLength ; j++)
		           				  {
		           					  zero += "0";
		           				  }
		           				  tmp_inner = zero + tmp_inner;
		           			  }
		           			  
		           			  //无需重新排序，倒序计算
		           			  for(int j = 0 ; j < 8 ; j++)
		           			  {
		           				  String temp_ii = tmp_inner.substring(j*2, j*2 + 2);
		           				  //根据寄存器内容获取实际位置
		           				  int pos = (count-1)*64 + i*8 + 7 - j;
		           				if((temp_ii.equalsIgnoreCase("31")) && pos <= 1500027)
//		           				if(!temp_ii.equalsIgnoreCase("00") && pos <= 12500003)
		           				  {
		           					String catchStr = selectionTable.value(pos);
		           					if(catchStr == null)continue;
		           					  if(sourceTable.contains(catchStr))
		           					  {
		           						catchCount++;
		           						s = "catch id index = " + String.valueOf(pos) + " value = " + selectionTable.value(pos) +"\n";
		           						bos.write(s.getBytes());
		           					  }
		           						  
		           				  }
		           			  }
		           		  }
		           	  }
		                 count++;
		            }
            }
            reader.close();
			bos.close();
			fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
		return catchCount;
	}

	@Override
	public boolean start() {
		InsertThread insertThread = new InsertThread(selection,false);
		InsertThread sourceThread = new InsertThread(source,true);
		sourceThread.start();
		insertThread.start();
		try {
			sourceThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Override
	public boolean databaseStart(int count) {
		DatabaseInsertThread insertThread = new DatabaseInsertThread(selectionTableName,selectionColumn,false, count);
		DatabaseInsertThread sourceThread = new DatabaseInsertThread(sourceTableName,sourceColumn,true, count);
		sourceThread.start();
		insertThread.start();
		try {
			sourceThread.join();
			insertThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public String selectResult(String resultFile) {
		System.out.println("start catch：" + getCurrentTime());
    	BufferedReader reader;
		int count = 0;
		String result = null;
		//读结果文件
        try {
            reader = new BufferedReader(new FileReader(resultFile));
	        String line = "";
	        boolean checked = false;
            
           
            while ((line = reader.readLine()) !=null) {
		            if(line.indexOf("Revealed output") >= 0)checked = true;
		            if(line.indexOf("bits oblivious transfer execution") >= 0)checked = false;
		            //处理结果数据
		            if(checked)
		            {
		            	int index = line.indexOf("0x");
		           	  	if(index >= 0)
		           	  	{
		           		  String tempStr = line.substring(index);
		           		  String[] temp = tempStr.split(" ");
		           		  for(int i = 0 ; i < temp.length ; i++)
		           		  {
		           			  String tmp_inner = temp[i];
		           			  tmp_inner = tmp_inner.substring(tmp_inner.indexOf("0x") + 2);
		           			  int strLength = tmp_inner.length();
		           			  //前补0
		           			  if(strLength < 16)
		           			  {
		           				  String zero = "";
		           				  for(int j = 0 ; j < 16 - strLength ; j++)
		           				  {
		           					  zero += "0";
		           				  }
		           				  tmp_inner = zero + tmp_inner;
		           			  }
		           			  
		           			  //无需重新排序，倒序计算
		           			  for(int j = 0 ; j < 8 ; j++)
		           			  {
		           				  String temp_ii = tmp_inner.substring(j*2, j*2 + 2);
		           				  //根据寄存器内容获取实际位置
		           				  int pos = (count-1)*64 + i*8 + 7 - j;
		           				if((temp_ii.equalsIgnoreCase("31")) && pos <= 12500003)
//		           				if(!temp_ii.equalsIgnoreCase("00") && pos <= 12500003)
		           				  {
	           						result = "select id index = " + String.valueOf(pos) + " value = " + sourceTable.value(pos) +"\n"; 
		           				  }
		           			  }
		           		  }
		           	  }
		                 count++;
		            }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            return result;
        }
		return result;
	}
	
	@Autowired
	private PersonMapper personMapper;
	
	@Autowired
	private PlatformTransactionManager transactionManager;
	
	// 模拟数据
	private Random random = new Random();

	private Faker FAKER = new Faker(Locale.CHINA);
	
	private int randomNumberLength = 18;
	
	@Override
	@Async("myThreadPool")
	public Future<String> doAsynFunc(String i) throws InterruptedException {
        System.out.println("@Async 执行: " + i);
		Thread.sleep(5000);
		ArrayList<PersonEntity> arrayList = new ArrayList<>();
		//实体赋值
		for(int j = 0 ; j < 100 ; j++)
		{
//			PersonEntity person = new PersonEntity();
//	    	person.setAdult(FAKER.bool().bool());
//	    	person.setAge(FAKER.number().randomDigit());
//	    	person.setBirthday(FAKER.date().birthday());
//	    	person.setHeight(FAKER.number().randomDouble(1, 150, 200));
//	    	long randomNumber = random.nextLong((long)Math.pow(10, randomNumberLength));
//	    	person.setId(String.format("%018d", randomNumber));
//	    	person.setMoney(new BigDecimal(FAKER.number().randomDigit()));
//	    	person.setName(FAKER.name().name());
//	    	person.setSex('男');
//	    	person.setWeight(FAKER.number().randomDouble(1, 100, 200));
//	    	person.setDescribe(FAKER.toString());
//	    	arrayList.add(person);
		}
		DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
        TransactionStatus status = transactionManager.getTransaction(transactionDefinition);
        try {
            personMapper.insertSplice(arrayList);
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
            throw e;
        }
//    	personMapper.insertPerson(person);
        return new AsyncResult<String>(i);
	}

	@Override
	public void setDataPath(String sTable, String sColumn, String iTable, String iColumn) {
		sourceTableName = sTable;
		sourceColumn = sColumn;
		selectionTableName = iTable;
		selectionColumn = iColumn;
	}
}

package com.zixian.platform.service.imp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.zixian.platform.entity.PersonEntity;
import com.zixian.platform.service.DynamicLibraryLoading;
import com.zixian.platform.service.PrivateRetrievalService;
import com.zixian.platform.utils.cuckoo.CuckooHashTable;
import com.zixian.platform.utils.cuckoo.CuckooHashTable.HashFamily;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PrivateRetrievalServiceImp implements PrivateRetrievalService{

	@Value("${zixian.cpp.filePath:/home/webDev}")
	private String cppFilePath;
	
	//定义散列函数集合
    HashFamily<String> hashFamily = new HashFamily<String>() {
        //根据which选取不同的散列函数
        @Override
        public int hash(String x, int which) {
            int hashVal = 0;
            switch (which){
                case 0:{
        		int b = 378551;
        		int a = 63689;
        		int n = x.length();
        		for (int i = 0; i < n; i++) {
        			hashVal = hashVal * a + x.charAt(i);
        			a = a * b;
        		}
                break;
                }
                case 1:
                    for (int i = 0; i < x.length(); i ++){
                        hashVal = 37 * hashVal + x.charAt(i);
                    }
                    break;
                case 2:
                    for (int i = 0; i < x.length(); i ++){
                        hashVal = 204 * hashVal + x.charAt(i);
                    }
                    break;
                    
            }
            return hashVal;
        }
        //返回散列函数集合的个数
        @Override
        public int getNumberOfFunctions() {
            return 3;
        }

        @Override
        public void generateNewFunctions() {

        }
    };
    
	private String getCurrentTime()
	{
		// 创建一个日期对象
        Date date = new Date();
        // 创建一个格式化对象
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        // 使用格式化对象将日期格式化为字符串
        String formattedDate = sdf.format(date);
        
        return formattedDate;
	}
    
	private CuckooHashTable<String> sourceTable = null;
	
	private CuckooHashTable<String> selectionTable = null;
	
	private String source = null;
	
	@Override
	public void createHashTable(int size) {
		sourceTable = new CuckooHashTable<String>(hashFamily, size);
		selectionTable = new CuckooHashTable<String>(hashFamily, size);
	}

	@Override
	public void setFilePath(String path) {
		source = path;
	}

	@Override
	public boolean startInsertHash() {
		if(sourceTable == null || source == null)return false;
      //对源数据进行布谷鸟hash
       System.out.println("start cuckooHash for source_data:" + getCurrentTime());
       BufferedReader reader;
       try {
           reader = new BufferedReader(new FileReader(source));
           String line = reader.readLine();
           while (line != null) {
    	   PersonEntity person = JSONObject.parseObject(line, PersonEntity.class);
           	sourceTable.insert(person.getId(),line);
            // read next line
            line = reader.readLine();
           }
           reader.close();
       } catch (IOException e) {
           e.printStackTrace();
           return false;
       }
		return true;
	}

	@Override
	public boolean insertSelData(String[] ids) {
		if(selectionTable == null)return false;
	       //对选择数据进行布谷鸟hash
        System.out.println("start cuckooHash for selection_data:" + getCurrentTime());
        for(String s : ids)
        {
        	selectionTable.insert(s);
        }
		return true;
	}

	@Override
	public boolean saveHashFile(String source,String select) {
		if(sourceTable == null || source == null || select == null || selectionTable == null)return false;
        if(!sourceTable.printBitArray(source))
        {
        	return false;
        }
        if(!selectionTable.printBitArray(select))
        {
        	return false;
        }
		return true;
	}

	@Override
	public boolean ot() {
		//执行脚本文件
        System.out.println("start linux shell:" + getCurrentTime());
        Process process = null;
        boolean b = true;
		try {
			//执行脚本文件
			String cmd = cppFilePath + "/ppmlacdev/src/example/ot-rs.sh /mnt/raid5/llama/ppmlac-xdma2-2022.2-secmul-hid-cm15-278.2MHz.xclbin";   //changsha 10.8
//		String cmd = "/home/zzx/ppmlacdev/src/example/ot-rs.sh /mnt/raid5/zixian/xclbins/ppmlac-xdma2-2022.2-secmul-hid-cm15-278.2MHz.xclbin";		//shanghai 151.99
			System.out.println("开始执行命令：" + cmd + getCurrentTime());
			process = Runtime.getRuntime().exec(cmd);   
			
			BufferedReader reader = new BufferedReader(
	                new InputStreamReader(process.getInputStream()));

	        String line = "";
	        while ((line = reader.readLine()) !=null){
	        	System.out.println(line);
	            if(line.indexOf("ERROR") >= 0 )
	            {
	            	b = false;
	            }
	        }
			
			process.waitFor();
		}catch (Exception e) {
			System.out.printf(e.getMessage(),e);
			return false;
		}
		finally {
			try {
				if(null != process) {
					process.destroy();
				}
			} catch (Exception e2) {
				System.out.printf(e2.getMessage(),e2);
				return false;
			}
		}
		System.out.println("结束执行命令：" + getCurrentTime());
		File file = new File(cppFilePath + "/ppmlacdev/src/example/result.txt");
		if(!file.exists())
		{
			System.out.println("ot执行失败");
			b = false;
		}
		return b;
	}
	
	private byte[] output = null;
	
	@Override
	public boolean ot(byte[] msg1, byte[] sel) {
		if(msg1 == null || sel == null)
		{
			msg1 = sourceTable.getIndexByteInfo();
			sel = selectionTable.getIndexByteInfo();
		}
		int msg1Count = msg1.length;
		int selCount= sel.length;
		if(msg1Count != selCount) return false;
		DynamicLibraryLoading dynamicLibraryLoading = new DynamicLibraryLoading();
		Thread sendThread = null;
		byte[] inputS = new byte[msg1Count*2];
		output = new byte[selCount];
		
		for(int i = 0 ; i < msg1Count ; i++)
		{
			inputS[i] = 0;
		}
		
		for(int i = msg1Count ; i < msg1Count * 2 ; i++)
		{
			inputS[i] = msg1[i-msg1Count];
		}
		
		sendThread = new Thread(() ->
			dynamicLibraryLoading.getPPMLACSo().OTRSS(inputS, msg1Count*8, 12000)
		);
		sendThread.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			log.error(e.getMessage());
		}
		dynamicLibraryLoading.getPPMLACSo().OTRSR(sel, selCount*8, output, "127.0.0.1", 12000);
//		int count = 1;
//		for(byte b : output)
//		{
//			System.out.print(b + "\t");
//			if(count == 8)
//			{
//				System.out.print("\n");
//				count = 0;
//			}
//			count++;
//		}
		return true;
	}

	@Override
	public String selectResult(String resultPath) {
		System.out.println("start catch：" + getCurrentTime());
    	BufferedReader reader;
		int count = 0;
		String result = null;
		//读结果文件
        try {
            reader = new BufferedReader(new FileReader(resultPath));
	        String line = "";
	        boolean checked = false;
            while ((line = reader.readLine()) !=null) {
		            if(line.indexOf("Revealed output") >= 0)checked = true;
		            if(line.indexOf("bits oblivious transfer execution") >= 0)checked = false;
		            //处理结果数据
		            if(checked)
		            {
		            	int index = line.indexOf("0x");
		           	  	if(index >= 0)
		           	  	{
		           		  String tempStr = line.substring(index);
		           		  String[] temp = tempStr.split(" ");
		           		  for(int i = 0 ; i < temp.length ; i++)
		           		  {
		           			  String tmp_inner = temp[i];
		           			  tmp_inner = tmp_inner.substring(tmp_inner.indexOf("0x") + 2);
		           			  int strLength = tmp_inner.length();
		           			  //前补0
		           			  if(strLength < 16)
		           			  {
		           				  String zero = "";
		           				  for(int j = 0 ; j < 16 - strLength ; j++)
		           				  {
		           					  zero += "0";
		           				  }
		           				  tmp_inner = zero + tmp_inner;
		           			  }
		           			  
		           			  //无需重新排序，倒序计算
		           			  for(int j = 0 ; j < 8 ; j++)
		           			  {
		           				  String temp_ii = tmp_inner.substring(j*2, j*2 + 2);
		           				  //根据寄存器内容获取实际位置
		           				  int pos = (count-1)*64 + i*8 + 7 - j;
		           				if((temp_ii.equalsIgnoreCase("31")) && pos <= 12500003)
//		           				if(!temp_ii.equalsIgnoreCase("00") && pos <= 12500003)
		           				  {
		           					String str = selectionTable.value(pos);
		           					if(str == null)continue;
		           					String tStr = sourceTable.contains(str,new PersonEntity());
		           					if(tStr != null)
		           					{
		           						result += "select id = " + str + " value = " + tStr +"\n"; 
		           					}
		           				  }
		           			  }
		           		  }
		           	  }
		                 count++;
		            }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            return result;
        }
		return result;
	}
	
	@Override
	public String selectResult() {
		String result = "";
		
		int count = 0;
		int row = 0;
		int col = 8;
		for(byte b : output)
		{
			col--;
			if(count == 8)
			{
				count = 0;
				row++;
				col = 7;
			}
			count++;
			int pos = row*8 + 7 - col;
			if(b == 1)
			{
				String catchStr = selectionTable.value(pos);
				if(catchStr == null)continue;
				String tStr = sourceTable.contains(catchStr,new PersonEntity());
				if(tStr != null)
				{
					result += "select id = " + catchStr + " value = " + tStr +"\n"; 
				}else {
					catchStr = sourceTable.value(pos);
					if(catchStr == null)continue;
					if(selectionTable.contains(catchStr))
					{
						tStr = sourceTable.contains(catchStr,new PersonEntity());
						result += "select id = " + catchStr + " value = " + tStr +"\n"; 
					}
				}
			}
		}
		
		
//		for(int i = 0 ; i < output.length ; i++)
//		{
//			if(output[i] == 1)
//			{
//				String str = selectionTable.value(i);
//				if(str == null)continue;
//				String tStr = sourceTable.contains(str,new PersonEntity());
//				if(tStr != null)
//				{
//					result += "select id = " + str + " value = " + tStr +"\n"; 
//				}
//			}
//		}
		return result;
	}

	@Override
	public CuckooHashTable<String> getSourceCuckooHash() {
		return sourceTable;
	}

	@Override
	public CuckooHashTable<String> getSelectionCuckooHash() {
		return selectionTable;
	}
}

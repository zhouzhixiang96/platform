package com.zixian.platform.service.imp;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zixian.platform.mapper.UserMapper;
import com.zixian.platform.service.BasicFunctionService;
import com.zixian.platform.service.CreateHashFileService;
import com.zixian.platform.utils.paillier.CommonUtils;
import com.zixian.platform.utils.paillier.PaillierCipher;
import com.zixian.platform.utils.paillier.PaillierKeyPair;

@Service
public class BasicFunctionServiceImp implements BasicFunctionService{
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public KeyPair generateGoodKeyPair() {
		return PaillierKeyPair.generateGoodKeyPair();
	}

	@Override
	public KeyPair generateStrongKeyPair() {
		return PaillierKeyPair.generateStrongKeyPair();
	}

	@Override
	public boolean savePublicKey(String account, KeyPair pKey) {
		try {
			userMapper.updatePublicKey(account, getPublicKeyStr(pKey.getPublic()));
		}catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean savePrivateKey(String account, KeyPair pKey) {
		try {
			userMapper.updatePrivateKey(account, getPrivateKeyStr(pKey.getPrivate()));
		}catch (Exception e) {
			return false;
		}
		return true;
	}
	
	@Override
	public PublicKey getPublicKeyByAccount(String account) {
		String publicKey = userMapper.selectPublicKey(account);
		try {
			return loadPublicKey(publicKey);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public PrivateKey getPrivateKeyByAccount(String account) {
		String privateKey = userMapper.selectPrivateKey(account);
		try {
			return loadPrivateKey(privateKey);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public BigInteger String2Big(String str) {
		ByteBuffer byteBuffer;
		Charset charset = Charset.forName("UTF-8");
		byteBuffer = charset.encode(str);
        byte[] nBytesKey = byteBuffer.array();
		return CommonUtils.fromUnsignedByteArray(nBytesKey);
	}

	@Override
	public String Big2String(BigInteger big) {
		return CommonUtils.byteToHexString(CommonUtils.asUnsignedByteArray(big));
	}

	@Override
	public String ciphertextAdd(String ciphertext1, String ciphertext2) {
		return PaillierCipher.ciphertextAdd(ciphertext1, ciphertext2);
	}

	@Override
	public String encrypt(BigInteger m, PublicKey publicKey, boolean bRandom) {
		return PaillierCipher.encrypt(m, publicKey,bRandom);
	}

	@Override
	public BigInteger decrypt(String ciphertext, PrivateKey privateKey) {
		return PaillierCipher.decrypt(ciphertext,privateKey);
	}

	@Override
	public boolean startCuckooHash(String source, String dest, int count) {
		CreateHashFileService createHashFileService = new CreateHashFileService(source, dest,count);
		createHashFileService.start();
		return true;
	}
	
    /**
     * 获取私钥字符串
     *
     * @return 当前的私钥字符串
     */
    public String getPrivateKeyStr(PrivateKey privateKey) {
        KeyFactory keyFactory = null;
        String privateKeyStr = null;
        try {
        	//将私钥对象转换为字符串
            keyFactory = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec keySpec = keyFactory.getKeySpec(privateKey, PKCS8EncodedKeySpec.class);
            byte[] buffer = keySpec.getEncoded();
            privateKeyStr = Base64.getEncoder().encodeToString(buffer);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("无此算法");
        } catch (InvalidKeySpecException e) {
            System.err.println("私钥非法");
        }
        return privateKeyStr;
    }

    /**
     * 获取公钥字符串
     *
     * @return 当前的公钥字符串
     */
    public String getPublicKeyStr(PublicKey publicKey) {
        KeyFactory keyFactory = null;
        String publicKeyStr = null;
        try {
        	//将公钥对象转换为字符串
            keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = keyFactory.getKeySpec(publicKey, X509EncodedKeySpec.class);
            byte[] buffer = keySpec.getEncoded();
            publicKeyStr = Base64.getEncoder().encodeToString(buffer);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("无此算法");
        } catch (InvalidKeySpecException e) {
            System.err.println("公钥非法");
        }
        return publicKeyStr;
    }
	
    /**
     * 从字符串中加载公钥
     *
     * @param publicKeyStr 公钥数据字符串
     * @throws Exception 加载公钥时产生的异常
     */
    private static PublicKey loadPublicKey(String publicKeyStr) throws Exception {
        PublicKey loadedPublicKey = null;
        try {
            byte[] buffer = Base64.getDecoder().decode(publicKeyStr);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            loadedPublicKey = (RSAPublicKey) keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception(e);
        } catch (InvalidKeySpecException e) {
            throw new Exception(e);
        } catch (NullPointerException e) {
            throw new Exception(e);
        }
        return loadedPublicKey;
    }

    /**
     * 从字符串中加载私钥
     *
     * @param privateKeyStr 私钥数据字符串
     * @throws Exception 加载私钥时产生的异常
     */
    private static PrivateKey loadPrivateKey(String privateKeyStr) throws Exception {
        PrivateKey loadedPrivateKey = null;
        try {
            byte[] buffer = Base64.getDecoder().decode(privateKeyStr);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            loadedPrivateKey = (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("私钥非法");
        } catch (NullPointerException e) {
            throw new Exception("私钥数据为空");
        }
        return loadedPrivateKey;
    }
}

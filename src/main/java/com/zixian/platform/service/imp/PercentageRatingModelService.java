package com.zixian.platform.service.imp;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.zixian.platform.entity.CompanyEntity;
import com.zixian.platform.entity.WebSocketRespondEntity;
import com.zixian.platform.entity.result.RateLevelInfoResult;
import com.zixian.platform.enums.RateLevel;
import com.zixian.platform.exception.PlatformExceptionEnum;
import com.zixian.platform.mapper.CompanyMapper;
import com.zixian.platform.mapper.plus.SalaryPlusMapper;
import com.zixian.platform.mapper.plus.TaxAdministrationPlusMapper;
import com.zixian.platform.service.*;
import com.zixian.platform.utils.exception.PlatformException;
import com.zixian.platform.utils.math.MathUtils;
import com.zixian.platform.utils.socket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.zixian.platform.utils.math.MathUtils.getDoubleArrFromIntArr;
import static com.zixian.platform.utils.math.MathUtils.getDoubleArrFromLongArr;
import static com.zixian.platform.utils.socket.WebSocketServer.sendProgress;

/**
 * author: tangzw
 * date: 2024/2/1 15:21
 * description:
 **/
@Service
@Slf4j
public class PercentageRatingModelService implements CreditRatingModelService {

    private final TaxAdministrationPlusMapper taxAdministrationMapper;
    private final CompanyMapper companyMapper;
    private final SalaryPlusMapper salaryPlusMapper;
    private final DynamicLibraryLoading dynamicLibraryLoading;
    private final CompanyServicePlus companyServicePlus;
    private final PpmlacSLibrary ppmlacSLibrary;
    private final PpmlacRLibrary ppmlacRLibrary;

    public PercentageRatingModelService(TaxAdministrationPlusMapper taxAdministrationMapper, CompanyMapper companyMapper, SalaryPlusMapper salaryPlusMapper, DynamicLibraryLoading dynamicLibraryLoading, CompanyServicePlus companyServicePlus, PpmlacSLibrary ppmlacSLibrary, PpmlacRLibrary ppmlacRLibrary) {
        this.taxAdministrationMapper = taxAdministrationMapper;
        this.companyMapper = companyMapper;
        this.salaryPlusMapper = salaryPlusMapper;
        this.dynamicLibraryLoading = dynamicLibraryLoading;
        this.companyServicePlus = companyServicePlus;
        this.ppmlacSLibrary = ppmlacSLibrary;
        this.ppmlacRLibrary = ppmlacRLibrary;
    }

    @Autowired
    private HttpServletRequest request;

    @Override
    public Map<String, RateLevelInfoResult> getRating(List<String> ids) {
        if (CollUtil.isEmpty(ids)) {
            return Maps.newHashMap();
        }
        int totalCount = 100000;
        int[] companyBAll = companyMapper.selectSalaryByCount("company_b", totalCount);
        int[] companyAAll = companyMapper.selectSalaryByCount("company_a", totalCount);
        log.info("获取到B公司所有薪资");
        int[] result = new int[totalCount];

        return ids.stream().collect(Collectors.toMap(id -> id, id -> {
            int salary = companyMapper.selectSalaryById("company_a", id);
            int[] companyA = new int[totalCount];
            Arrays.fill(companyA, salary);
            Thread thread = new Thread(() -> dynamicLibraryLoading.getPPMLACSo().Less32S(companyBAll, totalCount, totalCount, 0, "127.0.0.1", 9000));
            thread.start();
            dynamicLibraryLoading.getPPMLACSo().Less32R(companyA, totalCount, totalCount, result, 0, 9000);
            long bBigCount = Arrays.stream(result).filter(r -> r == 0).count(); //b公司大于当前用户的人数
            long aBigCount = MathUtils.getBigCount(salary, companyAAll); //本公司大于当前用户人数，本地计算
            int percentage = new BigDecimal(bBigCount + aBigCount).divide(new BigDecimal(totalCount * 2), 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")).intValue();
            return RateLevelInfoResult.builder()
                    .percentage(percentage)
                    .level(RateLevel.getByPercentage(percentage))
                    .build();
        }));
    }

    @Override
    public Map<String, RateLevelInfoResult> getCompanyRating(List<String> ids) {
        String ip = WebSocketServer.getIdByRequest(request);
        if (CollUtil.isEmpty(ids)) {
            WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("发送方错误", 0, true, "请输入需要查询的数据")), ip);
            sendProgress(100, ip);
            return Maps.newHashMap();
        }

        Map<String, Long> scoreMap = getTotalScore(ids);
        if (CollUtil.isEmpty(scoreMap)) {
            WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("发送方错误", 1, true, "未查询到有效信息")), ip);
            sendProgress(100, ip);
            return Maps.newHashMap();
        }

        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤一", 0, false, "步骤一: 发起方获取原始数据")), ip);

        sendProgress(20, ip);

        int count = getTotalCompanyCount();
        final int step = Math.min(count, 256);

        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤二", 0, false, "步骤二: 企业资信评分模型计算分数")), ip);
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤三", 0, false, "步骤三: 发起方准备发起匿踪查询")), ip);
        Map<String, RateLevelInfoResult> resultMap = Maps.newLinkedHashMap();
        AtomicInteger currentTime = new AtomicInteger(1);
        scoreMap.forEach((id, score) -> {
//            List<OperateManagerService.LessResult> lessResults = dynamicLibraryLoading.batchRun(() -> getFilledList(score, step), this::getCompanyTotalScoreByLimit, count, step, this::getLessResultByR);
            List<OperateManagerService.LessResult> lessResults = this.batchRun(score, step, true);
            long bigSum = lessResults.stream().mapToLong(OperateManagerService.LessResult::getBigCount).sum();
            long smallSum = lessResults.stream().mapToLong(OperateManagerService.LessResult::getSmallCount).sum();
            int percentage = MathUtils.getPercentage(bigSum, bigSum + smallSum);
            RateLevelInfoResult levelInfoResult = RateLevelInfoResult.builder()
                    .percentage(percentage)
                    .level(RateLevel.getByPercentage(percentage))
                    .build();


            WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤四", 0, false, StrUtil.format(StrUtil.format("步骤四-{}",currentTime.get()) + ": 发起方第{}轮计算id为{}的混淆数据", currentTime.get(), id))), ip);
            WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤五", 0, false, StrUtil.format(StrUtil.format("步骤五-{}",currentTime.get()) + ": 发送方发起第{}轮排名匿踪计算, 查询id: {}", currentTime.get(), id))), ip);
            WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤六", 1, false, StrUtil.format(StrUtil.format("步骤六-{}",currentTime.get()) + ": 协同方第{}轮计算id为{}的混淆数据", currentTime.get(), id))), ip);
            WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤七", 1, false, StrUtil.format(StrUtil.format("步骤七-{}",currentTime.get()) + ": 协同方第{}轮计算id为{}的排名匿踪计算结果", currentTime.get(), id))), ip);
            WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤八", 1, false, StrUtil.format(StrUtil.format("步骤八-{}",currentTime.get()) + ": 基于ppmlac的Less32算子调用成功，进行企业资信评分模型得分情况统计学计算", currentTime.get(), id))), ip);
            currentTime.getAndIncrement();


            resultMap.put(id, levelInfoResult);
        });

        sendProgress(80, ip);
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤九", 2, false, "发起方汇总分析排名匿踪计算结果")), ip);
        WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.BUILD("步骤十", 2, false, "企业资信评分完成，结果如下：")), ip);
        List<String> resultString = resultMap.entrySet().stream().map(e -> e.getValue().toFixString(e.getKey())).collect(Collectors.toList());
        for (String s : resultString) {
            WebSocketServer.SendMessage(JSONObject.toJSONString(WebSocketRespondEntity.PLATFORMSUCC(s)), ip);
        }
        sendProgress(100, ip);
        return resultMap;
    }

    public List<OperateManagerService.LessResult> batchRun(long score, int step) {
        return batchRun(score, step, false);
    }

    public List<OperateManagerService.LessResult> batchRun(long score, int step, boolean sendMsg) {
        String ip = null;
        int start = 0;
        int getCount = 0;
        List<OperateManagerService.LessResult> finalResult = Lists.newArrayList();
        List<Long> inputS;
        do {
            List<Long> inputR = getCompanyTotalScoreByLimit(start, step);
            if (CollUtil.isEmpty(inputR)) {
                break;
            }
            getCount = inputR.size();
            inputS = getFilledList(score, getCount);
            if (inputS.size() != inputR.size()) {
                log.error("DynamicLibraryLoading.batchRun error: size not match");
                throw new PlatformException(PlatformExceptionEnum.CALS_ERROR);
            }
            OperateManagerService.LessResult result = getLessResultByR(inputS, inputR);
            finalResult.add(result);
            start += step;
        } while (getCount == step);

        return finalResult;
    }

    private OperateManagerService.LessResult getLessResultByR(List<Long> inputS, List<Long> inputR) {
        return getLessResult(inputS, inputR, false);
    }
    private OperateManagerService.LessResult getLessResult(List<Long> inputS, List<Long> inputR, boolean sizeByS) {
        int size;
        if (sizeByS) {
            size = inputS.size();
        } else {
            size = inputR.size();
        }
        final int sizeFor16;
        if (size % 256 != 0) {
            int gap = 256 - size % 256;
            sizeFor16 = size + gap;
        } else {
            sizeFor16 = size;
        }
        int[] realInputS = cloneArray(CollUtil.sub(inputS, 0, size).stream().mapToInt(Long::intValue).toArray(), sizeFor16);;
        int[] realInputR = cloneArray(CollUtil.sub(inputR, 0, size).stream().mapToInt(Long::intValue).toArray(), sizeFor16);
        double[] result = new double[sizeFor16];

        Thread thread = new Thread(() -> ppmlacRLibrary.getInstance().Less64R(getDoubleArrFromIntArr(realInputS, 10), sizeFor16, sizeFor16, "127.0.0.1", 12123));
        thread.start();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ppmlacSLibrary.getInstance().Less64S(getDoubleArrFromIntArr(realInputR, 10), sizeFor16, result, sizeFor16, "127.0.0.1", 12123);

        int[] intResult = Arrays.stream(result).mapToInt(i -> (int) i).toArray();
        int[] realResult = size == sizeFor16 ? intResult : cloneArray(intResult, size);
        long smallCount = Arrays.stream(realResult).filter(r -> r == 0).count(); //r 小于等于 s 的人数
        long bigCount = size - smallCount;

        return OperateManagerService.LessResult.builder()
                .bigCount(bigCount)
                .smallCount(smallCount)
                .inputS(cloneArray(realInputS, realInputS.length))
                .inputR(cloneArray(realInputR, realInputS.length))
                .build();
    }

    public static int[] cloneArray(int[] original, int resultSize) {
        if (original == null) {
            return null;
        }
        int[] clone = new int[resultSize];
        System.arraycopy(original, 0, clone, 0, Math.min(original.length, resultSize));
        return clone;
    }

    //TODO: move to CompanyService
    private List<Long> getCompanyTotalScoreByLimit(int start, int limit) {
        List<CompanyEntity> companyEntities = companyServicePlus.getBaseMapper().selectList(new LambdaQueryWrapper<CompanyEntity>().select(CompanyEntity::getTotalScore).last(StrUtil.format(" limit {}, {}", start, limit)));
        if (companyEntities == null) {
            return Lists.newArrayList();
        }
        return companyEntities.stream().map(CompanyEntity::getTotalScore).collect(Collectors.toList());
    }

    private static <T> List<T> getFilledList(T obj, int length) {
            List<T> filledList = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                filledList.add(obj);
            }
            return filledList;
    }

    private int getTotalCompanyCount() {
        return Long.valueOf(companyServicePlus.count()).intValue();
    }

    private Map<String, Long> getTotalScore(List<String> ids) {
        List<CompanyEntity> companyEntities = companyServicePlus.getBaseMapper().selectList(new LambdaQueryWrapper<CompanyEntity>().select(CompanyEntity::getTotalScore, CompanyEntity::getUSCC).in(CompanyEntity::getUSCC, ids));
        return companyEntities.stream().collect(Collectors.toMap(CompanyEntity::getUSCC, ce -> Long.valueOf(ce.getTotalScore()), (o1, o2) -> o1, LinkedHashMap::new));
    }
}

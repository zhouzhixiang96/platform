package com.zixian.platform.service.imp;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.zixian.platform.entity.CommonArrayRespEntity;
import com.zixian.platform.entity.LogInfoEntity;
import com.zixian.platform.entity.persistence.LoggingEvent;
import com.zixian.platform.entity.persistence.LoggingEventException;
import com.zixian.platform.enums.ExceptionDescEnum;
import com.zixian.platform.mapper.plus.LoggingEventExceptionMapper;
import com.zixian.platform.mapper.plus.LoggingEventMapper;
import com.zixian.platform.service.LoggingService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * author: tangzw
 * date: 2024/1/17 11:19
 * description:
 **/
@Service
public class LoggingServiceImpl implements LoggingService {

    private final LoggingEventMapper loggingEventMapper;
    private final LoggingEventExceptionMapper loggingEventExceptionMapper;

    public LoggingServiceImpl(LoggingEventMapper loggingEventMapper, LoggingEventExceptionMapper loggingEventExceptionMapper) {
        this.loggingEventMapper = loggingEventMapper;
        this.loggingEventExceptionMapper = loggingEventExceptionMapper;
    }

    @Override
    public CommonArrayRespEntity<LogInfoEntity> selectLogByPage(Integer page, Integer pageSize) {
        Page<LoggingEvent> pageQuery = new Page<>(page, pageSize);
        Page<LoggingEvent> selectPage = loggingEventMapper.selectPage(pageQuery, new LambdaQueryWrapper<LoggingEvent>().orderByDesc(LoggingEvent::getTimestmp));
        return CommonArrayRespEntity.<LogInfoEntity>builder()
                .data(selectPage.getRecords().stream().map(this::mapLogEvent2LogInfoEntity).collect(Collectors.toList()))
                .totalTableCount(selectPage.getTotal())
                .build();
    }

    @Override
    public List<LoggingEventException> getLogStack(Long eventId) {
        if (eventId == null) {
            return Lists.newArrayList();
        }
        return loggingEventExceptionMapper.selectList(new LambdaQueryWrapper<LoggingEventException>().eq(LoggingEventException::getEventId, eventId));
    }

    private LogInfoEntity mapLogEvent2LogInfoEntity(LoggingEvent e) {
        if (e == null) {
            return null;
        }
        return LogInfoEntity.builder()
                .logType(e.getLevelString())
                .logContent(e.getFormattedMessage())
                .id(e.getEventId())
                .happenPlace(ExceptionDescEnum.getDescByLevel(e.getLevelString()))
                .timestamp(e.getTimestmp())
                .build();
    }
}

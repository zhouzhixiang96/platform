package com.zixian.platform.service.imp;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zixian.platform.entity.CompanyEntity;
import com.zixian.platform.mapper.CompanyPlusMapper;
import com.zixian.platform.service.CompanyServicePlus;

@Service
public class CompanyServicePlusImp extends ServiceImpl<CompanyPlusMapper, CompanyEntity> implements CompanyServicePlus{

}

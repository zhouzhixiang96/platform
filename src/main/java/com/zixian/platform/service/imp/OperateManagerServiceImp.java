package com.zixian.platform.service.imp;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.zixian.platform.djl.Operators;
import com.zixian.platform.entity.result.CalRResult;
import com.zixian.platform.entity.result.CalSResult;
import com.zixian.platform.entity.result.OperateResult;
import com.zixian.platform.service.PpmlacRLibrary;
import com.zixian.platform.service.PpmlacSLibrary;
import com.zixian.platform.template.OperateTemplateV2;
import com.zixian.platform.utils.exception.PlatformException;

import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import com.zixian.platform.enums.OperateEnum;
import com.zixian.platform.exception.PlatformExceptionEnum;
import com.zixian.platform.service.DynamicLibraryLoading;
import com.zixian.platform.service.OperateManagerService;
import com.zixian.platform.template.OperateTemplate;

import lombok.extern.slf4j.Slf4j;

import static com.zixian.platform.utils.PlatformStrUtil.printLine;
import static com.zixian.platform.utils.math.MathUtils.getDoubleArrFromLongArr;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OperateManagerServiceImp implements OperateManagerService{

	private final PpmlacSLibrary ppmlacSLibrary;
	private final PpmlacRLibrary ppmlacRLibrary;

	DynamicLibraryLoading dynamicLibraryLoading = new DynamicLibraryLoading();
	
	private long[] showInputS = null;
	
	private long[] showInputR = null;

	private long[] showOutput = null;

	private int showDecimal;

    public OperateManagerServiceImp(PpmlacSLibrary ppmlacSLibrary, PpmlacRLibrary ppmlacRLibrary) {
        this.ppmlacSLibrary = ppmlacSLibrary;
        this.ppmlacRLibrary = ppmlacRLibrary;
    }

    class Operate32 extends OperateTemplate<Integer> {
		
		@Override
		public void initData()
		{

		}
		
		@Override
		public void caculate()
		{
			
		}
		
	}
	
	class Operate64 extends OperateTemplate<Long> {
		
		private String mType = "";
		
		public Operate64(String type,String ipValue,Integer portValue)
		{
			mType = type;
			if(ipValue != null) ip = ipValue;
			if(portValue != null) port = portValue;
		}
		
		@Override
		public void initData()
		{
			if(mType.isEmpty())return;
			decimal = 0;
			switch (OperateEnum.getTypeId(mType)) {
			case 0://32个整数
			   long[] is = new long[32];
			   long[] ir = new long[32];
			   long[] op = new long[32];
			   for(int i = 0 ; i < 32 ; i++)
			   {
				   is[i] = (long) (Math.random()*100);
				   ir[i] = (long) (Math.random()*100);
				   op[i] = 0;
			   }
			   setLong(is, ir, op);
			   break;
			case 1://32*32个整数
			   long[] imats = new long[1024];
			   long[] imatr = new long[1024];
			   for(int i = 0 ; i < 1024 ; i++)
			   {
				   imats[i] = (long) (Math.random()*100);
				   imatr[i] = (long) (Math.random()*100);
			   }
			   setMat(imats,imatr);
			   break;
			case 2://32个定点数
				long[] rl_is = new long[32];
			    long[] rl_ir = new long[32];
			    long[] rl_op = new long[32];
			    for(int i = 0 ; i < 32 ; i++)
			    {
			    	rl_is[i] = (long) (Math.random()*(1 << 16));
			    	rl_ir[i] = (long) (Math.random()*(1 << 16));
			    	rl_op[i] = 0;
			    }
			    decimal = 16;
			    setLong(rl_is, rl_ir, rl_op);
				break;
			case 3://32*32个定点数
				long[] lmats = new long[1024];
			    long[] lmatr = new long[1024];
			    for(int i = 0 ; i < 1024 ; i++)
			    {
			    	lmats[i] = (long) (Math.random()*(1 << 32));
			    	lmatr[i] = (long) (Math.random()*(1 << 32));
			    }
			    decimal = 32;
			    setMat(lmats,lmatr);
			    break;
			case 4://32*32——3*3的定点数
				long[] conMats = new long[1024];
				long[] conMatt = new long[1024];
				long[] conMatr = new long[9];
			    for(int i = 0 ; i < 1024 ; i++)
			    {
			    	conMats[i] = (long) (Math.random()*(1 << 16));
			    	if(i<9)
			    		conMatr[i] = (long) (Math.random()*(1 << 16));
			    }
			    decimal = 16;
			    setMat(conMats,conMatt);
			    setConv(conMatr);
				break;
			default:
				break;
			}

			showDecimal = decimal;
		}
		
		@SneakyThrows
		@Override
		public void caculate()
		{
			if(mType.isEmpty())return;
			Thread sendThread = null;
			Thread sendThread2 = null;
			if(dynamicLibraryLoading.getPPMLACSo() == null)
			{
				log.error("动态库加载失败");
			}else {
				log.info("动态库加载成功");
			}
//			double[] inputS = getDoubleS();
//			double[] inputR = getDoubleR();
			double[] output = getDoubleO();
			double[] mat0Output = getDoubleArrFromLongArr(getMatO(), decimal);
			double[] matA0Output = getDoubleArrFromLongArr(getMatAO(), decimal);
			double[] mat96Output = getDoubleArrFromLongArr(getMat96(), decimal);
//			double[] matAS = getDoubleMatAS();
//			double[] matAR = getDoubleMatAR();
			switch (OperateEnum.toOperateEnum(mType)) {
				case ADD64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Add64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Add64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case ATTENTION64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().SelfAtt64R(getDoubleArrFromLongArr(showInputR = getMatAR(), decimal), 16, 16, 16, 16, 4000, ip, port),
							() -> ppmlacSLibrary.getInstance().SelfAtt64S(getDoubleArrFromLongArr(showInputS = getMatAS(), decimal), 16, 16, 16, 16, matA0Output, 4000, ip, port), matA0Output);
					break;
				case BATCHNORM64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().BatchNorm1d64R(getDoubleArrFromLongArr(showInputR = getMatC(), decimal), 32, 32, 3, 3, ip, port),
							() -> ppmlacSLibrary.getInstance().BatchNorm1d64S(getDoubleArrFromLongArr(showInputS = getMatS(), decimal), 32, 32, 3, 3, mat96Output, ip, port), mat96Output);
					break;
				case CONV2D64://R端需要小一点的卷积矩阵，一般为奇数3X3 成功 //已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Conv2D64R(getDoubleArrFromLongArr(showInputR = getMatC(), decimal), 16, 16, 3, 3, ip, port),
							() -> ppmlacSLibrary.getInstance().Conv2D64S(getDoubleArrFromLongArr(showInputS = getMatAS(), decimal), 16, 16, 3, 3, matA0Output, ip, port), matA0Output);
					break;
				case LINER64://已更新 TODO：数据输入长度有误，需要修正
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().LinearRegressionR(getDoubleArrFromLongArr(showInputR = getR(), decimal), 8, 4, 100, 0.01, ip, port),
							() -> ppmlacSLibrary.getInstance().LinearRegressionS(getDoubleArrFromLongArr(showInputS = getS(), decimal), 8, 4, output, 100, 0.01, ip, port), output);
					break;
				case MATMUL64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().MatMul64R(getDoubleArrFromLongArr(showInputR = getMatR(), decimal), 32, 32, 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().MatMul64S(getDoubleArrFromLongArr(showInputS = getMatS(), decimal), 32, 32, 32, 32, mat0Output, ip, port), mat0Output);
					break;
				case MATPROD64:
					//没有
					throw new PlatformException(PlatformExceptionEnum.OPERATOR_NOT_EXISTS);
				case MATTRANS64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().MatMulTransed64R(getDoubleArrFromLongArr(showInputR = getMatR(), decimal), 32, 32, 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().MatMulTransed64S(getDoubleArrFromLongArr(showInputS = getMatS(), decimal), 32, 32, 32, 32, mat0Output, ip, port), mat0Output);
					break;
				case RMSNORM64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().RmsNorm64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().RmsNorm64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case SECMUL64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Secmul64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Secmul64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case SIGMOID64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Sigmoid64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Sigmoid64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case SILU64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Silu64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Silu64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case SOFTMAX64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Softmax64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Softmax64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case SUB64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Sub64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Sub64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case SUM64://已更新 64位输出个数只能是16，32位输出个数只能是32
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Sum64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 8, ip, port),
							() -> ppmlacSLibrary.getInstance().Sum64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 8, ip, port), output);
					break;
				case TANH64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Tanh64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Tanh64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case XOR64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Xor64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Xor64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case EXP64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Exp64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Exp64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case LAYERNORM64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().LayerNorm64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().LayerNorm64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case ELU64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Elu64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Elu64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case RELU64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().Relu64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().Relu64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				case LEAKLYRELU64://已更新
					doCalculatorAsync(() -> ppmlacRLibrary.getInstance().LeakyRelu64R(getDoubleArrFromLongArr(showInputR = getR(), decimal), 32, 32, ip, port),
							() -> ppmlacSLibrary.getInstance().LeakyRelu64S(getDoubleArrFromLongArr(showInputS = getS(), decimal), 32, output, 32, ip, port), output);
					break;
				default:
					break;
			}
		}

		public void doCalculatorAsync(Runnable runnableR, Runnable runnableS, double[] output) throws InterruptedException {
			Thread sendThread = new Thread(runnableR);
			sendThread.start();
			Thread.sleep(200);
			runnableS.run();
			showOutput = Arrays.stream(output).mapToLong(o -> (long) (o * (1L << decimal))).toArray();
		}

		public void caculateS()
		{
			if(mType.isEmpty())return;
			switch (OperateEnum.toOperateEnum(mType)) {
				case ADD64://通过
					dynamicLibraryLoading.getPPMLACSo().Add64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case ATTENTION64://数据过大
					dynamicLibraryLoading.getPPMLACSo().SelfAtt64S(showInputS = getMatAS(), 16, 16, 16, 16, 4000, decimal, ip, port);
					break;
				case BATCHNORM64://成功
					dynamicLibraryLoading.getPPMLACSo().BatchNorm64S(showInputS = getMatS(), 32, 32, decimal, ip, port);
					break;
				case CONV2D64://R端需要小一点的卷积矩阵，一般为奇数3X3 成功
					dynamicLibraryLoading.getPPMLACSo().Conv2D64S(showInputS = getMatAS(), 16, 16, 3, 3, decimal, ip, port);
					break;
				case ELU64:
					dynamicLibraryLoading.getPPMLACSo().Elu64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case LAYERNORM64:
					dynamicLibraryLoading.getPPMLACSo().LayerNorm64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case LEAKLYRELU64:
					dynamicLibraryLoading.getPPMLACSo().LeakyRelu64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case RELU64:
					dynamicLibraryLoading.getPPMLACSo().Relu64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case LINER64:
					dynamicLibraryLoading.getPPMLACSo().LinearRegressionS(showInputS = getS(), 32, 32, 32, 32, decimal, ip, port);
				case MATMUL64://成功
					dynamicLibraryLoading.getPPMLACSo().MatMul64S(showInputS = getMatS(), 32, 32, 32, 32, decimal, ip, port);
					break;
				case MATPROD64:
					//没有
					throw new PlatformException(PlatformExceptionEnum.OPERATOR_NOT_EXISTS);
				case MATTRANS64://成功
					dynamicLibraryLoading.getPPMLACSo().MatMulTransed64S(showInputS = getMatS(), 32, 32, 32, 32, decimal, ip, port);
					break;
				case RMSNORM64://成功
					dynamicLibraryLoading.getPPMLACSo().RmsNorm64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case SECMUL64://成功
					dynamicLibraryLoading.getPPMLACSo().Secmul64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case SIGMOID64://成功
					dynamicLibraryLoading.getPPMLACSo().Sigmoid64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case SILU64://成功
					dynamicLibraryLoading.getPPMLACSo().Silu64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case SOFTMAX64://成功
					dynamicLibraryLoading.getPPMLACSo().Softmax64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case SUB64://成功
					dynamicLibraryLoading.getPPMLACSo().Sub64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case SUM64://成功 64位输出个数只能是16，32位输出个数只能是32
					dynamicLibraryLoading.getPPMLACSo().Sum64S(showInputS = getS(), 32, 16, decimal, ip, port);
					break;
				case TANH64://成功
					dynamicLibraryLoading.getPPMLACSo().Tanh64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case XOR64://成功
					dynamicLibraryLoading.getPPMLACSo().Xor64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				case EXP64://成功
					dynamicLibraryLoading.getPPMLACSo().Exp64S(showInputS = getS(), 32, 32, decimal, ip, port);
					break;
				default:
					break;
			}
		}
		
		public void caculateR()
		{
			if(mType.isEmpty())return;
			switch (OperateEnum.toOperateEnum(mType)) {
				case ADD64://通过
					dynamicLibraryLoading.getPPMLACSo().Add64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case ATTENTION64://数据过大
					dynamicLibraryLoading.getPPMLACSo().SelfAtt64R(showInputR = getMatAR(), 16, 16, 16, 16, 4000, showOutput = getMatAO(), decimal, port);
					break;
				case BATCHNORM64://成功
					dynamicLibraryLoading.getPPMLACSo().BatchNorm64R(showInputR = getMatC(), 32, 32, showOutput = getMatO(), decimal, port);
					break;
				case CONV2D64://R端需要小一点的卷积矩阵，一般为奇数3X3 成功
					dynamicLibraryLoading.getPPMLACSo().Conv2D64R(showInputR = getMatC(), 16, 16, 3, 3, showOutput = getMatAO(), decimal, port);
					break;
				case ELU64:
					dynamicLibraryLoading.getPPMLACSo().Elu64R(showInputR = getR(), 32, 32, showOutput = getMatAO(), decimal, port);
					break;
				case LAYERNORM64:
					dynamicLibraryLoading.getPPMLACSo().LayerNorm64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case LEAKLYRELU64:
					dynamicLibraryLoading.getPPMLACSo().LeakyRelu64R(showInputR = getR(), 32, 32, showOutput = getMatAO(), decimal, port);
					break;
				case RELU64:
					dynamicLibraryLoading.getPPMLACSo().Relu64R(showInputR = getR(), 32, 32, showOutput = getMatAO(), decimal, port);
					break;
				case LINER64:
					dynamicLibraryLoading.getPPMLACSo().LinearRegressionR(showInputR = getR(), 32, 32,  decimal,showOutput = getO(), port);
					break;
				case MATMUL64://成功
					dynamicLibraryLoading.getPPMLACSo().MatMul64R(showInputR = getMatR(), 32, 32, 32, 32, showOutput = getMatO(), decimal, port);
					break;
				case MATPROD64:
					//没有
					throw new PlatformException(PlatformExceptionEnum.OPERATOR_NOT_EXISTS);
				case MATTRANS64://成功
					dynamicLibraryLoading.getPPMLACSo().MatMulTransed64R(showInputR = getMatR(), 32, 32, 32, 32, showOutput = getMatO(), decimal, port);
					break;
				case RMSNORM64://成功
					dynamicLibraryLoading.getPPMLACSo().RmsNorm64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case SECMUL64://成功
					dynamicLibraryLoading.getPPMLACSo().Secmul64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case SIGMOID64://成功
					dynamicLibraryLoading.getPPMLACSo().Sigmoid64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case SILU64://成功
					dynamicLibraryLoading.getPPMLACSo().Silu64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case SOFTMAX64://成功
					dynamicLibraryLoading.getPPMLACSo().Softmax64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case SUB64://成功
					dynamicLibraryLoading.getPPMLACSo().Sub64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case SUM64://成功 64位输出个数只能是16，32位输出个数只能是32
					dynamicLibraryLoading.getPPMLACSo().Sum64R(showInputR = getR(), 32, 16, showOutput = getResult(), decimal, port);
					break;
				case TANH64://成功
					dynamicLibraryLoading.getPPMLACSo().Tanh64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case XOR64://成功
					dynamicLibraryLoading.getPPMLACSo().Xor64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				case EXP64://成功
					dynamicLibraryLoading.getPPMLACSo().Exp64R(showInputR = getR(), 32, 32, showOutput = getO(), decimal, port);
					break;
				default:
					break;
			}
		}
		
	}

	@Override
	public void operateCal(String type) {
		Operate64 operate64 = new Operate64(type,null,null);
		operate64.OperateCaculate();
	}
	
	@Override
	public void dealSOLog(int lines) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public OperateResult<Double, Double, Integer> operate32Cal(String type) throws InterruptedException {
		OperateTemplateV2.Operate32Template operate32Template = new OperateTemplateV2.Operate32Template();
		OperateResult<Double, Double, Integer> result = operate32Template.calculate(type);
		if (CollUtil.isNotEmpty(result.getResult())) {
			printLine();
			System.out.println(StrUtil.format("精度为：{}", result.getDecimalBits()));
			printLine();
			System.out.println("input_s: " + result.getInputS());
			printLine();
			System.out.println("input_r: " + result.getInputR());
			printLine();
			System.out.println("result: " + result.getResult());
			printLine();
			System.out.println("originResult: " + result.getOriginResult());
			printLine();
		}
		return result;
	}

	@Override
	public CalSResult<Double> operate32CalS(String type, String ip, Integer port){
		OperateTemplateV2.Operate32Template operate32Template = new OperateTemplateV2.Operate32Template();
        try {
            return operate32Template.calculateS(type, ip, port);
        } catch (InterruptedException e) {
            log.error("operate32CalS error: {}", e);
			throw new PlatformException(PlatformExceptionEnum.CALS_ERROR);
        }
    }

	@Override
	public CalRResult operate32CalR(String type, Integer port) {
		OperateTemplateV2.Operate32Template operate32Template = new OperateTemplateV2.Operate32Template();
		try {
			return operate32Template.calculateR(type, port);
		} catch (InterruptedException e) {
			log.error("operate32CalR error: {}", e);
			throw new PlatformException(PlatformExceptionEnum.CALS_ERROR);
		}
	}

	@Override
	public double[] check(String typeStr, List<Double> inputS, List<Double> inputR) {
		double[] result = new double[]{-1};
		if (StrUtil.isNotEmpty(typeStr)) {
			OperateEnum type = OperateEnum.toOperateEnum(typeStr);

			double[] inputSArr = inputS.stream().mapToDouble(Double::doubleValue).toArray();
			double[] inputRArr = inputR.stream().mapToDouble(Double::doubleValue).toArray();

			switch (Objects.requireNonNull(type)) {
				case CONV2D64:
				case CONV2D32:
					result = Operators.conv2d(inputSArr, inputRArr);
					break;
				case SOFTMAX32:
				case SOFTMAX64:
					result = Operators.softmax(inputSArr);
					break;
				case ELU64:
					result = Operators.elu(inputSArr);
					break;
				case LAYERNORM64:
					result = Operators.layerNorm(inputSArr, inputRArr, inputRArr);
					break;
				case RELU64:
					result = Operators.relu(inputSArr);
					break;
				case LEAKLYRELU64:
					result = Operators.leakyRelu(inputSArr, 0.01);
					break;
				case SIGMOID32:
				case SIGMOID64:
					result = Operators.sigmoid(inputSArr);
					break;
				case SILU32:
				case SILU64:
					result = Operators.silu(inputSArr);
					break;
				case ATTENTION64:
					result = Operators.selfAttention(inputSArr, inputRArr, 16, 16, 16, 16);
					break;
				case BATCHNORM32:
				case BATCHNORM64:
					result = Operators.batchNorm(inputRArr);
					break;
				case RMSNORM32:
				case RMSNORM64:
					result = Operators.rmsNorm(inputRArr);
					break;
				case TANH32:
				case TANH64:
					result = Operators.tanh(inputRArr);
					break;
				default:
					log.error("no such check type: {}", type);
			}
		}

		return result;
	}

	@Override
	public List<Double> getInputS() {
		int count = showInputS.length;
		double[] result = new double[count];
		System.out.println("decimal:" + showDecimal);
		if(showDecimal == 0)
		{
			for(int i = 0 ; i < count ; i++)
			{
				result[i] = (double)showInputS[i];
			}
		}else {
			for(int i = 0 ; i < count ; i++)
			{
				result[i] = (showInputS[i] * 1.0) / (1L << showDecimal);
			}
		}
		return Arrays.stream(result).boxed().collect(Collectors.toList());
	}

	@Override
	public List<Double> getInputR() {
		int count = showInputR.length;
		double[] result = new double[count];
		if(showDecimal == 0)
		{
			for(int i = 0 ; i < count ; i++)
			{
				result[i] = (double)showInputR[i];
			}
		}else {
			for(int i = 0 ; i < count ; i++)
			{
				result[i] = (showInputR[i] * 1.0) / (1L << showDecimal);
			}
		}
		return Arrays.stream(result).boxed().collect(Collectors.toList());
	}

	@Override
	public List<Double> getOutput() {
		int count = showOutput.length;
		double[] result = new double[count];
		if(showDecimal == 0)
		{
			for(int i = 0 ; i < count ; i++)
			{
				result[i] = (double)showOutput[i];
			}
		}else {
			for(int i = 0 ; i < count ; i++)
			{
				result[i] = (showOutput[i] * 1.0) / (1L << showDecimal);
			}
		}
		return Arrays.stream(result).boxed().collect(Collectors.toList());
	}

	@Override
	public int getShowDecimal() {
		return showDecimal;
	}

	@Override
	public CalSResult<Double> operate64CalS(String type, String ip, Integer port) {
		Operate64 operate64 = new Operate64(type,ip,port);
		operate64.initData();
		operate64.caculateS();
		return CalSResult.<Double>builder()
                .inputS(getInputS())
                .build();
	}

	@Override
	public CalRResult operate64CalR(String type, Integer port) {
		Operate64 operate64 = new Operate64(type,null,port);
		operate64.initData();
		operate64.caculateR();
		return CalRResult.<Double, Double, Long>builder()
                .originResult(Arrays.stream(showOutput).boxed().collect(Collectors.toList()))
                .inputR(getInputR())
                .result(getOutput())
                .build();
	}

}

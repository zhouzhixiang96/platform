package com.zixian.platform.dispatch.service;

import com.zixian.platform.entity.result.CalRResult;
import com.zixian.platform.entity.result.CalSResult;

/**
 * author: tangzw
 * date: 2024/3/1 10:38
 * description:
 **/
public interface DispatchOperateService {
    CalSResult calS(String type, String ip, Integer port);

    CalRResult calR(String type, Integer port);
}

package com.zixian.platform.dispatch.service.impl;

import com.zixian.platform.dispatch.service.DispatchOperateService;
import com.zixian.platform.entity.result.CalRResult;
import com.zixian.platform.entity.result.CalSResult;
import com.zixian.platform.service.OperateManagerService;
import org.springframework.stereotype.Service;

import static com.zixian.platform.enums.PlatformConstant.OP32;
import static com.zixian.platform.enums.PlatformConstant.OP64;

/**
 * author: tangzw
 * date: 2024/3/1 10:38
 * description:
 **/
@Service
public class DispatchOperateServiceImpl implements DispatchOperateService {

    private final OperateManagerService operateManagerService;

    public DispatchOperateServiceImpl(OperateManagerService operateManagerService) {
        this.operateManagerService = operateManagerService;
    }

    @Override
    public CalSResult calS(String type, String ip, Integer port) {
        if (type.contains(OP64)) {
            return operateManagerService.operate64CalS(type, ip, port);
        } else if (type.contains(OP32)) {
            return operateManagerService.operate32CalS(type, ip, port);
        }

        return new CalSResult();
    }

    @Override
    public CalRResult calR(String type, Integer port) {
        if (type.contains(OP64)) {
            return operateManagerService.operate64CalR(type, port);
        } else if (type.contains(OP32)) {
            return operateManagerService.operate32CalR(type, port);
        }

        return new CalRResult();
    }
}

package com.zixian.platform.dispatch;

import com.zixian.platform.dispatch.service.DispatchOperateService;
import com.zixian.platform.entity.RespondEntity;
import com.zixian.platform.entity.result.CalRResult;
import com.zixian.platform.entity.result.CalSResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * author: tangzw
 * date: 2024/3/1 10:34
 * description:
 **/
@RestController
@RequestMapping("/dispatch/operate")
public class DispatchOperateController {

    private final DispatchOperateService dispatchOperateService;

    public DispatchOperateController(DispatchOperateService dispatchOperateService) {
        this.dispatchOperateService = dispatchOperateService;
    }

    @GetMapping("calS")
    public RespondEntity<CalSResult> calS(String type, String ip, Integer port) {
        return RespondEntity.SUCCESS(dispatchOperateService.calS(type, ip, port));
    }

    @GetMapping("calR")
    public RespondEntity<CalRResult> calR(String type, Integer port) {
        return RespondEntity.SUCCESS(dispatchOperateService.calR(type, port));
    }
}

package com.zixian.platform.config;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

/**
 author: tangzw
 date: 2024-01-16 14:58:02
 description:
 **/
@Configuration
public class CustomizeDataSourceInitializer {

    @Value("${spring.sql.init.schema-locations}")
    private Resource schemaLocation;

    @Value("${spring.sql.init.schema-locations-dm}")
    private Resource schemaLocationDM;

    @Value("${spring.datasource.driver-class-name}")
    private String driverName;

//    @Bean
//    public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
//        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
//        dataSourceInitializer.setDataSource(dataSource);
//        dataSourceInitializer.setDatabasePopulator(databasePopulator());
//        return dataSourceInitializer;
//    }
//
//    private DatabasePopulator databasePopulator() {
//        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
//        if (driverName.contains("dm")) {
//            resourceDatabasePopulator.addScript(schemaLocationDM);
//        } else {
//            resourceDatabasePopulator.addScript(schemaLocation);
//        }
//        return resourceDatabasePopulator;
//    }
}

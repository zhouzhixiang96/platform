package com.zixian.platform.thread;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadPoolExecutor;

import com.zixian.platform.aop.MDCAop;
import org.slf4j.MDC;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskDecorator;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;

@Configuration
@EnableAsync//开启异步调用
public class ThreadPoolConfig {

	// 获取服务器的cpu个数
	private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();// 获取cpu个数
	private static final int COUR_SIZE = CPU_COUNT * 2;
	private static final int MAX_COUR_SIZE = CPU_COUNT * 4;

	// 接下来配置一个bean，配置线程池。
	@Bean(name = "myThreadPool")
	public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setCorePoolSize(COUR_SIZE);// 设置核心线程数
		threadPoolTaskExecutor.setMaxPoolSize(MAX_COUR_SIZE);// 配置最大线程数
		threadPoolTaskExecutor.setQueueCapacity(MAX_COUR_SIZE * 4);// 配置队列容量（这里设置成最大线程数的四倍）
		threadPoolTaskExecutor.setThreadNamePrefix("thread-pool-zixian");// 给线程池设置名称
		threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());// 设置任务的拒绝策略
		threadPoolTaskExecutor.setTaskDecorator(new TaskDecorator() {
			@Override
			public Runnable decorate(Runnable runnable) {
				Map<String, String> copyOfContextMap = MDC.getCopyOfContextMap();
				return new Runnable() {
					@Override
					public void run() {
						try {
							MDC.setContextMap(copyOfContextMap);
							if (StringUtils.isEmpty(MDC.get(MDCAop.TRACE_ID))) {
								MDC.put(MDCAop.TRACE_ID, UUID.randomUUID().toString());
							}
							runnable.run();
						} finally {
							MDC.clear();
						}
					}
				};
			}
		});
		return threadPoolTaskExecutor;
	}
}
package com.zixian.platform.utils.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MathUtils {
    public static long getBigCount(int salary, int[] companyAAll) {
        return Arrays.stream(companyAAll).filter(o -> o > salary).count();
    }

    public static int getPercentage(long numerator, long denominator) {
        return new BigDecimal(numerator).divide(new BigDecimal(denominator), 2, RoundingMode.HALF_UP).multiply(new BigDecimal("100")).intValue();
    }

    /**
     * 将long型的数组转为double，通过定点小数的方式
     * @param input 源数组
     * @param decimalBit 定点小数位
     * @return
     */
    public static double[] getDoubleArrFromLongArr(long[] input, int decimalBit) {
        List<Double> inputR = getInputTmp(input, decimalBit);
        return inputR.stream().mapToDouble(i -> i).toArray();
    }

    public static double[] getDoubleArrFromIntArr(int[] input, int decimalBit) {
        long[] longInput = Arrays.stream(input).mapToLong(i -> i).toArray();
        List<Double> inputR = getInputTmp(longInput, decimalBit);
        return inputR.stream().mapToDouble(i -> i).toArray();
    }

    public static List<Double> getInputTmp(long[] tmp, int decimalBit) {
        int count = tmp.length;
        double[] result = new double[count];
        if(decimalBit == 0)
        {
            for(int i = 0 ; i < count ; i++)
            {
                result[i] = (double)tmp[i];
            }
        }else {
            for(int i = 0 ; i < count ; i++)
            {
                result[i] = (tmp[i] * 1.0) / (1L << decimalBit);
            }
        }
        return Arrays.stream(result).boxed().collect(Collectors.toList());
    }

    public class LagrangeInterpolationFormula{
        int j,k,m,n;		//循环值
        double fz,fm,y,f;		//中间值
        double xi[],yi[];	//原始数据
        BigInteger bXi[],bYi[];
        BigInteger bfz,bfm,by,bf;
        //构造函数
        public LagrangeInterpolationFormula() {
        	y = 0;
        }
        //插值数据
        public void interpolateData(double a[],double b[])
        {
        	xi = a;
        	yi = b;
        	m = a.length;
            n = b.length;
        }
        //插值数据重载
        public void bInterpolateData(BigInteger a[],BigInteger b[])
        {
        	bXi = a;
        	bYi = b;
        	m = a.length;
            n = b.length;
        }
        //计算H(x)
        public double hValue(double x){
        	y = 0;
            for(k=0;k<m;k++){
                fz=1;
                fm=1;
                for(j=0;j<n;j++){
                    if(j!=k)
                    {
                        fz=fz*(x-xi[j]);
                        fm=fm*(xi[k]-xi[j]);
                    }
                }
                y += yi[k]*fz/fm;
            }
            return y;
        }
        public BigInteger hValue(BigInteger x) {
        	by = BigInteger.valueOf(0);
            for(k=0;k<m;k++){
                bfz=BigInteger.valueOf(1);
                bfm=BigInteger.valueOf(1);
                for(j=0;j<n;j++){
                    if(j!=k)
                    {
                    	bfz = bfz.multiply(x.subtract(bXi[j]));
                    	bfm = bfm.multiply(bXi[k].subtract(bXi[j]));
                    }
                }
                by = by.add(bYi[k].multiply(bfz).divide(bfm));
            }
            return by;
        }
        //计算F(x)
        public double fValue(double x) {
        	f = 1;
        	for( k = 0 ; k < m ; k++)
        	{
        		f *= (x-xi[k]);
        	}
        	return f;
        }
        public BigInteger fValue(BigInteger x) {
        	bf = BigInteger.valueOf(1);
        	for( k = 0 ; k < m ; k++)
        	{
        		bf = bf.multiply(x.subtract(bXi[k]));
        	}
			return bf;
        }
    }
}

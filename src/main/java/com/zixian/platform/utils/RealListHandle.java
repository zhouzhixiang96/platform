package com.zixian.platform.utils;

import java.util.List;

import org.apache.ibatis.type.MappedTypes;

import com.alibaba.fastjson.TypeReference;
import com.zixian.platform.entity.RealEstateDTO;

@MappedTypes({List.class})
public class RealListHandle extends ListTypeHandler<RealEstateDTO>{

	@Override
	protected TypeReference<List<RealEstateDTO>> specificType() {
		return new TypeReference<List<RealEstateDTO>>() {
        };
	}

}

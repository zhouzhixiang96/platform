package com.zixian.platform.utils.cuckoo;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import com.alibaba.fastjson.JSONObject;
import com.zixian.platform.entity.PersonEntity;

public class CuckooHashTable<AnyType> {

	public interface HashFamily <AnyType>{
	    //根据which来选择散列函数，并返回hash值
	    int hash(AnyType x, int which);
	    //返回集合中散列函数的个数
	    int getNumberOfFunctions();
	    //获取到新的散列函数
	    void generateNewFunctions();
	}
	
    public CuckooHashTable(HashFamily<? super AnyType> hf){
        this(hf, DEFAULT_TABLE_SIZE);
    }
    //初始化操作
    public CuckooHashTable(HashFamily<? super AnyType> hf, int size){
    	hashFunctions = hf;
        numHashFunctions = hf.getNumberOfFunctions();
        allocateArray(nextPrime(size));
        buckSize = array[0].length;
        doClear();
    }

    public void makeEmpty(){
        doClear();
    }

    public boolean contains(AnyType x){
    	int[] posArray = findPos(x);
        int pos = posArray[1];
        return pos != -1;
    }
    
    public String contains(AnyType x,PersonEntity p)
    {
    	for (int i = 0; i < numHashFunctions; i ++){
            //获取到当前hash值
            int pos = myHash(x, i);
            //判断表中是否存在当前元素
            AnyType s = array[i][pos];
            if(s != null)
            {
            	String tStr = s.toString();
            	p = JSONObject.parseObject(tStr, PersonEntity.class);
            	if(x.equals(p.getId()))
            	{
            		return tStr;
            	}
            }
        }
    	return null;
    }
    
	public String getCurrentTime()
	{
		// 创建一个日期对象
        Date date = new Date();
        // 创建一个格式化对象
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        // 使用格式化对象将日期格式化为字符串
        String formattedDate = sdf.format(date);
        
        return formattedDate;
	}

    /**
     *
     * @param x 当前的元素
     * @param which 选取的散列函数对应的位置
     * @return
     */
    private int myHash(AnyType x, int which){
        //调用散列函数集合中的hash方法获取到hash值
        int hashVal = hashFunctions.hash(x, which);
        //再做一定的处理
        hashVal %= buckSize;
        if (hashVal < 0){
            hashVal += buckSize;
        }
        return hashVal;
    }

    /**
     * 查询元素的位置，若找到元素，则返回其当前位置，否则返回-1
     * @param x
     * @return
     */
    int[] result = new int[2];
    private int[] findPos(AnyType x){
    	result[1] = -1;
        //遍历散列函数集合，因为不确定元素所用的散列函数为哪个
        for (int i = 0; i < numHashFunctions; i ++){
            //获取到当前hash值
            int pos = myHash(x, i);
            //判断表中是否存在当前元素
            if (array[i][pos] != null && array[i][pos].equals(x)){
            	result[0] = i;
            	result[1] = pos;
                return result;
            }
        }
        return result;
    }

    /**
     * 删除元素：先查询表中是否存在该元素，若存在，则进行删除该元素
     * @param x
     * @return
     */
    public boolean remove(AnyType x){
    	int[] posArray = findPos(x);
        int pos = posArray[1];
        if (pos != -1){
            array[pos] = null;
            currentSize --;
        }
        return pos != -1;
    }

    /**
     * 插入：先判断该元素是否存在，若存在，在判断表的大小是否达到最大负载，
     * 若达到，则进行扩展，最后调用insertHelper方法进行插入元素
     * @param x
     * @return
     */
    public boolean insert(AnyType x){
//        if (contains(x)){
//            return false;
//        }
//        if (currentSize >= array[0].length * MAX_LOAD){
//            expand();
//        }
        boolean b = insertHelper(x);
        return b;
    }
    
    public boolean insert(AnyType key,AnyType value)
    {
        boolean b = insertHelper(key,value);
        return b;
    }

    /**
     * 具体的插入过程：
     * a. 先遍历散列函数集合，找出元素所有的可存放的位置，若找到的位置为空，则放入即可，完成插入
     * b. 若没有找到空闲位置，随机产生一个位置
     * c. 将插入的元素替换随机产生的位置，并将要插入的元素更新为被替换的元素
     * d. 替换后，回到步骤a.
     * e. 若超过查找次数，还是没有找到空闲位置，那么根据rehash的次数，
     * 判断是否需要进行扩展表，若超过rehash的最大次数，则进行扩展表，
     * 否则进行rehash操作，并更新散列函数集合
     * @param x
     * @return
     */
    int failCount = 0;
    //记录循环的最大次数
    final int COUNT_LIMIT = 100;
    private boolean insertHelper(AnyType x) {
        while (true){
            //记录上一个元素位置
        	int lastPos = -1;
            int pos;
            //进行查找插入
            failCount = 0;
            for (int count = 0; count < COUNT_LIMIT; count ++){
                for (int i = 0; i < numHashFunctions; i ++){
                    pos = myHash(x, i);
                    //查找成功，直接返回
                    if (array[i][pos] == null){
                        array[i][pos] = x;
                        currentSize ++;
                        System.out.println("value:" + x + "buck:" + i + "index:" + pos);
                        return true;
                    }
                }
                failCount++;
                //查找失败，进行替换操作，产生随机数位置，当产生的位置不能与原来的位置相同
                int i = 0;
                int buck = -1;
                do {
//                	 buck = r.nextInt(numHashFunctions);
                	buck = failCount % 3;
                    pos = myHash(x,buck);
                    failCount++;
                } while (pos == lastPos && i ++ < 5);

                lastPos = pos;
                //固定替换桶区域内元素
//                buck = failCount % 3;
                //进行替换操作
                AnyType temp = array[buck][lastPos];
                array[buck][lastPos] = x;
                x = temp;
            }
            //超过次数，还是插入失败，则进行扩表或rehash操作
            if (++ rehashes > ALLOWED_REHASHES){
                expand();
                rehashes = 0;
            } else {
                rehash();
            }
        }
    }
    
    private boolean insertHelper(AnyType key,AnyType value) {
        while (true){
            //记录上一个元素位置
        	int lastPos = -1;
            int pos;
            //进行查找插入
            for (int count = 0; count < COUNT_LIMIT; count ++){
                for (int i = 0; i < numHashFunctions; i ++){
                    pos = myHash(key, i);
                    //查找成功，直接返回
                    if (array[i][pos] == null){
                        array[i][pos] = value;
                        currentSize ++;
                        return true;
                    }
                }
                failCount++;
                //查找失败，进行替换操作，产生随机数位置，当产生的位置不能与原来的位置相同
                int i = 0;
                int buck = -1;
                do {
                	 buck = r.nextInt(numHashFunctions);
                     pos = myHash(key,buck);
                } while (pos == lastPos && i ++ < 5);

                lastPos = pos;
                //固定替换最后一个桶区域内元素
//                pos = myHash(x,numHashFunctions - 1);
                //进行替换操作
                AnyType temp = array[buck][lastPos];
                array[buck][lastPos] = value;
                PersonEntity person = JSONObject.parseObject(temp.toString(), PersonEntity.class);
                key = (AnyType) person.getId();
            }
            //超过次数，还是插入失败，则进行扩表或rehash操作
            if (++ rehashes > ALLOWED_REHASHES){
                expand();
                rehashes = 0;
            } else {
                rehash();
            }
        }
    }

    private void expand(){
    	buckSize = (int)(buckSize / MAX_LOAD);
        rehash( buckSize);
    }

    private void rehash(){
        hashFunctions.generateNewFunctions();
        rehash(buckSize);
    }

    private void rehash(int newLength){
    	System.out.println("rehash");
        AnyType[][] oldArray = array;
        allocateArray(nextPrime(newLength));
        currentSize = 0;
        buckSize = newLength;
        for (AnyType[] strArray : oldArray){
        	for(AnyType str : strArray)
        	{
                if (str != null){
                    insert(str);
                }
        	}
        }
    }
    //清空操作
    private void doClear(){
        currentSize = 0;
        for (int i = 0; i < array.length; i ++){
        	for(int j = 0 ; j < array[i].length ; j++)
        	{
        		array[i][j] = null;	
        	}
        }
    }
    //初始化表
    private void allocateArray(int arraySize){
    	array = (AnyType[][])new Object[numHashFunctions][arraySize];
    }

    public void printArray(){
        System.out.println("当前散列表如下：");
        for(int i = 0 ; i < array.length ; i++)
        {
        	System.out.printf("buck%d size：%d\n",i, array[i].length);
        }
        System.out.println("kick out times：" + failCount);
//        for (int i = 0; i < array.length; i ++){
//            if (array[i] != null)
//                System.out.println("current pos: " + i + " current value: " + array[i]);
//        }
    }
    
    public String getTableStruct()
    {
    	String result = "";
    	result += "当前散列表如下：\n";
        for(int i = 0 ; i < array.length ; i++)
        {
        	result += "buck" + i +  "size：" + array[i].length + "\n";
        }
    	return result;
    }
    
    public String value(int pos)
    {
    	if(pos < buckSize * array.length)
    	{
        	int buck = pos / buckSize;
        	int bPos = pos % buckSize;
    		if (array[buck][bPos] != null)
        			return array[buck][bPos].toString();
    	}
    	return null;
    }
    
    public String tempS = "";
    
    public boolean printBitArray(String filePath) {
    	tempS = "";
    	FileOutputStream fos;
		try {
			fos = new FileOutputStream(filePath);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			String s = "";
			for(int j = 0 ; j < array.length ; j++)
			{
				for(int i = 0 ; i < buckSize; i ++) {
		    		if (array[j][i] != null)
		    		{
		    			s = "1";
		    		}else {
		    			s = "0";
		    		}
		    		if( j * array.length + i < 100)
		    		{
		    			tempS += s;
		    		}
		    		bos.write(s.getBytes());
				}
			}
			bos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		tempS += "......Total size = " + String.valueOf(array.length * buckSize);
		return true;
    }
    
    public byte[] getIndexByteInfo()
    {
    	byte[] result = new byte[array.length*buckSize];
    	for(int j = 0 ; j < array.length ; j++)
		{
			for(int i = 0 ; i < buckSize; i ++) {
	    		if (array[j][i] != null)
	    		{
	    			result[j*buckSize + i] = 1;
	    		}else {
	    			result[j*buckSize + i] = 0;
	    		}
			}
		}
    	return result;
    }

    //定义最大装填因子为0.8
    private static final double MAX_LOAD = 0.8;
    //定义rehash次数达到一定时，进行
    private static final int ALLOWED_REHASHES = 1;
    //定义默认表的大小
    private static final int DEFAULT_TABLE_SIZE = 101;
    //定义散列函数集合
    private final HashFamily<? super AnyType> hashFunctions;
    //定义散列函数个数
    private final int numHashFunctions;
    //定义当前表
    private AnyType[][] array;
    //定义当前表的大小
    private int currentSize;
    //定义rehash的次数
    private int rehashes = 0;
    //定义一个随机数
    private Random r = new Random();
    //定义桶大小
    private int buckSize;
    
    public int getBuckSize()
    {
    	return buckSize;
    }

    //返回下一个素数
    private static int nextPrime(int n){
        while (!isPrime(n)){
            n ++;
        }
        return n;
    }
    //判断是否为素数
    private static boolean isPrime(int n){
        for (int i = 2; i <= Math.sqrt(n); i ++){
            if (n % i == 0 && n != 2){
                return false;
            }
        }
        return true;
    }

}
package com.zixian.platform.utils.socket;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;


@Component
@ServerEndpoint(value = "/ws", configurator = WebSocketConfig.class)
@Slf4j
public class WebSocketServer {

    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    
    private static String currentSessionId;

    private static final AtomicInteger OnlineCount = new AtomicInteger(0);

    // concurrent包的线程安全Set，用来存放每个客户端对应的Session对象。
    private static CopyOnWriteArraySet<Session> SessionSet = new CopyOnWriteArraySet<Session>();
    
    private static Map<String,String> sessionMap = new HashMap<String, String>();


    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session) {
        SessionSet.add(session);
        this.session = session;
        String ip = (String) session.getUserProperties().get("X_Real_IP");
        currentSessionId = this.session.getId();
        if (sessionMap.get(ip) == null) {
            int cnt = OnlineCount.incrementAndGet(); // 在线数加1
            System.out.printf("有连接加入，当前连接数为：{%d}\n"+ip, cnt);
        } else {
        	OnlineCount.incrementAndGet(); // 在线数加1
            System.out.printf("ip地址为：{%s} 的连接重新建连\n", ip);
        }
        sessionMap.put(ip, session.getId());
        SendMessage(session, "与服务器连接成功");
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        SessionSet.remove(session);
        String ip = (String) session.getUserProperties().get("X_Real_IP");
        sessionMap.remove(ip);
        int cnt = OnlineCount.decrementAndGet();
        System.out.printf("有连接关闭，当前连接数为：{%d}\n", cnt);
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
    	System.out.printf("来自客户端的消息：{%s}\n", message);
        SendMessage(session, "收到消息，消息内容：" + message);

    }

    /**
     * 出现错误
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
    	System.out.printf("发生错误：{%s}，Session ID： {%s}\n", error.getMessage(), session.getId());
//        error.printStackTrace();
    }

    /**
     * 发送消息，实践表明，每次浏览器刷新，session会发生变化。
     *
     * @param session
     * @param message
     */
    public static void SendMessage(Session session, String message) {
        try {
//            session.getBasicRemote().sendText(String.format("%s (From Server，Session ID=%s)", message, session.getId()));
        	session.getBasicRemote().sendText(message);
        } catch (IOException e) {
        	System.out.printf("发送消息出错：{%s}\n", e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 群发消息
     *
     * @param message
     * @throws IOException
     */
    public static void BroadCastInfo(String message) {
        for (Session session : SessionSet) {
            if (session.isOpen()) {
                SendMessage(session, message);
            }
        }
    }

    /**
     * 指定Session发送消息
     *
     * @param sessionId
     * @param message
     * @throws IOException
     */
    public static void SendMessage(String message, String sessionId) {
        Session session = null;
        if(sessionId == null)sessionId = currentSessionId;
        for (Session s : SessionSet) {
            if (s.getId().equals(sessionId)) {
                session = s;
                break;
            }
        }
        if (session != null) {
            SendMessage(session, message);
        } else {
        	System.out.printf("没有找到你指定ID的会话：{%s}\n", sessionId);
        }
    }

    /**
     * 指定Session发送JSON消息
     *
     * @param sessionId
     * @param message
     * @throws IOException
     */
    public static void SendMessageJson(Object message, String sessionId) {
        Session session = null;
        if(sessionId == null)sessionId = currentSessionId;
        for (Session s : SessionSet) {
            if (s.getId().equals(sessionId)) {
                session = s;
                break;
            }
        }
        if (session != null) {
            SendMessage(session, JSONObject.toJSONString(message));
        } else {
            System.out.printf("没有找到你指定ID的会话：{%s}\n", sessionId);
        }
    }

    public static String getCurrentId()
    {
		return currentSessionId;
    }
    
    public static String getIdByIp(String ip)
    {
    	if(ip == null)return null;
    	return sessionMap.get(ip);
    }
    
    public static String getIdByRequest(HttpServletRequest request)
    {
		String ip = request.getHeader("x-forwarded-for");
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
			System.out.println("-----------Proxy-Client-IP-------------\n" + ip);
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
			System.out.println("-----------WL-Proxy-Client-IP-------------\n" + ip);
		}
		if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
			System.out.println("-----------unknown-------------\n" + ip);
		}
		System.out.println("-----------final-------------\n" + ip);
		if(ip != null)
		{
			ip = ip.replaceAll("\\s*", "");
			String[] ips = ip.split(",");
			if(ips.length > 0)ip = ips[0];
		}
		System.out.println("-----------deal-------------\n" + ip);
		return getIdByIp(ip);
    }
    
    public static boolean closeCurrent(String sessionId) throws IOException
    {
    	Session session = null;
        for (Session s : SessionSet) {
            if (s.getId().equals(sessionId)) {
                session = s;
                break;
            }
        }
        if (session != null) {
            session.close();
            return true;
        } else {
        	System.out.printf("关闭失败，没有找到你指定ID的会话：{%s}\n", sessionId);
        }
        return false;
    }

    public static Map<String, String> getSessions() {
        return sessionMap;
    }
    
    @Scheduled(fixedRate = 1000 * 50)
    private void heartBeat()
    {
    	if(OnlineCount.get() > 0)
    	{
    		for(String sessionId : sessionMap.values())
    		{
    			log.info("WebsocketHeartBeat,sessionId = " + sessionId);
    			SendMessage("HeartBeat", sessionId);
    		}
    	}
    }

    public static void sendProgress(int value, String ip) {
        JSONObject json = new JSONObject();
        json.put("progress", value);
        json.put("err", false);
        json.put("message", "进度条信息");
        WebSocketServer.SendMessage(json.toJSONString(), ip);
    }
}
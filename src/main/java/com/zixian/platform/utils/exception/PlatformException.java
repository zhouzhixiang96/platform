package com.zixian.platform.utils.exception;

import com.zixian.platform.exception.PlatformExceptionEnum;

/**
 * author: tangzw
 * date: 2024/2/1 16:19
 * description:
 **/
public class PlatformException extends RuntimeException {

    public PlatformException(String errMsg) {
        super(errMsg);
    }
    public PlatformException(PlatformExceptionEnum exceptionEnum) {
        super(exceptionEnum.getMsg());
    }
}

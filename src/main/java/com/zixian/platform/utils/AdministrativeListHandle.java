package com.zixian.platform.utils;

import java.util.List;

import org.apache.ibatis.type.MappedTypes;

import com.alibaba.fastjson.TypeReference;
import com.zixian.platform.entity.AdministrativePenaltyDTO;

@MappedTypes({List.class})
public class AdministrativeListHandle extends ListTypeHandler<AdministrativePenaltyDTO>{

	@Override
	protected TypeReference<List<AdministrativePenaltyDTO>> specificType() {
		return new TypeReference<List<AdministrativePenaltyDTO>>() {
			
        };
	}

}

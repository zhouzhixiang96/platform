package com.zixian.platform.utils;

import java.util.List;

import org.apache.ibatis.type.MappedTypes;

import com.alibaba.fastjson.TypeReference;
import com.zixian.platform.entity.RiskDTO;

@MappedTypes({List.class})
public class RiskListHandle extends ListTypeHandler<RiskDTO>{

	@Override
	protected TypeReference<List<RiskDTO>> specificType() {
		return new TypeReference<List<RiskDTO>>() {
        };
	}

}

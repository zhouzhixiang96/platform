package com.zixian.platform.utils;

/**
 * author: tangzw
 * date: 2024/2/4 16:41
 * description:
 **/
public class PlatformStrUtil {

    public static void printLine() {
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < 30; i++) {
            line.append("--*--");
        }

        System.out.println(line);
    }
}

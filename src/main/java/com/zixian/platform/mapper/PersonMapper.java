package com.zixian.platform.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.cursor.Cursor;

import com.zixian.platform.entity.PersonEntity;
@Mapper
public interface PersonMapper {
	
	@Select("SELECT * FROM person")
//	@Results({
//		@Result(property = "id",column = "id"),
//		@Result(property = "name",column = "name"),
//		@Result(property = "age",column = "age"),
//		@Result(property = "birthday",column = "birthday"),
//		@Result(property = "sex",column = "sex"),
//		@Result(property = "height",column = "height"),
//		@Result(property = "weight",column = "weight"),
//		@Result(property = "money",column = "money"),
//		@Result(property = "adult",column = "adult"),
//		@Result(property = "describe",column = "describe")
//	})
	public List<PersonEntity> getAllPerson();
	
	@Select("SELECT name FROM person")
	@Results({
		@Result(property = "name",column = "name")
	})
	public List<String> getNames();
	
	
	@Insert("INSERT INTO person values(#{id},#{name},#{age},#{birthday},#{sex},#{height},#{weight},#{money},#{adult},#{describe})")
	public int insertPerson(PersonEntity person);

	@Insert({"<script>" + "INSERT INTO person values "
			+ "<foreach collection=\"subList\" item=\"sub\" separator=\",\">"
			+ "(#{sub.id},#{sub.name},#{sub.age},#{sub.birthday},#{sub.sex},#{sub.height},#{sub.weight},#{sub.money},#{sub.adult},#{sub.describe}) "
			+ "</foreach>" + "</script>"})
	public void insertSplice(@Param("subList") List<PersonEntity> subList); 
	
	@Select("SELECT * FROM person WHERE id = #{id}")
	public PersonEntity selectPersonById(@Param("id") String id);
	
	@Select("SELECT COUNT(*) FROM person")
	public int getAllPersonCount();
	
	@Select("SELECT * FROM person LIMIT #{index},#{count}")
	public List<PersonEntity> getPagePerson(@Param("index")int startIndex,@Param("count")int count);
	
	@Select("SELECT * FROM person LIMIT #{limit}")
    @Options(fetchSize = 2)
    Cursor<PersonEntity> findStudentByScan(@Param("limit") long limit);
}

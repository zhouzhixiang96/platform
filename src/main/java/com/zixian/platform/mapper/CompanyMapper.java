package com.zixian.platform.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.zixian.platform.entity.CompanyEntity;
import com.zixian.platform.entity.MutiDimDataEntity;

@Mapper
public interface CompanyMapper {
	
	//插入公司模拟数据 param公司名
	@Insert("INSERT INTO ${tableName} (id_card,name,salary,social_num,tax_num) VALUES(#{id_card},#{name},#{salary},#{social_num},#{tax_num})")
	public int insertCompanyValue(@Param("tableName") String tableName,
								  @Param("id_card") String id_card,
								  @Param("name") String name,
								  @Param("salary") int salary,
								  @Param("social_num") int social_num,
								  @Param("tax_num") int tax_num);
	
	//查询公司模拟收入数据 param公司名
	@Select("SELECT salary FROM ${tableName} LIMIT #{count}")
	public int[] selectSalaryByCount(@Param("tableName") String tableName,@Param("count") int count);

	@Select("SELECT salary FROM ${tableName} where id_card = #{id_card}")
	public int selectSalaryById(@Param("tableName") String tableName,@Param("id_card") String id);



	//插入随机模拟数据
	@Insert({"<script>" + "INSERT INTO muti_dim_data values "
			+ "<foreach collection=\"subList\" item=\"sub\" separator=\",\">"
			+ "(#{sub.province},#{sub.population},#{sub.area},#{sub.salary},#{sub.illiteracy},#{sub.graduate},#{sub.frost},#{sub.linearData},#{sub.randomData}) "
			+ "</foreach>" + "</script>"})
	public int insertLinearData(@Param("subList") List<MutiDimDataEntity> subList);


	//查询随机模拟数据
	@Select("SELECT * FROM muti_dim_data")
	@Results({
		@Result(property = "province",column = "province"),
		@Result(property = "population",column = "population"),
		@Result(property = "area",column = "area"),
		@Result(property = "salary",column = "salary"),
		@Result(property = "illiteracy",column = "illiteracy"),
		@Result(property = "graduate",column = "high_graduate"),
		@Result(property = "frost",column = "frost_days"),
		@Result(property = "linearData",column = "linear_data"),
		@Result(property = "randomData",column = "random_data")
	})
	public List<MutiDimDataEntity> getAllData();
	
//	//插入随机模拟数据
//	@Insert({"<script>" + "INSERT INTO company values "
//			+ "<foreach collection=\"subList\" item=\"sub\" separator=\",\">"
//			+ "(#{sub.USCC},#{sub.companyName},#{sub.marketSize},#{sub.marketValue},#{sub.liabilities},#{sub.revenue},#{sub.netProfit},#{sub.profitRate},#{sub.employee},#{sub.risk},#{sub.administrativePenalty},#{sub.realEstate},#{sub.totalScore}) "
//			+ "</foreach>" + "</script>"})
//	public int insertRandomCompanyData(@Param("subList") List<CompanyEntity> subList);
//	
//	//查询随机模拟数据
//	@Select("SELECT * FROM company")
//	@Results({
//		@Result(property = "USCC",column = "USCC"),
//		@Result(property = "companyName",column = "company_name"),
//		@Result(property = "marketSize",column = "market_size"),
//		@Result(property = "marketValue",column = "market_value"),
//		@Result(property = "liabilities",column = "liabilities"),
//		@Result(property = "revenue",column = "revenue"),
//		@Result(property = "netProfit",column = "net_profit"),
//		@Result(property = "profitRate",column = "profit_rate"),
//		@Result(property = "employee",column = "employee"),
//		@Result(property = "risk",column = "risk"),
//		@Result(property = "administrativePenalty",column = "administrative_penalty"),
//		@Result(property = "realEstate",column = "real_estate"),
//		@Result(property = "totalScore",column = "total_score")
//	})
//	public List<CompanyEntity> getAllCompanyData();
	
}

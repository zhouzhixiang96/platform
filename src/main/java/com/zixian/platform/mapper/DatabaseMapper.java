package com.zixian.platform.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.cursor.Cursor;

import com.zixian.platform.entity.AuthEntity;
import com.zixian.platform.entity.HistoryEntity;
import com.zixian.platform.entity.LogInfoEntity;
import com.zixian.platform.entity.OperatorEntity;
import com.zixian.platform.entity.PartiesEntity;

@Mapper
public interface DatabaseMapper {
	
	//查询所有数据库表
	@Select("SHOW TABLES")
	List<String> getAllTable();

	//查询数据库表的所有字段
	@Select("DESC ${table}")
	List<String> getDescribe(@Param("table") String table);
	
	@Select("SELECT COLUMN_NAME FROM all_tab_columns WHERE owner='zixian' and Table_Name='${table}'")
	List<String> getDescribeByDM(@Param("table") String table);
	
	//通过字段和表名查询数据信息
	@Select("SELECT ${column} FROM ${tableName} LIMIT #{limit}")
    @Options(fetchSize = 2)
    Cursor<String> selectColumn(
    		@Param("tableName") String name,
    		@Param("column") String column,
    		@Param("limit") long limit);
	
	//查询数据库表有多少条数据
	@Select("SELECT COUNT(*) FROM ${tableName}")
	long getTableRowCount(@Param("tableName") String name);
	
	//查询所有的历史数据
	@Select("SELECT * FROM history")
	@Results({
	@Result(property = "id",column = "id"),
	@Result(property = "senderAccount",column = "sender_account"),
	@Result(property = "recieveAccount",column = "recieve_account"),
	@Result(property = "serveId",column = "serve_id"),
	@Result(property = "resultType",column = "result_type"),
	@Result(property = "timestamp",column = "timestamp"),
	@Result(property = "keepTime",column = "keep_time"),
	@Result(property = "totalData",column = "total_data")
	})
//	@Options(fetchSize = 2)
//	Cursor<HistoryEntity> selectHistoryInfo();
	List<HistoryEntity> selectHistoryInfo();
	
	//查询指定数量的历史数据信息
	@Select("SELECT * FROM history ORDER BY id desc LIMIT #{index},#{count}")
	@Results({
	@Result(property = "id",column = "id"),
	@Result(property = "senderAccount",column = "sender_account"),
	@Result(property = "recieveAccount",column = "recieve_account"),
	@Result(property = "serveId",column = "serve_id"),
	@Result(property = "resultType",column = "result_type"),
	@Result(property = "timestamp",column = "timestamp"),
	@Result(property = "keepTime",column = "keep_time"),
	@Result(property = "totalData",column = "total_data")
	})
	List<HistoryEntity> selectHistoryInfoByCount(@Param("index") long index, @Param("count") int count);
	
	@Insert("INSERT INTO history (sender_account,recieve_account,serve_id,result_type,timestamp,keep_time,total_data) "
			+ "values(#{senderAccount},#{recieveAccount},#{serveId},#{resultType},#{timestamp},#{keepTime},#{totalData})")
	public void insertHistory(HistoryEntity par);
	
	//查询所有的日志数据
	@Select("SELECT * FROM log_info")
	@Results({
	@Result(property = "id",column = "id"),
	@Result(property = "logType",column = "log_type"),
	@Result(property = "logContent",column = "log_content"),
	@Result(property = "timestamp",column = "timestamp"),
	@Result(property = "happenPlace",column = "happen_place"),
	@Result(property = "describe",column = "describe")
	})
//	@Options(fetchSize = 2)
//	Cursor<LogInfoEntity> selectLogInfo();
	List<LogInfoEntity> selectLogInfo();
	
	//查询指定数量的日志数据
	@Select("SELECT * FROM log_info ORDER BY id asc LIMIT #{index},#{count}")
	@Results({
	@Result(property = "id",column = "id"),
	@Result(property = "logType",column = "log_type"),
	@Result(property = "logContent",column = "log_content"),
	@Result(property = "timestamp",column = "timestamp"),
	@Result(property = "happenPlace",column = "happen_place"),
	@Result(property = "describe",column = "describe")
	})
	List<LogInfoEntity> selectLogInfoByCount(@Param("index") long index, @Param("count") int count);
	
	//查询指定表的数据总数
	@Select("SELECT COUNT(*) FROM ${tableName}")
	public long getTableCount(@Param("tableName") String tableName);
	
	//查询权限情况
	@Select("SELECT * FROM auth")
	@Results({
	@Result(property = "authId",column = "auth_id"),
	@Result(property = "authLevel",column = "auth_level"),
	@Result(property = "describe",column = "describe")
	})
	public List<AuthEntity> getAllAuthDetail();
	
	//获取所有算子类型
	@Select("SELECT DISTINCT operator_type_name FROM operator_info")
	public List<String> getAllOperatorType();
	
	//按类型获取具体算子
	@Select("SELECT * FROM operator_info WHERE operator_type_name = #{typeName}")
	@Results({
	@Result(property = "operatorName",column = "operator_name"),
	@Result(property = "operatorType",column = "operator_type"),
	@Result(property = "operatorTypeName",column = "operator_type_name"),
	@Result(property = "bit",column = "bit"),
	@Result(property = "describe",column = "describe")
	})
	public List<OperatorEntity> selectOperatorByType(@Param("typeName") String typeName);
	
	@Select("SELECT * FROM ${tableName} LIMIT #{index},5")
	public List<Map<String ,Object>> selectExampleListMap(@Param("index") long index, @Param("tableName") String tableName);
	
}

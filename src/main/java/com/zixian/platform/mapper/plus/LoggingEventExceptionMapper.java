package com.zixian.platform.mapper.plus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zixian.platform.entity.persistence.LoggingEventException;

/**
 * author: tangzw
 * date: 2024/1/17 14:18
 * description:
 **/
public interface LoggingEventExceptionMapper extends BaseMapper<LoggingEventException> {
}

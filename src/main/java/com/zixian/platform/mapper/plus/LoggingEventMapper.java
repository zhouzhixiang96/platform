package com.zixian.platform.mapper.plus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zixian.platform.entity.persistence.LoggingEvent;

/**
 * author: tangzw
 * date: 2024/1/17 11:14
 * description:
 **/
public interface LoggingEventMapper extends BaseMapper<LoggingEvent> {
}

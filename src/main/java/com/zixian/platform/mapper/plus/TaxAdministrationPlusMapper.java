package com.zixian.platform.mapper.plus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zixian.platform.entity.persistence.TaxAdministration;
import com.zixian.platform.entity.result.TaxRatingResult;
import org.apache.ibatis.annotations.MapKey;

import java.util.List;
import java.util.Map;

/**
 * author: tangzw
 * date: 2024/2/1 15:37
 * description:
 **/
public interface TaxAdministrationPlusMapper extends BaseMapper<TaxAdministration> {

    List<TaxRatingResult> getRating(List<String> ids);
}

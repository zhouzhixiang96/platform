package com.zixian.platform.mapper.plus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zixian.platform.entity.persistence.Salary;
import org.apache.ibatis.annotations.Mapper;

/**
 * author: tangzw
 * date: 2024/3/1 14:48
 * description:
 **/
@Mapper
public interface SalaryPlusMapper extends BaseMapper<Salary> {
}

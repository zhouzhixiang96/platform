package com.zixian.platform.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zixian.platform.entity.CompanyEntity;

@Mapper
public interface CompanyPlusMapper extends BaseMapper<CompanyEntity> {

}

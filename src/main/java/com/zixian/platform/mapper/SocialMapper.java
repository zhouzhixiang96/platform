package com.zixian.platform.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zixian.platform.entity.SocialEntity;
@Mapper
public interface SocialMapper {
	
	@Insert({"<script>" + "INSERT INTO social_insurance_person values "
			+ "<foreach collection=\"subList\" item=\"sub\" separator=\",\">"
			+ "(#{sub.id_card},#{sub.name},#{sub.social_insurance_num},#{sub.address},#{sub.cardinal_number},#{sub.first_record_data},#{sub.account_state}) "
			+ "</foreach>" + "</script>"})
	public void insertSplice(@Param("subList") List<SocialEntity> subList); 

}

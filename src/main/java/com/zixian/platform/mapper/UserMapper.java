package com.zixian.platform.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zixian.platform.entity.PartiesEntity;
@Mapper
public interface UserMapper {
	
	@Select("SELECT password FROM user WHERE userName = #{userName}")
	public String selectUserByUserName(@Param("userName") String userName);

	@Select("SELECT * FROM parties JOIN auth on parties.auth_id = auth.auth_id ORDER BY create_timestamp DESC")
	@Results({
	@Result(property = "organizedId",column = "organized_id"),
	@Result(property = "organizedName",column = "organized_name"),
	@Result(property = "phone",column = "phone"),
	@Result(property = "address",column = "address"),
	@Result(property = "dataList",column = "data_list"),
	@Result(property = "authId",column = "auth_id"),
	@Result(property = "logInfo",column = "log_info"),
	@Result(property = "authTips",column = "describe")})
	public List<PartiesEntity> selectAllParties();
	
	@Select("SELECT organized_id FROM parties")
	public List<String> selectAllAccount();
	
	@Select("SELECT data_list FROM parties WHERE organized_id = #{account}")
	public String selectDataTable(@Param("account") String account);
	
	@Select("SELECT auth_id FROM parties WHERE organized_id = #{account}")
	public int selectDataAuth(@Param("account") String account);
	
	@Insert("INSERT INTO parties values(#{organizedId},#{organizedName},#{phone},#{address},#{dataList},#{authId},#{logInfo},'','',#{createTimestamp})")
	public void insertParties(PartiesEntity par);
	
	@Update("UPDATE parties SET organized_name = #{organizedName},phone = #{phone},address = #{address},data_list = #{dataList},auth_id = #{authId} WHERE organized_id = #{organizedId}")
	public int updateParties(PartiesEntity par);
	
	@Delete("DELETE FROM parties WHERE organized_id = #{organizedId}")
	public int deleteParties(@Param("organizedId") String id);
	
	@Update("UPDATE parties SET public_key = #{pKey} WHERE organized_id = #{account}")
	public void updatePublicKey(@Param("account") String account,@Param("pKey") String pKey);
	
	@Update("UPDATE parties SET private_key = #{pKey} WHERE organized_id = #{account}")
	public void updatePrivateKey(@Param("account") String account,@Param("pKey") String pKey);
	
	@Select("SELECT public_key FROM parties WHERE organized_id = #{account}")
	public String selectPublicKey(@Param("account") String account);
	
	@Select("SELECT private_key FROM parties WHERE organized_id = #{account}")
	public String selectPrivateKey(@Param("account") String account);
}

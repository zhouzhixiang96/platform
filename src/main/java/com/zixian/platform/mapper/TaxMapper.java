package com.zixian.platform.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zixian.platform.entity.TaxEntity;
@Mapper
public interface TaxMapper {
	
	@Insert({"<script>" + "INSERT INTO tax_administration values "
			+ "<foreach collection=\"subList\" item=\"sub\" separator=\",\">"
			+ "(#{sub.id_card},#{sub.name},#{sub.tax_account},#{sub.tax_place},#{sub.first_tax_data},#{sub.total_tax}) "
			+ "</foreach>" + "</script>"})
	public void insertSplice(@Param("subList") List<TaxEntity> subList); 

}

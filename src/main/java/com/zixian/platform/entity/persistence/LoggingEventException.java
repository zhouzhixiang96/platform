package com.zixian.platform.entity.persistence;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * author: tangzw
 * date: 2024/1/17 14:18
 * description:
 **/
@Data
public class LoggingEventException {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    private Long eventId;
    private Integer i;
    private String traceLine;
}

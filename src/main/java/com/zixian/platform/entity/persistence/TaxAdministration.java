package com.zixian.platform.entity.persistence;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * author: tangzw
 * date: 2024/2/1 15:32
 * description:
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaxAdministration {

    @TableId(type = IdType.INPUT)
    private String idCard;

    private Double taxAccount;

    private String taxPlace;

    private Date firstTaxData;

    private Double totalTax;

    private String name;

}

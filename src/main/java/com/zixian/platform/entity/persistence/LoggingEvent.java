package com.zixian.platform.entity.persistence;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * author: tangzw
 * date: 2024/1/17 11:06
 * description:
 **/
@Data
public class LoggingEvent {

    @TableId(type = IdType.AUTO)
    private Long eventId;
    private Long timestmp;
    private String formattedMessage;
    private String loggerName;
    private String levelString;
    private String threadName;
    private String referenceFlag;
    private String arg0;
    private String arg1;
    private String arg2;
    private String arg3;
    private String callerFilename;
    private String callerClass;
    private String callerMethod;
    private String callerLine;
    private String traceId;

    private String getHappenPlace() {
        return callerFilename + StrUtil.DASHED +callerClass + StrUtil.DASHED + callerLine;
    }
}

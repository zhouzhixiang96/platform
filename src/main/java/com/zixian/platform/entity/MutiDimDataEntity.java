package com.zixian.platform.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MutiDimDataEntity {
	//省份
	private String province;

	//人口(万人)
	private int population;
	
	//面积(平方千米)
	private double area;
	
	//人均收入
	private double salary;
	
	//文盲数(*100人)
	private double illiteracy;
	
	//高中毕业数(万人)
	private double graduate;
	
	//霜冻天气(天)
	private double frost;
	
	//线性真实结果数据
	private double linearData;
	
	//非线性随机结果数据
	private double randomData;
}

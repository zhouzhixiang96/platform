package com.zixian.platform.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RealEstateDTO {
	//不动产编号
	private String id;
	//名称
	private String name;
	//类型
	private int type;
	//价值
	private double value;
	//地址
	private String address;
	//时间戳
	private long timestamp;
}

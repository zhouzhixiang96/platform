package com.zixian.platform.entity.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * author: tangzw
 * date: 2024/3/7 15:19
 * description:
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperateCalResult {

    private Object[] inputS;
    private Object[] inputR;
    private List<String> result;
    private long totalTime;
    private long calTime;
    private List<String> confoundS;
    private List<String> confoundR;
}

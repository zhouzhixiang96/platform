package com.zixian.platform.entity.result;

import cn.hutool.core.util.StrUtil;
import com.zixian.platform.enums.RateLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * author: tangzw
 * date: 2024/3/1 15:08
 * description:
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RateLevelInfoResult {

    private Integer percentage;
    private RateLevel level;

    public String toFixString(String id) {
        return StrUtil.format("企业{}评分等级为：{}，百分位为：{}%", id, level, percentage);
    }
}

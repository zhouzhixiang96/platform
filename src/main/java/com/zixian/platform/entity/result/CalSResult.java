package com.zixian.platform.entity.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * author: tangzw
 * date: 2024/3/1 10:42
 * description:
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CalSResult<I> {

    List<I> inputS;
}

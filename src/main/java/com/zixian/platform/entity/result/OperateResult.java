package com.zixian.platform.entity.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * author: tangzw
 * date: 2024/2/4 16:32
 * description: I: input类型，R: result类型
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OperateResult<I, R, O> {

    private List<R> result;
    private List<I> inputS;
    private List<I> inputR;
    private List<O> originResult;
    private int decimalBits;
}

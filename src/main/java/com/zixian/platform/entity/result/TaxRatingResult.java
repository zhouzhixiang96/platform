package com.zixian.platform.entity.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * author: tangzw
 * date: 2024/2/1 16:30
 * description:
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaxRatingResult {

    private String idCard;
    private Integer rank;
}

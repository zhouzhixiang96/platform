package com.zixian.platform.entity.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * author: tangzw
 * date: 2024/3/1 11:23
 * description:
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CalRResult<R, I, O> {
    private List<R> result;
    private List<I> inputR;
    private List<O> originResult;
}

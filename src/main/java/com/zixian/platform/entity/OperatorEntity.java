package com.zixian.platform.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel("算子信息")
public class OperatorEntity {
	
	@ApiModelProperty(value = "算子名称",example = "")
	private String operatorName;
	@ApiModelProperty(value = "算子类型",example = "0/1/2")
	private int operatorType;
	@ApiModelProperty(value = "算子类型名称",example = "基础算子/矩阵算子/机器学习算子")
	private String operatorTypeName;
	@ApiModelProperty(value = "数据位数",example = "32/64")
	private int bit;
	@ApiModelProperty(value = "算子描述",example = "")
	private String describe;

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public int getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(int operatorType) {
		this.operatorType = operatorType;
	}

	public String getOperatorTypeName() {
		return operatorTypeName;
	}

	public void setOperatorTypeName(String operatorTypeName) {
		this.operatorTypeName = operatorTypeName;
	}

	public int getBit() {
		return bit;
	}

	public void setBit(int bit) {
		this.bit = bit;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}
}

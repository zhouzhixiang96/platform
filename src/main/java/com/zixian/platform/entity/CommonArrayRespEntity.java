package com.zixian.platform.entity;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * author: tangzw
 * date: 2024/1/17 11:01
 * description:
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommonArrayRespEntity<T> {

    private List<T> data = Lists.newArrayList();
    private long totalTableCount;
}

package com.zixian.platform.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {
	public String getId_card() {
		return id_card;
	}

	public void setId_card(String id_card) {
		this.id_card = id_card;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@ApiModelProperty(value = "身份证号",required = true,example = "4307XXXXXXXXXXXXX")
	public String id_card;
	
	@ApiModelProperty(value = "姓名",required = true,example = "张三")
	public String name;
	
	@ApiModelProperty(value = "居住地址",required = true,example = "湖南省长沙市岳麓区XXXX")
	public String address;
	
	@ApiModelProperty(value = "手机号",required = true,example = "138XXXXXX")
	public String phone;
	
	@ApiModelProperty(value = "公司名称",required = true,example = "紫先科技")
	public String company;
}

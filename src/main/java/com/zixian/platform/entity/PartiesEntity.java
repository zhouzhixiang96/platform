package com.zixian.platform.entity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartiesEntity {
	
	@NotBlank(message = "组织账号不能为空")
	@ApiModelProperty(value = "组织账号,不能为空",required = true,example = "hunan_social")
	@Size(min = 6,max = 12)
	private String organizedId;
	
	@NotBlank(message = "组织名称不能为空")
	@ApiModelProperty(value = "组织名称,不能为空",required = true,example = "社保局")
	private String organizedName;
	
	@ApiModelProperty(value = "联系电话",required = true,example = "0731-XXXXXXX")
	private String phone;
	
	@ApiModelProperty(value = "地址",required = true,example = "湖南省长沙市岳麓区XXXX")
	private String address;
	
	@ApiModelProperty(value = "所持有可提供的数据字段,逗号分隔",required = true,example = "id_card,name,phone")
	private String dataList;

	@ApiModelProperty(value = "组织权限编号",required = true,example = "2")
	private int authId;
	
	@ApiModelProperty(value = "日志/备注信息，不用管此字段",required = false,example = "XXXXXXX")
	private String logInfo;
	
	@ApiModelProperty(value = "权限说明查询时用到，注册/更新时不用管",required = false,example = "提供明文数据")
	private String authTips;
	
	@ApiModelProperty(value = "创建/更新时间戳",required = false,example = "1705463799010")
	private long createTimestamp;

	public long getCreateTimestamp() {
		return createTimestamp;
	}

	public void setCreateTimestamp(long createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

	public String getAuthTips() {
		return authTips;
	}

	public void setAuthTips(String authTips) {
		this.authTips = authTips;
	}

	public String getOrganizedId() {
		return organizedId;
	}

	public void setOrganizedId(String organizedId) {
		this.organizedId = organizedId;
	}

	public String getOrganizedName() {
		return organizedName;
	}

	public void setOrganizedName(String organizedName) {
		this.organizedName = organizedName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDataList() {
		return dataList;
	}

	public void setDataList(String dataList) {
		this.dataList = dataList;
	}

	public int getAuthId() {
		return authId;
	}

	public void setAuthId(int authId) {
		this.authId = authId;
	}

	public String getLogInfo() {
		return logInfo;
	}

	public void setLogInfo(String logInfo) {
		this.logInfo = logInfo;
	}	
}

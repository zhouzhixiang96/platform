package com.zixian.platform.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AdministrativePenaltyDTO {
	//处罚编号
	private String id;
	//处罚名称
	private String name;
	//处罚等级
	private int level;
	//罚金
	private int fine;
	//是否处理
	private boolean deal;
	//时间戳
	private long timestamp;
}

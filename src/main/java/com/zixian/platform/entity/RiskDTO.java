package com.zixian.platform.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RiskDTO {
	//风险名称
	private String riskName;
	//风险等级
	private int riskLevel;
	//时间戳
	private long timestamp;
	//内容
	private String content;

}

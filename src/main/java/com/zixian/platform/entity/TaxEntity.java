package com.zixian.platform.entity;

import java.util.Date;

public class TaxEntity extends UserEntity{
	public double getTax_account() {
		return tax_account;
	}

	public void setTax_account(double tax_account) {
		this.tax_account = tax_account;
	}

	public String getTax_place() {
		return tax_place;
	}

	public void setTax_place(String tax_place) {
		this.tax_place = tax_place;
	}

	public Date getFirst_tax_data() {
		return first_tax_data;
	}

	public void setFirst_tax_data(Date first_tax_data) {
		this.first_tax_data = first_tax_data;
	}

	public double getTotal_tax() {
		return total_tax;
	}

	public void setTotal_tax(double total_tax) {
		this.total_tax = total_tax;
	}

	private double tax_account;
	
	private String tax_place;
	
	private Date first_tax_data;
	
	private double total_tax;
}

package com.zixian.platform.entity;

import java.util.Date;

public class SocialEntity extends UserEntity{
	public double getSocial_insurance_num() {
		return social_insurance_num;
	}

	public void setSocial_insurance_num(double social_insurance_num) {
		this.social_insurance_num = social_insurance_num;
	}

	public double getCardinal_number() {
		return cardinal_number;
	}

	public void setCardinal_number(double cardinal_number) {
		this.cardinal_number = cardinal_number;
	}

	public Date getFirst_record_data() {
		return first_record_data;
	}

	public void setFirst_record_data(Date first_record_data) {
		this.first_record_data = first_record_data;
	}

	public int getAccount_state() {
		return account_state;
	}

	public void setAccount_state(int account_state) {
		this.account_state = account_state;
	}

	private double social_insurance_num;
	
	private double cardinal_number;
	
	private Date first_record_data;
	
	private int account_state;
}

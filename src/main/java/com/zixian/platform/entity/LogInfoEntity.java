package com.zixian.platform.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogInfoEntity {
	
	private long id;
	
	private String logType;
	
	private String logContent;
	
	private long timestamp;
	
	private String happenPlace;
	
	private String describe;

}

package com.zixian.platform.entity;

import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.zixian.platform.utils.AdministrativeListHandle;
import com.zixian.platform.utils.ListTypeHandler;
import com.zixian.platform.utils.RealListHandle;
import com.zixian.platform.utils.RiskListHandle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@TableName(value = "company", autoResultMap = true)
@AllArgsConstructor
@NoArgsConstructor
public class CompanyEntity {
	//社会统一信用码
	@TableId(value = "USCC", type = IdType.INPUT)
	private String USCC;
	//公司名称
	private String companyName;
	//市场占有率
	private double marketSize;
	//市值
	private double marketValue;
	//负债（万元）
	private double liabilities;
	//总收入
	private double revenue;
	//净利润（万元）
	private double netProfit;
	//利率（%）
	private double profitRate;
	//员工总数
	private long employee;
	//风险
//	@TableField(value = "risk", typeHandler = RiskListHandle.class)
	@TableField(value = "risk", typeHandler = FastjsonTypeHandler.class)
	private List<RiskDTO> risk;
	//行政处罚
//	@TableField(value = "administrative_penalty", typeHandler = AdministrativeListHandle.class)
	@TableField(value = "administrative_penalty", typeHandler = FastjsonTypeHandler.class)
	private List<AdministrativePenaltyDTO> administrativePenalty;
	//不动产
//	@TableField(value = "real_estate", typeHandler = RealListHandle.class)
	@TableField(value = "real_estate", typeHandler = FastjsonTypeHandler.class)
	private List<RealEstateDTO> realEstate;
	//总评分
	private long totalScore;
}

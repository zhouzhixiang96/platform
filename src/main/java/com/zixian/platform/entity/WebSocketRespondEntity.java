package com.zixian.platform.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@ApiModel("显示信息")
public class WebSocketRespondEntity {
	
	@ApiModelProperty(value = "显示位置",example = "发起方/协同方/平台")
	private String showName;
	@ApiModelProperty(value = "显示位置类型",example = "发起方日志(0)/协同方日志(1)/平台日志(2)/发送方数据(3)/协同方数据(4)")
	private int showType;
	@ApiModelProperty(value = "是否有错误",example = "true/false")
	private boolean error;
	@ApiModelProperty(value = "需要显示的信息",example = "")
	private Object data;

	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public int getShowType() {
		return showType;
	}

	public void setShowType(int showType) {
		this.showType = showType;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public Object getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public WebSocketRespondEntity(String showName,int showType,boolean err,Object data)
	{
		this.showName = showName;
		this.showType = showType;
		this.error = err;
		this.data = data;
	}
	
	public static WebSocketRespondEntity SENDERSUCC(String data)
	{
		return new WebSocketRespondEntity("发送方", 0, false, data);
	}
	
	public static WebSocketRespondEntity RECIEVESUCC(String data)
	{
		return new WebSocketRespondEntity("协同方", 1, false, data);
	}
	
	public static WebSocketRespondEntity PLATFORMSUCC(String data) 
	{
		return new WebSocketRespondEntity("平台", 2, false, data);
	}
	
	public static WebSocketRespondEntity SENDERERR(String data)
	{
		return new WebSocketRespondEntity("发送方", 0, true, data);
	}
	
	public static WebSocketRespondEntity RECIEVEERR(String data)
	{
		return new WebSocketRespondEntity("协同方", 1, true, data);
	}
	
	public static WebSocketRespondEntity PLATFORMERR(String data) 
	{
		return new WebSocketRespondEntity("平台", 2, true, data);
	}
	
	public static WebSocketRespondEntity BUILD(String showName,int showType,boolean err,Object data) 
	{
		return new WebSocketRespondEntity(showName, showType, err, data);
	}
}

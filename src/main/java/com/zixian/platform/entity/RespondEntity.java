package com.zixian.platform.entity;

import com.zixian.platform.exception.PlatformExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RespondEntity<T> {
	private int errCode;
	
	private String errMsg;
	
	private T data;

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
    public RespondEntity(int code, String msg) {
        this.errCode = code;
        this.errMsg = msg;
    }

    public RespondEntity(int code, String msg, T data) {
        this.errCode = code;
        this.errMsg = msg;
        this.data = data;
    }
    
    /**
     * 成功 返回（data数据）成功信息
     *
     * @param data
     * @return
     */
    public static <T> RespondEntity<T> SUCCESS(T data) {
        return new RespondEntity(0, "操作成功", data);
    }

    /**
     * 成功 返回自定义（消息、data数据）成功信息
     *
     * @param msg
     * @param data
     * @return
     */
    public static RespondEntity SUCCESS(String msg, String data) {
        return new RespondEntity(0, msg, data);
    }

    /**
     * 失败 返回默认失败信息
     *
     * @return
     */
    public static RespondEntity ERROR() {
        return new RespondEntity(-1, "操作失败", "");
    }

    /**
     * 失败 返回自定义（消息）失败信息
     *
     * @param msg
     * @return
     */
    public static RespondEntity ERROR(String msg) {
        return new RespondEntity(-1, msg, "");
    }

    /**
     * 失败 返回自定义（消息、状态码）失败信息
     *
     * @param code
     * @param msg
     * @return
     */
    public static RespondEntity ERROR(Integer code, String msg) {
        return new RespondEntity(code, msg, "");
    }

    public static RespondEntity ERROR(PlatformExceptionEnum exception) {
        return new RespondEntity(exception.getCode(), exception.getMsg(), "");
    }

}
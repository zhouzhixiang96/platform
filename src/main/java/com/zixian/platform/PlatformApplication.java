package com.zixian.platform;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan("com.zixian.platform.mapper")  // mapper 包路径
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
@EnableScheduling
public class PlatformApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(PlatformApplication.class, args);
	}

}

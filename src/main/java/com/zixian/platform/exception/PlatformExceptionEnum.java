package com.zixian.platform.exception;

import lombok.Getter;

/**
 * author: tangzw
 * date: 2024/1/24 14:29
 * description:
 **/
@Getter
public enum PlatformExceptionEnum {
    COLUMN_NOT_FOUND(90001, "无可用字段"),
    DATA_LIST_EMPTY(90002, "数据目录为空"),
    PERCENTAGE_INVALID(90003, "百分数非法"),
    OPERATOR_NOT_EXISTS(90004, "算子暂不支持"),

    //distribute
    CALS_ERROR(80001, "calS出错"),
    CAL_ERROR(80002, "算子s、r数据不匹配"),
    ;

    private final int code;
    private final String msg;

    PlatformExceptionEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

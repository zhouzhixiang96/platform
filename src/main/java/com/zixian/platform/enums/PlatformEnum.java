package com.zixian.platform.enums;

public enum PlatformEnum {
	
	AUTH_LEVEL_1("不提供数据、不提供密态数据",1),
	AUTH_LEVEL_2("不提供明文数据、提供密态数据",2),
	AUTH_LEVEL_3("需授权特定提供明文数据",3),
	AUTH_LEVEL_4("提供明文数据",4),
	AUTH_LEVEL_5("可要求密态数据协同",5),
	;
	private  String name;
	private  int authId;
	 
	PlatformEnum(String name,int id) {
        this.name = name;
        this.authId = id;
    }
 
    public String getName() {
        return name;
    }

	public int getAuthId() {
		return authId;
	}
}

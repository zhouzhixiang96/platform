package com.zixian.platform.enums;

import com.zixian.platform.exception.PlatformExceptionEnum;
import com.zixian.platform.utils.exception.PlatformException;

/**
 * author: tangzw
 * date: 2024/2/1 15:26
 * description:
 **/
public enum RateLevel {
    A(20),
    B(40),
    C(60),
    D(80),
    E(100),
    ;

    private final int percentage;

    RateLevel(int percentage) {
        this.percentage = percentage;
    }

    public static RateLevel getByPercentage(int percentage) {
        for (RateLevel level : values()) {
            if (level.percentage >= percentage) {
                return level;
            }
        }

        throw new PlatformException(PlatformExceptionEnum.PERCENTAGE_INVALID);
    }
}

package com.zixian.platform.enums;

/**
 * author: tangzw
 * date: 2024/3/1 10:45
 * description:
 **/
public class PlatformConstant {

    public static final String OP64 = "64";
    public static final String OP32 = "32";
}

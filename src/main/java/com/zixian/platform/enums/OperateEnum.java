package com.zixian.platform.enums;

/**
 * type:0 32个整数	用于基础算子
 * type:1 32*32个整数	用于矩阵算子
 * type:2 32个定点数	用于机器学习算子
 * type:3 32*32个定点数	用于机器学习算子
 * type:4 32*32——3*3定点数	用于卷积
 */

public enum OperateEnum {
	LESS32("Less32", 0),
	LESS64("Less64", 0),
	ADD32("Add32",0),
	ADD64("Add64",0),
	SUB32("Sub32",0),
	SUB64("Sub64",0),
	SUM32("Sum32",0),
	SUM64("Sum64",0),
//	XOR32("Xor32",0),
	XOR64("Xor64",0),
	SECMUL32("Secmul32",0),
	SECMUL64("Secmul64",0),
	MATMUL32("Matmul32",1),
	MATMUL64("Matmul64",1),
	MATPROD32("Matprod32",1),
	MATPROD64("Matprod64",1),
	MATTRANS32("Mattrans32",1),
	MATTRANS64("Mattrans64",1),
	EXP64("Exp64",2),
	EXP32("Exp32",2),
	LAYERNORM64("LayerNorm64", 2),
	ELU64("Elu64",2),
	LEAKLYRELU64("LeaklyRelu64",2),
	LINER64("Liner64",2),
	RELU64("Relu64",2),
	SIGMOID64("Sigmoid64",2),
	SIGMOID32("Sigmoid32", 2),
	SILU64("Silu64",2),
	SILU32("Silu32", 2),
	SOFTMAX64("SoftMax64",2),
	SOFTMAX32("Softmax32", 5),
	TANH64("Tanh64",2),
	TANH32("Tanh32", 2),
	RMSNORM64("RmsNorm64",2),
	RMSNORM32("Rmsnorm32", 2),
	ATTENTION64("Attention64",3),
	BATCHNORM64("BatchNorm64", 4),
	BATCHNORM32("Batchnorm32", 4),
	CONV2D64("Conv2d64",4),
	CONV2D32("Conv2d32", 4),
	;

	private  String name;

	//0：0-100的整数
	//1：0-100的整数矩阵
	//2: 0-1的定点数
	//3: 0-1的定点数矩阵
	//4: 0-1的大小不同的定点数矩阵
	//5: S为0-1的定点数，R全为0
	private  int type;
	 
	OperateEnum(String name,int id) {
        this.name = name;
        this.type = id;
    }
 
    public String getName() {
        return name;
    }
    
    public int getType()
    {
    	return type;
    }
    
    public static OperateEnum toOperateEnum(String value) {
    	for(OperateEnum o : OperateEnum.values())
    	{
    		if(o.name.equalsIgnoreCase(value))return o;
    	}
		return null;
    }

    public static int getTypeId(String value) {
    	for(OperateEnum o : OperateEnum.values())
    	{
    		if(o.name.equalsIgnoreCase(value))return o.getType();
    	}
		return -1;
    }
}

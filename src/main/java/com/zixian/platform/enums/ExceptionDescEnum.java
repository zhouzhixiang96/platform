package com.zixian.platform.enums;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;

/**
 * author: tangzw
 * date: 2024/1/25 15:54
 * description:
 **/
@Getter
public enum ExceptionDescEnum {
    DEBUG("debug", "系统跟踪日志"),
    INFO("info", "系统记录"),
    WARN("warn", "系统警告"),
    ERROR("error", "系统错误"),
    ;

    private final String level;
    private final String desc;

    ExceptionDescEnum(String level, String desc) {
        this.level = level;
        this.desc = desc;
    }

    public static ExceptionDescEnum getByLevel(String level) {
        for (ExceptionDescEnum e : values()) {
            if (StrUtil.equalsIgnoreCase(e.level, level)) {
                return e;
            }
        }

        return null;
    }

    public static String getDescByLevel(String level) {
        for (ExceptionDescEnum e : values()) {
            if (StrUtil.equalsIgnoreCase(e.level, level)) {
                return e.getDesc();
            }
        }

        return "UNKNOWN";
    }
}

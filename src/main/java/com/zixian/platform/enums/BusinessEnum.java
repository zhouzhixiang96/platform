package com.zixian.platform.enums;

public enum BusinessEnum{

	PSI("隐私求交",0),
	PIR_OT("匿踪查询-不经意传输",1),
	PIR_LAG("匿踪查询-拉格朗日插值法",2),
	ENTERPRISE_CREDIT_RATING("企业资信评分",3);
	
	private  String name;
	
	private  int businessId;
	 
	BusinessEnum(String name,int id) {
        this.name = name;
        this.businessId = id;
    }
 
    public String getName() {
        return name;
    }
    
    public int getBusinessId()
    {
    	return businessId;
    }

}

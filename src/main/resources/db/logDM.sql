CREATE TABLE IF NOT EXISTS "logging_event"
(
"timestmp" BIGINT NOT NULL,
"formatted_message" TEXT NOT NULL,
"logger_name" VARCHAR(254) NOT NULL,
"level_string" VARCHAR(254) NOT NULL,
"thread_name" VARCHAR(254),
"reference_flag" SMALLINT,
"arg0" VARCHAR(64),
"arg1" VARCHAR(254),
"arg2" VARCHAR(254),
"arg3" VARCHAR(254),
"caller_filename" VARCHAR(254) NOT NULL,
"caller_class" VARCHAR(254) NOT NULL,
"caller_method" VARCHAR(254) NOT NULL,
"caller_line" CHAR(4) NOT NULL,
"event_id" BIGINT IDENTITY(31, 1) NOT NULL,
"trace_id" VARCHAR(100),
NOT CLUSTER PRIMARY KEY("event_id")
) STORAGE(ON "MAIN", CLUSTERBTR);

CREATE INDEX IF NOT EXISTS "trace_id_idx" ON "logging_event"("trace_id" DESC) STORAGE(ON "MAIN", CLUSTERBTR) ;
COMMIT;

CREATE TABLE IF NOT EXISTS "logging_event_exception"
(
"id" BIGINT IDENTITY(127, 1) NOT NULL,
"event_id" BIGINT NOT NULL,
"i" SMALLINT NOT NULL,
"trace_line" VARCHAR(254) NOT NULL,
NOT CLUSTER PRIMARY KEY("id")
) STORAGE(ON "MAIN", CLUSTERBTR);

CREATE INDEX IF NOT EXISTS "event_id_idx" ON "zixian"."logging_event_exception"("event_id" DESC) STORAGE(ON "MAIN", CLUSTERBTR) ;
COMMIT;

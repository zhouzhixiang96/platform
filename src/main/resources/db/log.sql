BEGIN;
CREATE TABLE IF NOT EXISTS logging_event
  (
    timestmp         BIGINT NOT NULL,
    formatted_message  TEXT NOT NULL,
    logger_name       VARCHAR(254) NOT NULL,
    level_string      VARCHAR(254) NOT NULL,
    thread_name       VARCHAR(254),
    reference_flag    SMALLINT,
    arg0              VARCHAR(64),
    arg1              VARCHAR(254),
    arg2              VARCHAR(254),
    arg3              VARCHAR(254),
    caller_filename   VARCHAR(254) NOT NULL,
    caller_class      VARCHAR(254) NOT NULL,
    caller_method     VARCHAR(254) NOT NULL,
    caller_line       CHAR(4) NOT NULL,
    event_id          BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    trace_id          VARCHAR(100),
    Index `trace_id_idx` (trace_id)
  )ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

BEGIN;
CREATE TABLE IF NOT EXISTS logging_event_exception
  (
    id               BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    event_id         BIGINT NOT NULL,
    i                SMALLINT NOT NULL,
    trace_line       VARCHAR(254) NOT NULL,
    Index `event_id_idx` (event_id)
  )ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

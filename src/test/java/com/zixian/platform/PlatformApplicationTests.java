package com.zixian.platform;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.ibatis.cursor.Cursor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.zixian.platform.controller.DemoHelloWorld;
import com.zixian.platform.djl.Operators;
import com.zixian.platform.entity.AdministrativePenaltyDTO;
import com.zixian.platform.entity.CompanyEntity;
import com.zixian.platform.entity.MutiDimDataEntity;
import com.zixian.platform.entity.PersonEntity;
import com.zixian.platform.entity.RealEstateDTO;
import com.zixian.platform.entity.RiskDTO;
import com.zixian.platform.entity.SocialEntity;
import com.zixian.platform.entity.TaxEntity;
import com.zixian.platform.entity.WebSocketRespondEntity;
import com.zixian.platform.mapper.CompanyMapper;
import com.zixian.platform.mapper.DatabaseMapper;
import com.zixian.platform.mapper.PersonMapper;
import com.zixian.platform.mapper.SocialMapper;
import com.zixian.platform.mapper.TaxMapper;
import com.zixian.platform.service.CompanyServicePlus;
import com.zixian.platform.utils.cuckoo.CuckooHashTable;
import com.zixian.platform.utils.cuckoo.CuckooHashTable.HashFamily;
import com.zixian.platform.utils.math.MathUtils;
import com.zixian.platform.utils.math.MathUtils.LagrangeInterpolationFormula;
import com.zixian.platform.utils.paillier.CommonUtils;
import com.zixian.platform.utils.redis.RedisUtils;

//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PlatformApplicationTests {
	
	//定义散列函数集合
    HashFamily<String> hashFamily = new HashFamily<String>() {
        //根据which选取不同的散列函数
        @Override
        public int hash(String x, int which) {
            int hashVal = 0;
            int n = x.length();
            switch (which){
                case 0:{
            		int b = 378551;
            		int a = 63689;
            		for (int i = 0; i < n; i++) {
            			hashVal = hashVal * a + x.charAt(i);
            			a = a * b;
            		}
                    break;
                }
                case 1:
                    for (int i = 0; i < n; i ++){
                        hashVal = 37 * hashVal + x.charAt(i);
                    }
                    break;
                case 2:
                    for (int i = 0; i < n; i ++){
                        hashVal = 204 * hashVal + x.charAt(i);
                    }
                    break;
                case 3:
            		for (int i = 0; i < n; i++) {
            			if ((i & 1) == 0) {
            				hashVal ^= ((hashVal << 7) ^ x.charAt(i) ^ (hashVal >> 3));
            			} else {
            				hashVal ^= (~((hashVal << 11) ^ x.charAt(i) ^ (hashVal >> 5)));
            			}
            		}
                	break;
            }
            return hashVal;
        }
        //返回散列函数集合的个数
        @Override
        public int getNumberOfFunctions() {
            return 3;
        }

        @Override
        public void generateNewFunctions() {

        }
    };
    
    private static volatile int num = 0;
//	@Test
	public void contextLoads() throws UnknownHostException {
		InetAddress host=InetAddress.getLocalHost();
        String hostAddress=host.getHostAddress();
        int pos=hostAddress.lastIndexOf(".");
 
        //获得本机ip的网段
        String bigNet=hostAddress.substring(0,pos+1);
        List<String> ips= Collections.synchronizedList(new ArrayList<>());
        for (int i=0;i<=225;i++){
            String ip=bigNet+i;
            new Thread(()->{
                try {
                    Process process = Runtime.getRuntime().exec("ping "+ip+" -w 280 -n 1");
                    InputStream inputStream=process.getInputStream();
                    InputStreamReader inputStreamReader=new InputStreamReader(inputStream,"GBK");
                    BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
                    String line=null;
                    String isActive=null;
                    while((line=bufferedReader.readLine()) != null) {
                        if(!line.equals("")){
                            isActive=bufferedReader.readLine().substring(0,2);
                            break;
                        }
                    }
                    if(isActive.equals("来自")){
                        ips.add(ip);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                num++;
            }).start();
        }
        while (num!=225) {}
        System.out.println(ips);
	}
	
//	@Test
	public void insertLinearRegression()
	{
//		String[] chineseProvinceNames = {"北京市", "天津市", "河北省", "山西省", "内蒙古自治区", "辽宁省", "吉林省", "黑龙江省", "上海市", "江苏省", "浙江省", "安徽省", "福建省", "江西省", "山东省", "河南省", "湖北省", "湖南省", "广东省", "广西壮族自治区", "海南省", "重庆市", "四川省", "贵州省", "云南省", "西藏自治区", "陕西省", "甘肃省", "青海省", "宁夏回族自治区", "新疆维吾尔自治区"};
//		List<MutiDimDataEntity> listMul = new LinkedList<MutiDimDataEntity>();
//		Random random = new Random();
//		for(int i = 0 ; i < chineseProvinceNames.length ; i++)
//		{
//			String provinceName = chineseProvinceNames[i];
//			int population = random.nextInt(1,8);
//			double area = random.nextDouble(1.05,1.307);
//			double salary = random.nextDouble(3.25455,6.45812);
//			double illiteracy = random.nextDouble(0.789,9);
//			double graduate = random.nextDouble(5.41,7);
//			double frost = random.nextDouble(1.4,3);
//			double linearData = random.nextDouble(0.00015,0.00025) * population
//							+  	0 * area
//							+	random.nextDouble(-0.015,-0.012) * salary
//							+	random.nextDouble(2.015,2.135) * illiteracy
//							+ 	random.nextDouble(-1.055,-0.835) * graduate
//							+	0 * frost;
//			MutiDimDataEntity mul = new MutiDimDataEntity(provinceName, population, area, salary, illiteracy, graduate, frost, linearData, random.nextDouble(2548.55,3025.12));
//			listMul.add(mul);
//		}
//		System.out.println(companyMapper.insertLinearData(listMul));
		
//		String[] chineseProvinceNames = {"北京市", "天津市", "河北省", "山西省", "内蒙古自治区", "辽宁省", "吉林省", "黑龙江省", "上海市", "江苏省", "浙江省", "安徽省", "福建省", "江西省", "山东省", "河南省", "湖北省", "湖南省", "广东省", "广西壮族自治区", "海南省", "重庆市", "四川省", "贵州省", "云南省", "西藏自治区", "陕西省", "甘肃省", "青海省", "宁夏回族自治区", "新疆维吾尔自治区"};
//		List<MutiDimDataEntity> listMul = new LinkedList<MutiDimDataEntity>();
//		Random random = new Random();
//		for(int i = 0 ; i < 100 ; i++)
//		{
//			String provinceName = chineseProvinceNames[i % chineseProvinceNames.length] + i / chineseProvinceNames.length;
//			int population = random.nextInt(1,8);
//			double area = random.nextDouble(1.05,1.307);
//			double salary = random.nextDouble(3.25455,6.45812);
//			double illiteracy = random.nextDouble(0.789,9);
//			double graduate = random.nextDouble(5.41,7);
//			double frost = random.nextDouble(1.4,3);
//			double linearData = 1.5 * population
//							+  	0.005 * area
//							+	(-2.5) * salary
//							+	4.17 * illiteracy
//							+ 	(-2.168) * graduate
//							+	0.125 * frost;
//			MutiDimDataEntity mul = new MutiDimDataEntity(provinceName, population, area, salary, illiteracy, graduate, frost, linearData, random.nextDouble(2548.55,3025.12));
//			listMul.add(mul);
//		}
//		System.out.println(companyMapper.insertLinearData(listMul));
	}
	
	@Autowired
	private CompanyMapper companyMapper;
	
//	@Test
	public void insertCompany() {
//		Faker FAKER = new Faker(Locale.CHINA);
//		Random random = new Random();
//		int randomNumberLength = 18;
//		for(int i = 0 ; i < 1000000 ; i++)
//		{
//        	long randomNumber = random.nextLong((long)Math.pow(10, randomNumberLength));
//        	String id_card = String.format("%018d", randomNumber);
//			companyMapper.insertCompanyValue("company_a", id_card, FAKER.name().name(), random.nextInt(3000,100000), random.nextInt(3000,100000), random.nextInt(3000,100000));
//		}
//		for(int i = 0 ; i < 1000000 ; i++)
//		{
//        	long randomNumber = random.nextLong((long)Math.pow(10, randomNumberLength));
//        	String id_card = String.format("%018d", randomNumber);
//			companyMapper.insertCompanyValue("company_b", id_card, FAKER.name().name(), random.nextInt(3000,100000), random.nextInt(3000,100000), random.nextInt(3000,100000));
//		}
		
	}
	
//	@Test
	public void getMatAS()
	{
		long[] tmp = new long[16*16];
		double seed = 0.05;
		for(int i = 0 ; i < 16 ; i++)
		{
			seed = seed * (i + 1);
			for(int j = 0 ; j < 16 ; j++)
			{
				tmp[i*16 + j] = (long) (seed*(Math.pow(2,32)));
				seed += 0.05;
			}
			seed = 0.05;
		}
		
		for(int row = 0 ; row < 16 ; row++)
		{
			for(int col = 0 ; col < 16 ; col++)
			{
				System.out.print(tmp[row*16+col]);
				System.out.print("\t");
			}
			System.out.print("\n");
		}
	}
	
//	@Test
	public void getMatAR()
	{
		long[] tmp = new long[16*16*4];
		double seed = 0.05;
		for(int i = 0 ; i < 16 ; i++)
		{
			seed = seed * (i + 2);
			for(int j = 0 ; j < 16 ; j++)
			{
				tmp[i*16 + j] = (long) (seed*(1 << 32));
				seed += 0.05;
			}
			seed = 0.05;
		}
		for(int i = 0 ; i < 16 ; i++)
		{
			seed = seed * (i + 3);
			for(int j = 0 ; j < 16 ; j++)
			{
				tmp[16*16 + i*16 + j] = (long) (seed*(1 << 32));
				seed += 0.05;
			}
			seed = 0.05;
		}
		for(int i = 0 ; i < 16 ; i++)
		{
			seed = seed * (i + 4);
			for(int j = 0 ; j < 16 ; j++)
			{
				tmp[16*16*2 + i*16 + j] = (long) (seed*(1 << 32));
				seed += 0.05;
			}
			seed = 0.05;
		}

	}
	
//	@Test
	public void testBinaryFile() {
//		File file = new File("C:\\Users\\Administrator\\Desktop\\test.bin");  //文件对象
		 
        //使用二进制 I/O 将 100 个随机生成的整数写入文件
//        try(DataOutputStream outputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
//            for (int i = 0; i < 100; i++)
//            {
//            	ByteBuffer buffer = ByteBuffer.allocate(4);
//            	buffer.putFloat(new SecureRandom().nextFloat());
//            	buffer.order(ByteOrder.LITTLE_ENDIAN);
//            	outputStream.writeFloat(buffer.getFloat(0));
//            }
////                outputStream.writeFloat(new SecureRandom().nextFloat());
//        } catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
 
//		File file = new File("C:\\Users\\Administrator\\Desktop\\server data\\x_train.bin");
//		File file = new File("C:\\Users\\Administrator\\Desktop\\x_train.bin");
//        //打印数据
//        try(DataInputStream inputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)))) {
//            try {
//                while (true)
//                {
//                	int ch1 = inputStream.read();
//                	int ch2 = inputStream.read();
//            		int ch3 = inputStream.read();
//            		int ch4 = inputStream.read();
//            		if ((ch1 | ch2 | ch3 | ch4) < 0)
//            			throw new EOFException();
//            		System.out.println(Float.intBitsToFloat((ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0)));
//                }
//                    
//            } catch (EOFException ex) {
//            	
//            }
//        } catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	
	
	@Autowired
	private PersonMapper personMapper;
	
//	@Test
	public void testBSearch()
	{
		System.out.println("start:" + getCurrentTime());
		List<PersonEntity> lp = personMapper.getAllPerson();
		System.out.println("finish:" + getCurrentTime() + "count:" + lp.size());
	}
	
//	@Test
	public void testDatabase() {
		//手动创建线程池，注意你 数据库连接池的 允许连接数量，别超过了就行。
		System.out.println("select all:" + getCurrentTime());
		int total = personMapper.getAllPersonCount();
		int page = 100000;
		int maxThread = (total + page -1) / page;
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
        		maxThread,
        		maxThread,
                300,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(maxThread),
                (r, executor) -> System.out.println("拒绝" + r));
        List<PersonEntity> entityList = new ArrayList<>();
        //使用CountDownLatch保证所有线程都执行完成
        CountDownLatch latch = new CountDownLatch(maxThread);
        System.out.println("start thread:" + getCurrentTime());
		for(int i = 0 ; i < maxThread ; i++)
		{
			int index = i;
			poolExecutor.execute(() -> {
				entityList.addAll(personMapper.getPagePerson(index*page, page));
	            latch.countDown();
			 });
		}
		
        try {
			latch.await(100,TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        poolExecutor.shutdown();
        System.out.println(entityList.size() + getCurrentTime());
	}
	
	//mybatis流式查询数据库
//	@Test
//	@Transactional
	public void testFlowSelectDatabase()
	{
//		List<PersonEntity> students=new ArrayList<>();
		CuckooHashTable<String> cuckooHashTable = new CuckooHashTable<String>(hashFamily, 2000000);    
	    //缺点：注意 Spring 框架当中注解使用的坑：只在外部调用时生效。
	    // 在当前类中调用这个方法，依旧会报错。
		System.out.println("start count:" + getCurrentTime());
		int total = personMapper.getAllPersonCount();
	    Cursor<PersonEntity> cursor2 =personMapper.findStudentByScan(total);
	    System.out.println("start select:" + getCurrentTime());
	    //Cursor 实现了迭代器接口，因此在实际使用当中，从 Cursor 取数据非常简单
	    cursor2.forEach(stu -> {
//	    	System.out.println("游标的当前索引CurrentIndex："+cursor2.getCurrentIndex());
//	    	System.out.println("用于在取数据之前判断 Cursor 对象是否是打开状态。" +
//	                "只有当打开时 Cursor 才能取数据；："+cursor2.isOpen()); //：true
//	    	System.out.println("用于判断查询结果是否全部取完："+cursor2.isConsumed()); //false
//	    	System.out.println("输出的信息："+ JSON.toJSONString(stu));
	        //可以对数据进行处理，主要用于处理大批量的数据
//	        students.add(stu);
	    	cuckooHashTable.insert(stu.getId());
	    });
	    System.out.println("end select:" + getCurrentTime());
	    System.out.println("返回多少条数据："+(cursor2.getCurrentIndex()+1)); //返回多少条数据：需要加1才是返回总数
	    System.out.println("用于在取数据之前判断 Cursor 对象是否是打开状态。" +
	            "只有当打开时 Cursor 才能取数据2；："+cursor2.isOpen()); //：false
	    System.out.println("用于判断查询结果是否全部取完2："+cursor2.isConsumed()); //true
	    if(cursor2.isConsumed()){
	    	System.out.println("final");
	    	cuckooHashTable.printArray();
	    }else {
	    	System.out.println("err");
	    }
	}
	
	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	
	@Autowired
	private PlatformTransactionManager transactionManager;
	
//	@Test
//	@Async("threadPoolTaskExecutor")// 提交到线程池中去处理
	public void insertDatabase() {
		Faker FAKER = new Faker(Locale.CHINA);
    	ArrayList<PersonEntity> arrayList = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        // 模拟数据
        int randomNumberLength = 18;
		Random random = new Random();
        
        
        for (int i = 0; i < 10000000; i++){
//        	PersonEntity person = new PersonEntity();
//        	person.setAdult(FAKER.bool().bool());
//        	person.setAge(FAKER.number().randomDigit());
//        	person.setBirthday(FAKER.date().birthday());
//        	person.setHeight(FAKER.number().randomDouble(1, 150, 200));
//        	long randomNumber = random.nextLong((long)Math.pow(10, randomNumberLength));
//        	person.setId(String.format("%018d", randomNumber));
//        	person.setMoney(new BigDecimal(FAKER.number().randomDigit()));
//        	person.setName(FAKER.name().name());
//        	person.setSex('男');
//        	person.setWeight(FAKER.number().randomDouble(1, 100, 200));
//        	person.setDescribe(FAKER.toString());
//            arrayList.add(person);
        }
        System.out.println("start insert");
        int count = arrayList.size();
        int pageSize = 10000; // 每批次插入的数据量
        int threadNum = count / pageSize; // 线程数
        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        for (int i = 0; i < threadNum; i++) {
            int startIndex = i * pageSize;
            int endIndex = Math.min(count, (i + 1) * pageSize);
            List<PersonEntity> subList = arrayList.subList(startIndex, endIndex);
            threadPoolTaskExecutor.execute(() -> {
                DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
                TransactionStatus status = transactionManager.getTransaction(transactionDefinition);
                try {
                    personMapper.insertSplice(subList);
                    transactionManager.commit(status);
                } catch (Exception e) {
                    transactionManager.rollback(status);
                    throw e;
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

	}
	
	@Autowired
	private SocialMapper socialMapper;
	
	@Autowired
	private DatabaseMapper databaseMapper;
	
	@Autowired
	private TaxMapper taxMapper;
	
	//mybatis流式查询数据库
//	@Test
//	@Transactional
	public void test()
	{
//		CuckooHashTable<String> cuckooHashTable = new CuckooHashTable<String>(hashFamily, 2000000);    
	    //缺点：注意 Spring 框架当中注解使用的坑：只在外部调用时生效。
	    // 在当前类中调用这个方法，依旧会报错。
		System.out.println("start count:" + getCurrentTime());
		System.out.println(databaseMapper.getTableRowCount("social_insurance_person")); 
	    Cursor<String> cursor2 =databaseMapper.selectColumn("social_insurance_person","id_card",1000000);
	    System.out.println("start select:" + getCurrentTime());
	    //Cursor 实现了迭代器接口，因此在实际使用当中，从 Cursor 取数据非常简单
	    cursor2.forEach(stu -> {
//	    	System.out.println("游标的当前索引CurrentIndex："+cursor2.getCurrentIndex());
//	    	System.out.println("用于在取数据之前判断 Cursor 对象是否是打开状态。" +
//	                "只有当打开时 Cursor 才能取数据；："+cursor2.isOpen()); //：true
//	    	System.out.println("用于判断查询结果是否全部取完："+cursor2.isConsumed()); //false
//	    	System.out.println("输出的信息："+ stu);
	        //可以对数据进行处理，主要用于处理大批量的数据
//	        students.add(stu);
//	    	cuckooHashTable.insert(stu.getId());
	    });
	    System.out.println("end select:" + getCurrentTime());
	    System.out.println("返回多少条数据："+(cursor2.getCurrentIndex()+1)); //返回多少条数据：需要加1才是返回总数
	    System.out.println("用于在取数据之前判断 Cursor 对象是否是打开状态。" +
	            "只有当打开时 Cursor 才能取数据2；："+cursor2.isOpen()); //：false
	    System.out.println("用于判断查询结果是否全部取完2："+cursor2.isConsumed()); //true
	    if(cursor2.isConsumed()){
	    	System.out.println("final");
	    }else {
	    	System.out.println("err");
	    }
	}
	
//	@Test
//	@Async("threadPoolTaskExecutor")// 提交到线程池中去处理
	public void insertSocial() {
		Faker FAKER = new Faker(Locale.CHINA);
    	ArrayList<SocialEntity> arrayList = new ArrayList<>();
    	ArrayList<TaxEntity> arrayList1 = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        // 模拟数据
        int randomNumberLength = 18;
		Random random = new Random();
        
        for (int i = 0; i < 100000; i++){
//        	SocialEntity social = new SocialEntity();
//        	String name = FAKER.name().name();
//        	social.setName(name);
//        	long randomNumber = random.nextLong((long)Math.pow(10, randomNumberLength));
//        	String id_card = String.format("%018d", randomNumber);
//        	String address = FAKER.address().fullAddress();
//        	String phone = FAKER.phoneNumber().phoneNumber();
//        	social.setId_card(id_card);
//        	social.setAccount_state(random.nextInt(2));
//        	social.setAddress(address);
//        	social.setCardinal_number(random.nextDouble() * 100000);
//        	social.setCompany(FAKER.company().name());
//        	social.setPhone(phone);
//        	social.setSocial_insurance_num(random.nextDouble() * 10000000);
//        	social.setFirst_record_data(FAKER.date().birthday());
//            arrayList.add(social);
//
//            TaxEntity tax = new TaxEntity();
//            tax.setTax_account(random.nextDouble() * 10000000);
//            tax.setTax_place(FAKER.address().fullAddress());
//            tax.setTotal_tax(random.nextDouble() * 10000000);
//            tax.setFirst_tax_data(FAKER.date().birthday());
//            tax.setId_card(id_card);
//            tax.setPhone(phone);
//            tax.setAddress(address);
//            tax.setName(name);
//            arrayList1.add(tax);
        }
        int count = arrayList.size();
        int pageSize = 10000; // 每批次插入的数据量
        int threadNum = count / pageSize; // 线程数
        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        for (int i = 0; i < threadNum; i++) {
            int startIndex = i * pageSize;
            int endIndex = Math.min(count, (i + 1) * pageSize);
            List<SocialEntity> subList = arrayList.subList(startIndex, endIndex);
            List<TaxEntity> subList1 = arrayList1.subList(startIndex, endIndex);
            threadPoolTaskExecutor.execute(() -> {
                DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
                TransactionStatus status = transactionManager.getTransaction(transactionDefinition);
                try {
                	socialMapper.insertSplice(subList);
                	taxMapper.insertSplice(subList1);
                    transactionManager.commit(status);
                } catch (Exception e) {
                    transactionManager.rollback(status);
                    throw e;
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

	}
	
//	@Test
	public void generateKeyPair() {
		DemoHelloWorld demoHelloWorld = new DemoHelloWorld();
		System.out.println(demoHelloWorld.getKeyPair());
	}

	public String getCurrentTime()
	{
		// 创建一个日期对象
        Date date = new Date();
        // 创建一个格式化对象
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        // 使用格式化对象将日期格式化为字符串
        String formattedDate = sdf.format(date);
        
        return formattedDate;
	}
	
//	@Test
	public void testCuckooHash() {
	    //定义布谷鸟散列
        CuckooHashTable<String> cuckooHashTable = new CuckooHashTable<String>(hashFamily, 15000000);    
        System.out.println("start insert hash:" + getCurrentTime());
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "C:\\Users\\Administrator\\Desktop\\create_data.txt"),10*1024*1024);
//            		"C:\\Users\\Administrator\\Desktop\\source_data.txt"));
            String line = reader.readLine();
            
            while (line != null) {
                cuckooHashTable.insert(line);
                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }  
        
        System.out.println("finish insert hash:" + getCurrentTime());

        //打印表
        cuckooHashTable.printArray();
        System.out.println("start save hash:" + getCurrentTime());
//        System.out.println(cuckooHashTable.printBitArray("C:\\Users\\Administrator\\Desktop\\create_data_hash.txt"));
        System.out.println(cuckooHashTable.printBitArray("C:\\Users\\Administrator\\Desktop\\source_data_hash.txt"));

        System.out.println("start catch value:" + getCurrentTime());
        int count = 0;
        try {
            reader = new BufferedReader(new FileReader(
                    "C:\\Users\\Administrator\\Desktop\\selection_data.txt"));
            String line = reader.readLine();
            
            while (line != null) {
            	if(cuckooHashTable.contains(line))
            	{
//            		System.out.println(line);
            		count++;
            	}
                // read next line
                line = reader.readLine();
            }
            reader.close();
            System.out.printf("catch count = %d\n",count);
            System.out.println("end catch value:" + getCurrentTime());
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
//	@Test
	public void testStr()
	{
      BufferedReader reader;
      int count = 0;
      try {
          reader = new BufferedReader(new FileReader(
                  "C:\\Users\\Administrator\\Desktop\\result.txt"));
          String line = "";
	      boolean checked = false;
          while ((line = reader.readLine()) !=null) {
		            if(line.indexOf("Revealed output") >= 0)checked = true;
		            if(line.indexOf("bits oblivious transfer execution") >= 0)checked = false;
		            //处理结果数据
		            if(checked)
		            {
		            	int index = line.indexOf("0x");
		           	  	if(index >= 0)
		           	  	{
		           		  String tempStr = line.substring(index);
		           		  String[] temp = tempStr.split(" ");
		           		  for(int i = 0 ; i < temp.length ; i++)
		           		  {
		           			  String tmp_inner = temp[i];
		           			  tmp_inner = tmp_inner.substring(tmp_inner.indexOf("0x") + 2);
		           			  int strLength = tmp_inner.length();
		           			  //前补0
		           			  if(strLength < 16)
		           			  {
		           				  String zero = "";
		           				  for(int j = 0 ; j < 16 - strLength ; j++)
		           				  {
		           					  zero += "0";
		           				  }
		           				  tmp_inner = zero + tmp_inner;
		           			  }
		           			  
		        			  //无需重新排序，倒序计算
		        			  for(int j = 0 ; j < 8 ; j++)
		        			  {
		        				  String temp_ii = tmp_inner.substring(j*2, j*2 + 2);
		        				  int pos = (count-1)*64 + i*8 + 7 - j;
		        				  if(temp_ii.equalsIgnoreCase("31") && pos <= 12500003)
		           				  {
//		           					  if(cuckooHashTable.contains(cuckooHashTableSelect.value(pos)))
//		           						  result += "catch id index = " + String.valueOf(pos) + " value = " + cuckooHashTableSelect.value(pos) +"\n";
		        					  System.out.printf("catch str index = %d", (count-1)*64 + i*8 + 7 - j);
		           				  }
//		        				  if(temp_ii.equalsIgnoreCase("00"))
//		        				  {
//		        					  ;//nothing todo
//		        				  }else {
//		        					  System.out.printf("catch str index = %d", (count-1)*64 + i*8 + 7 - j);
//		        				  }
		        			  }
		           		  }
		           	  }
		                 count++;
		            }
          }
          reader.close();
          
//          while (line != null) {
//        	  int index = line.indexOf("0x");
//        	  if(index >= 0)
//        	  {
//        		  String tempStr = line.substring(index);
//        		  String[] temp = tempStr.split(" ");
//        		  for(int i = 0 ; i < temp.length ; i++)
//        		  {
//        			  String tmp_inner = temp[i];
//        			  tmp_inner = tmp_inner.substring(tmp_inner.indexOf("0x") + 2);
//        			  int strLength = tmp_inner.length();
//        			  //前补0
//        			  if(strLength < 16)
//        			  {
//        				  String zero = "";
//        				  for(int j = 0 ; j < 16 - strLength ; j++)
//        				  {
//        					  zero += "0";
//        				  }
//        				  tmp_inner = zero + tmp_inner;
//        			  }
//        			  
//        			  //重新排序
////        			  String t1 = tmp_inner.substring(0, 2);
////        			  String t2 = tmp_inner.substring(2, 4);
////        			  String t3 = tmp_inner.substring(4, 6);
////        			  String t4 = tmp_inner.substring(6, 8);
////        			  String t5 = tmp_inner.substring(8, 10);
////        			  String t6 = tmp_inner.substring(10, 12);
////        			  String t7 = tmp_inner.substring(12, 14);
////        			  String t8 = tmp_inner.substring(14, 16);
////        			  tmp_inner = t8 + t7 + t6 + t5 + t4 + t3 + t2 + t1;
////        			  System.out.printf(tmp_inner + "\n");
//        			  
//        			  //无需重新排序，倒序计算
//        			  for(int j = 0 ; j < 8 ; j++)
//        			  {
//        				  String temp_ii = tmp_inner.substring(j*2, j*2 + 2);
//        				  if(temp_ii.equalsIgnoreCase("00"))
//        				  {
//        					  ;//nothing todo
//        				  }else {
//        					  System.out.printf("catch str index = %d", (count-1)*64 + i*8 + 7 - j);
//        				  }
//        			  }
//        		  }
////        		  System.out.printf(tempStr + "\n");
//        	  }
//        	     	  
//              // read next line
//              line = reader.readLine();
//              count++;
//          }
//          reader.close();
      } catch (IOException e) {
          e.printStackTrace();
      }
	}

//	@Test
	public void testCatch()
	{
		BufferedReader reader;
		String[] source = new String[0];
		String[] select = new String[0];
		
	      try {
	          reader = new BufferedReader(new FileReader(
	                  "C:\\Users\\Administrator\\Desktop\\source_data.txt"));
	          String line = reader.readLine();
	          
	          while (line != null) {
	        	  source = Arrays.copyOf(source, source.length + 1);

	        	  source[source.length - 1] = line;
	              // read next line
	              line = reader.readLine();
	          }
	          reader.close();
	          
	          reader = new BufferedReader(new FileReader(
	                  "C:\\Users\\Administrator\\Desktop\\selection_data.txt"));
	          line = reader.readLine();
	          
	          while (line != null) {
	        	  select = Arrays.copyOf(select, select.length + 1);

	        	  select[select.length - 1] = line;
	              // read next line
	              line = reader.readLine();
	          }
	          
	          reader.close();
	      } catch (IOException e) {
	          e.printStackTrace();
	      }
	      
	      System.out.println("start circulate:" + getCurrentTime());
	      
	      int count = 0;
	      for(int i = 0 ; i < source.length ; i++)
	      {
	    	  for(int j = 0 ; j < select.length ; j++)
	    	  {
	    		  if(select[j].equals(source[i]))
	    		  {
	    			  count++;
	    			  System.out.println(select[j]);
	    		  }
	    	  }
	      }
	      System.out.println("finish circulate:" + getCurrentTime());
	      System.out.printf("catch count = %d" , count);
	}
	
//	@Test
	public void createData()
	{
    	String filePath = "C:\\Users\\Administrator\\Desktop\\create_data_lagrange.txt";
    	FileOutputStream fos;
    	Faker FAKER = new Faker(Locale.CHINA);
		try {
			fos = new FileOutputStream(filePath);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			String s = "";
			int randomNumberLength = 18;
			Random random = new Random();
			long randomNumber = 0;
			for(int i = 0 ; i < 10000000; i ++) {
//				randomNumber = random.nextLong((long)Math.pow(10, randomNumberLength));
//		    	PersonEntity person = new PersonEntity();
//	    		person.setAdult(FAKER.bool().bool());
//	        	person.setAge(FAKER.number().randomDigit());
//	        	person.setHeight(FAKER.number().randomDouble(1, 150, 200));
//	        	person.setId(String.format("%018d", randomNumber));
//	        	person.setMoney(new BigDecimal(FAKER.number().randomDigit()));
//	        	person.setName(FAKER.name().name());
//	        	person.setSex('男');
//	        	person.setWeight(FAKER.number().randomDouble(1, 100, 200));
//    	        s = JSONObject.toJSONString(person) + "\n";
//	    		bos.write(s.getBytes());
			}
			bos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Resource
    private RedisUtils redisUtil;
 
    public boolean redisset(String key, String value){
        System.out.println(key+"--"+value);
        return redisUtil.set(key,value);
    }
 
    public Object redisget(String key){
        System.out.println(redisUtil.get(key));
        return redisUtil.get(key);
    }
 
    public boolean expire(String key,long ExpireTime){
        return redisUtil.expire(key,ExpireTime);
    }
    
//    @Test
    public void testRedis()
    {
    	redisset("testKey","testValue");
    	redisget("testKey");
    }
    
//    @Test
    public void createData2()
    {
    	String filePath = "C:\\Users\\Administrator\\Desktop\\create_data.txt";
    	FileOutputStream fos;
    	int count = 0;
    	BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
            		"C:\\Users\\Administrator\\Desktop\\source_data.txt"));
            fos = new FileOutputStream(filePath);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
			String s = "";
			int randomNumberLength = 18;
			Random random = new Random();
            String line = reader.readLine();
            
            while (line != null) {
                if(count % 2 == 0)
                {
//                	long randomNumber = random.nextLong((long)Math.pow(10, randomNumberLength));
//
//        	        s = String.format("%018d", randomNumber);
//        	        s += "\n";
//    	    		bos.write(s.getBytes());
                }else {
                	line+= "\n";
                	bos.write(line.getBytes());
                }
            	count++;
                line = reader.readLine();
            }
            reader.close();
            bos.close();
			fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }
    
//    @Test
    public void testEntity()
    {
//    	PersonEntity person = JSONObject.parseObject(personString, PersonEntity.class);
//    	PersonEntity person = new PersonEntity();
//    	person.setAdult(true);
//    	person.setAge(26);
//    	person.setBirthday(DateUtil.parse("1996-11-29 12:00:22"));
//    	person.setHeight(170);
//    	person.setId("430702199611290011");
//    	person.setMoney(new BigDecimal(1000));
//    	person.setName("周志翔");
//    	person.setSex('男');
//    	person.setWeight(55);
//    	person.setDescribe("1234567自行车");
//    	String str = JSONObject.toJSONString(person);
//    	System.out.println(str);
//    	System.out.println(hashFamily.hash(str, 0));
    	Faker FAKER = new Faker(Locale.CHINA);
    	PersonEntity person = new PersonEntity();
    	int count = 10000000;
    	//定义布谷鸟散列
        CuckooHashTable<String> cuckooHashTable = new CuckooHashTable<String>(hashFamily, count);    
        System.out.println("start time:" + getCurrentTime());
        
        String filePath = "C:\\Users\\Administrator\\Desktop\\create_data.txt";
    	FileOutputStream fos;
		try {
			fos = new FileOutputStream(filePath);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			String s = "";
			for(int i = 0 ; i < count; i ++) {
				person.setAdult(FAKER.bool().bool());
	        	person.setAge(FAKER.number().randomDigit());
	        	person.setBirthday(FAKER.date().birthday());
	        	person.setHeight(FAKER.number().randomDouble(1, 150, 200));
	        	person.setId(FAKER.idNumber().ssnValid());
	        	person.setMoney(new BigDecimal(FAKER.number().randomDigit()));
	        	person.setName(FAKER.name().name());
	        	person.setSex('男');
	        	person.setWeight(FAKER.number().randomDouble(1, 100, 200));
	        	person.setDescribe(FAKER.toString());
    	        s = JSONObject.toJSONString(person);
    	        s += "\n";
	    		bos.write(s.getBytes());
			}
			bos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//    	for(int i = 0 ; i < count ; i++)
//    	{
//        	person.setAdult(FAKER.bool().bool());
//        	person.setAge(FAKER.number().randomDigit());
//        	person.setBirthday(FAKER.date().birthday());
//        	person.setHeight(FAKER.number().randomDouble(1, 150, 200));
//        	person.setId(FAKER.idNumber().ssnValid());
//        	person.setMoney(new BigDecimal(FAKER.number().randomDigit()));
//        	person.setName(FAKER.name().name());
//        	person.setSex('男');
//        	person.setWeight(FAKER.number().randomDouble(1, 100, 200));
//        	person.setDescribe(FAKER.toString());
////        	cuckooHashTable.insert(JSONObject.toJSONString(person));
//    	}
    	System.out.println("end time:" + getCurrentTime());
    }
    
//    @Test
    public void testLagrange()
    {
        Scanner reader =new Scanner(System.in);
        System.out.println("请输待处理的数据长度:");
        int N = reader.nextInt();
        double xi[] = new double[N];
        double yi[] = new double[N];
        BigInteger bXi[] = new BigInteger[N];
        BigInteger bYi[] = new BigInteger[N];
        System.out.println("请依次输入给定的插值点xi:");
        for(int i = 0;i < xi.length;i++)
        {
            xi[i] = reader.nextDouble();
            bXi[i] = BigInteger.valueOf((long) xi[i]);
        }
        System.out.println("请依次输入给定插值点对应的函数值yi:");
        for(int j = 0;j < yi.length;j++)
        {
            yi[j] = reader.nextDouble();
            bYi[j] = BigInteger.valueOf((long) yi[j]);
        }
        double x;
        BigInteger bx;
        LagrangeInterpolationFormula M = new MathUtils().new LagrangeInterpolationFormula();
        M.interpolateData(xi, yi);
//        System.out.println("运用拉格朗日插值法解得:");
//        for(x=xi[0];x<=xi[xi.length-1];x+=0.01)
//        {
//            System.out.printf("f(%4.2f)=%f\n",x,M.hValue(x));
//        }
        M.bInterpolateData(bXi, bYi);
        System.out.println("运用拉格朗日插值法解得:");
		for(int i = 0 ; i < N ; i++)
		{
			System.out.printf("f(%d)=%d\n",bXi[i].intValue(),M.hValue(bXi[i]).intValue());
		}
        
        System.out.println();
        System.out.println("请输入单独求的数值数目为:");
        int Num = reader.nextInt();
        System.out.println("要求的x值为:");
        double x3[]=new double[Num];
        BigInteger bX3[] = new BigInteger[Num];
        for(int i=0;i<Num;i++){
            x3[i] = reader.nextDouble();
            bX3[i] = BigInteger.valueOf((long) x3[i]);
        }
        for(int j=0;j<Num;j++){
            double Num_x=x3[j];
            BigInteger bNum_x = bX3[j];
            System.out.println("f("+Num_x+")=" + M.hValue(Num_x));
            System.out.println(M.fValue(Num_x) == 0);
            System.out.println("f("+bNum_x.intValue()+")=" + M.hValue(bNum_x));
            System.out.println(M.fValue(bNum_x).compareTo(BigInteger.valueOf(0)) == 0 ? true : false);
        }
    }
    
//    @Test
    public void testString2Char()
    {
    	String id = "621121196506084135";
    	char[] arrResult;
    	arrResult = id.toCharArray();
    	String result = "";
    	for(int i = 0; i < arrResult.length; i++)
        {
            int asc1 = (int)(arrResult[i]);
            result += String.format("%02x", asc1);
        }
    	System.out.println(result);
    	String str = new String(CommonUtils.hexStringToBytes(result));
        System.out.println(str);
    }
    
//    @Test
    public void testString2Byte()
    {
    	String name = "测试中文123";
    	Charset charset = Charset.forName("UTF-8");
    	ByteBuffer byteBuffer = charset.encode(name);
        byte[] byteArray = byteBuffer.array();
        System.out.println("中文字符串的字节数组：" + new String(byteArray));
    }
    
    @Autowired
    private CompanyServicePlus companyServicePlus;
    
//    @Test
    public void testCompany()
    {
//    	Random random = new Random();
//    	List<CompanyEntity> listComp = new ArrayList<CompanyEntity>();
//    	for(int i = 0 ; i < 1000 ; i++)
//    	{
//    		RiskDTO riskDTO = new RiskDTO("testRiskName" + i, random.nextInt(), random.nextInt(), "testRisk" + i);
//        	List<RiskDTO> listRiskDTO = new ArrayList<RiskDTO>();
//        	listRiskDTO.add(riskDTO);
//        	AdministrativePenaltyDTO administrativePenaltyDTO = new AdministrativePenaltyDTO("testID" + i, "testName" + i, random.nextInt(), random.nextInt(), false, random.nextInt());
//        	List<AdministrativePenaltyDTO> listAdministrativePenaltyDTO = new ArrayList<AdministrativePenaltyDTO>();
//        	listAdministrativePenaltyDTO.add(administrativePenaltyDTO);
//        	RealEstateDTO realEstateDTO = new RealEstateDTO("testRealID" + i, "testRealName" + i, random.nextInt(), random.nextDouble(), "长沙", random.nextInt());
//        	List<RealEstateDTO> listRealEstateDTO = new ArrayList<RealEstateDTO>();
//        	listRealEstateDTO.add(realEstateDTO);
//        	CompanyEntity companyEntity = new CompanyEntity("testUSCC" + i, "testCompany" + i, random.nextDouble(), random.nextDouble(), random.nextDouble(), random.nextDouble(), random.nextDouble(), random.nextDouble(), random.nextInt(), listRiskDTO, listAdministrativePenaltyDTO, listRealEstateDTO, random.nextInt(15,300));
//        	listComp.add(companyEntity);
//    	}
//    	System.out.println(companyServicePlus.saveBatch(listComp));
    }
    
//    @Test
    public void testOperator()
    {
    	double[] inputS = new double[16];
    	for(int i = 0 ; i < inputS.length ; i++)
    	{
    		inputS[i] = i;
    	}
    	double[] result = Operators.rmsNorm(inputS);
    	for(double d : result)
    	{
    		System.out.println(d);
    	}
    }

	@Test
	public void testNewOperator() {
		double[] inputS = new double[13];
		for(int i = 0 ; i < inputS.length ; i++)
		{
			inputS[i] = i;
		}



	}
}

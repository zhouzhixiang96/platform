#!/bin/bash

maven_home='/home/webDev/maven/apache-maven-3.9.6'
jar_path='./target/platform-0.0.1-SNAPSHOT.jar'
pid=$(ps aux | grep java | grep platform | grep -v grep | grep -v restart | awk '{print $2}')

if [ -n "$pid" ]; then
  echo "shutdown..."
  kill -15 ${pid}

  sleep 3

  pid=$(ps aux | grep java | grep platform | grep -v grep | grep -v restart | awk '{print $2}')

  if [ -n "$pid" ]; then
    kill -9 ${pid}
    sleep 1
  fi
fi

echo "updating project..."
git pull

echo "packaging..."
${maven_home}/bin/mvn clean package

echo "restarting"
nohup ${JAVA_HOME}/bin/java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5898 -jar ${jar_path} --spring.profiles.active=test --enable.operator.check=false > nohup.out 2>&1 &

tail -f ./nohup.out
